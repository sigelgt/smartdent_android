package sigel.smart_dent.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by pablin on 23/05/2016.
 */
public class Paciente implements Parcelable {

    private String _id;
    private String _id_doctor;
    private String imagen;
    private String nombres;
    private String apellidos;
    private String fecha_nacimiento;
    private String edad;
    private String genero;
    private String nacionalidad;
    private String estado_civil;
    private String direccion;
    private String email;
    private String ocupacion;
    private String numero_telefono;
    private String referido_por;
    private String contacto;
    private String contacto_numero_telefono;
    private String estado;
    private String tipo_identificacion;
    private String numero_identificacion;
    private String consideraciones_medicas;
    private String motivo_consulta;

    public String getEdad() {
        return edad;
    }

    public void setEdad(String edad) {
        this.edad = edad;
    }

    public String getMotivo_consulta() {
        return motivo_consulta;
    }

    public void setMotivo_consulta(String motivo_consulta) {
        this.motivo_consulta = motivo_consulta;
    }

    public String getTipo_identificacion() {
        return tipo_identificacion;
    }

    public void setTipo_identificacion(String tipo_identificacion) {
        this.tipo_identificacion = tipo_identificacion;
    }

    public String getNumero_identificacion() {
        return numero_identificacion;
    }

    public void setNumero_identificacion(String numero_identificacion) {
        this.numero_identificacion = numero_identificacion;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;

    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String get_id_doctor() {
        return _id_doctor;
    }

    public void set_id_doctor(String _id_doctor) {
        this._id_doctor = _id_doctor;
    }

    public String getImagen() {
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getFecha_nacimiento() {
        return fecha_nacimiento;
    }

    public void setFecha_nacimiento(String fecha_nacimiento) {
        this.fecha_nacimiento = fecha_nacimiento;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    public String getNacionalidad() {
        return nacionalidad;
    }

    public void setNacionalidad(String nacionalidad) {
        this.nacionalidad = nacionalidad;
    }

    public String getEstado_civil() {
        return estado_civil;
    }

    public void setEstado_civil(String estado_civil) {
        this.estado_civil = estado_civil;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getOcupacion() {
        return ocupacion;
    }

    public void setOcupacion(String ocupacion) {
        this.ocupacion = ocupacion;
    }

    public String getNumero_telefono() {
        return numero_telefono;
    }

    public void setNumero_telefono(String numero_telefono) {
        this.numero_telefono = numero_telefono;
    }

    public String getReferido_por() {
        return referido_por;
    }

    public void setReferido_por(String referido_por) {
        this.referido_por = referido_por;
    }

    public String getContacto() {
        return contacto;
    }

    public void setContacto(String contacto) {
        this.contacto = contacto;
    }

    public String getContacto_numero_telefono() {
        return contacto_numero_telefono;
    }

    public void setContacto_numero_telefono(String contacto_numero_telefono) {
        this.contacto_numero_telefono = contacto_numero_telefono;
    }

    public String getConsideraciones_medicas() {
        return consideraciones_medicas;
    }

    public void setConsideraciones_medicas(String consideraciones_medicas) {
        this.consideraciones_medicas = consideraciones_medicas;
    }

    public Paciente() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this._id);
        dest.writeString(this._id_doctor);
        dest.writeString(this.imagen);
        dest.writeString(this.nombres);
        dest.writeString(this.apellidos);
        dest.writeString(this.fecha_nacimiento);
        dest.writeString(this.edad);
        dest.writeString(this.genero);
        dest.writeString(this.nacionalidad);
        dest.writeString(this.estado_civil);
        dest.writeString(this.direccion);
        dest.writeString(this.email);
        dest.writeString(this.ocupacion);
        dest.writeString(this.numero_telefono);
        dest.writeString(this.referido_por);
        dest.writeString(this.contacto);
        dest.writeString(this.contacto_numero_telefono);
        dest.writeString(this.consideraciones_medicas);
        dest.writeString(this.motivo_consulta);
    }

    protected Paciente(Parcel in) {
        this._id = in.readString();
        this._id_doctor = in.readString();
        this.imagen = in.readString();
        this.nombres = in.readString();
        this.apellidos = in.readString();
        this.fecha_nacimiento = in.readString();
        this.edad = in.readString();
        this.genero = in.readString();
        this.nacionalidad = in.readString();
        this.estado_civil = in.readString();
        this.direccion = in.readString();
        this.email = in.readString();
        this.ocupacion = in.readString();
        this.numero_telefono = in.readString();
        this.referido_por = in.readString();
        this.contacto = in.readString();
        this.contacto_numero_telefono = in.readString();
        this.consideraciones_medicas = in.readString();
        this.motivo_consulta = in.readString();
        this.estado = in.readString();
        this.tipo_identificacion = in.readString();
        this.numero_identificacion = in.readString();

    }

    public static final Creator<Paciente> CREATOR = new Creator<Paciente>() {
        @Override
        public Paciente createFromParcel(Parcel source) {
            return new Paciente(source);
        }

        @Override
        public Paciente[] newArray(int size) {
            return new Paciente[size];
        }
    };
}

package sigel.smart_dent.models;

import android.view.View;
import android.widget.CompoundButton;
import android.widget.RadioButton;

/**
 * Created by Juan on 22/06/2016.
 */

public class CustomRadioButtonHandler {


    int mId;
    Boolean mCheked = false;
    RadioButton mRadio;

    public CustomRadioButtonHandler(View view){
        this.mRadio = (RadioButton) view;
        toggleCheck();

        mRadio.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(!isChecked){
                    mCheked = false;
                }
            }
        });

        this.mRadio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toggleCheck();
            }
        });

    }

    public void ckeck(){
        mCheked = true;
        mRadio.setChecked(true);
    }

    public void unChek(){
        mCheked = false;
        mRadio.setChecked(false);
    }

    public void toggleCheck(){
        if(mCheked){
            unChek();
        }else{
            ckeck();
        }

    }

    public boolean isChecked(){
        return mCheked;
    }


}

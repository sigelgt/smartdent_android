package sigel.smart_dent.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Juan on 11/07/2016.
 */
public class DentalRecordModel implements Parcelable {

    public DentalRecordModel(){}

    protected DentalRecordModel(Parcel in) {

    }

    public static final Creator<DentalRecordModel> CREATOR = new Creator<DentalRecordModel>() {
        @Override
        public DentalRecordModel createFromParcel(Parcel in) {
            return new DentalRecordModel(in);
        }

        @Override
        public DentalRecordModel[] newArray(int size) {
            return new DentalRecordModel[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
    }
}

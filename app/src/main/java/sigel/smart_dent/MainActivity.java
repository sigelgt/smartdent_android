package sigel.smart_dent;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import sigel.smart_dent.modules.user_management.activities.Login;
import sigel.smart_dent.modules.user_management.activities.Registro;

public class MainActivity extends AppCompatActivity {

    Button login;
    Button registro;
    TextView mSubtitleParagraph;
    TextView mWelcomText;

    private static final String PREFS_FILE = "sigel.smart_dent.preferences";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Typeface fontAvenirMedium = Typeface.createFromAsset(getAssets(), "Avenir-Medium.ttf");

        mSubtitleParagraph = (TextView) findViewById(R.id.subtitleParagraphWelcome);
        mWelcomText = (TextView) findViewById(R.id.welcomeText);
        mSubtitleParagraph.setTypeface(fontAvenirMedium);
        mWelcomText.setTypeface(fontAvenirMedium);

                //Intent redirecIntent = new Intent(MainActivity.this, Login.class);
        //startActivity(redirecIntent);

        //getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        //DbHelper helper = new DbHelper(this);
        //SQLiteDatabase db = helper.getWritableDatabase();
        //Manager manager = new Manager(this);

        // Link para layout login
        login = (Button) findViewById(R.id.btn2);
        login.setTypeface(fontAvenirMedium);

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Crearmos el Intent
                Intent intent = new Intent(MainActivity.this, Login.class);

                //Iniciamos la nueva actividad
                startActivity(intent);
            }
        });

        //Link para Layout Registro
        registro = (Button) findViewById(R.id.btn3);
        registro.setTypeface(fontAvenirMedium);

        registro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Crearmos el Intent
                Intent intent = new Intent(MainActivity.this, Registro.class);

                //Iniciamos la nueva actividad
                startActivity(intent);
            }
        });

    }

    @Override
    protected void onResume(){
        super.onResume();

        SharedPreferences prefs = getSharedPreferences(PREFS_FILE, MODE_PRIVATE);
        String restoredID = prefs.getString("_id", null);
        String restoredName = prefs.getString("nombres", null);
        String restoredLastname = prefs.getString("apellidos", null);
        Boolean restoredSesion = prefs.getBoolean("sesion", false);

        Log.e("SESION", String.valueOf(restoredSesion));

        if(restoredSesion){
            Intent intent = new Intent(MainActivity.this, Login.class);
            startActivity(intent);
        }


    }
}

package sigel.smart_dent;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;

import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.http.HEAD;
import sigel.smart_dent.modules.user_management.Plan;
import sigel.smart_dent.modules.user_management.Usuario;
import sigel.smart_dent.modules.user_management.UsuarioDatos;
import sigel.smart_dent.modules.user_management.activities.Dashboard;
import sigel.smart_dent.modules.user_management.activities.Registro;
import sigel.smart_dent.rest.APIPlug;
import sigel.smart_dent.rest.ServiceGenerator;
import sigel.smart_dent.tools.Tools;

import static sigel.smart_dent.R.id.container;

/**
 * Created by eduardogomez on 22/07/16.
 */
public class Planes extends AppCompatActivity {

    SectionsPagerAdapter mSectionsPagerAdapter;
    ViewPager mViewPager;

    Button plan;

    static int planSelect;
    String planString;

    APIPlug cliente = ServiceGenerator.createService(APIPlug.class);
    private static final String PREFS_FILE = "sigel.smart_dent.preferences";

    Tools tools;

    static TextView textTitulo;
    TextView textDetalle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_planes);

        tools = new Tools();

        //Sincro sincro = new Sincro();
        planString = "";
        final UsuarioDatos usuarioDatos = new UsuarioDatos();

        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();

        textTitulo = (TextView)findViewById(R.id.textSelectPlan);
        textDetalle = (TextView)findViewById(R.id.textSelectPlanDetalle);
        plan = (Button) findViewById(R.id.butonSelectPlan);


        if (bundle != null) {

           // usuarioDatos.set_id(bundle.get("_id").toString());
            usuarioDatos.setEmail(bundle.get("email").toString());
//            usuarioDatos.set_id(bundle.get("_id").toString());
            usuarioDatos.setUser(bundle.get("usuario").toString());
            usuarioDatos.setPassword(bundle.get("pass").toString());
            usuarioDatos.setConfirm_Password(bundle.get("confirmar_pass").toString());
            usuarioDatos.setName(bundle.get("nombre").toString());
            usuarioDatos.setLastName(bundle.get("apellido").toString());
            usuarioDatos.setDateBirth(bundle.get("fecha_nacimiento").toString());
            usuarioDatos.setNationality(bundle.get("nacionalidad").toString());
            usuarioDatos.setAddress(bundle.get("dirección").toString());
            usuarioDatos.setCity(bundle.get("ciudad").toString());
            usuarioDatos.setProvince(bundle.get("provincia").toString());
            usuarioDatos.setPostalCode(bundle.get("codigo_postal").toString());
            usuarioDatos.setTelephone(bundle.get("telefono").toString());

            //Link para layout registro

            plan.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    switch (planSelect){
                        case 0:

                            usuarioDatos.setId_planes("577ecd12a15e43497ed27fe0");
                            Log.e("usuarioDatos",usuarioDatos.toString());
                            register(usuarioDatos,getApplicationContext());
                            break;
                        case 1:
                            usuarioDatos.setId_planes("577ecd12a15e43497ed27fe0");
                            Log.e("usuarioDatos",usuarioDatos.toString());
                            register(usuarioDatos,getApplicationContext());
                            break;
                        case 2:

                            usuarioDatos.setId_planes("577ecd12a15e43497ed27fe0");
                            Log.e("usuarioDatos",usuarioDatos.toString());
                            register(usuarioDatos,getApplicationContext());
                            break;
                    }

                }
            });
        }

        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.

        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(container);

        mSectionsPagerAdapter.addFragment(SectionsPagerAdapter.PlaceholderFragment.newInstance(0, getResources().getColor(R.color.android_blue)));
        mSectionsPagerAdapter.addFragment(SectionsPagerAdapter.PlaceholderFragment.newInstance(1, getResources().getColor(R.color.android_darkpurple)));
        mSectionsPagerAdapter.addFragment(SectionsPagerAdapter.PlaceholderFragment.newInstance(2, getResources().getColor(R.color.android_orange)));

        mViewPager.setAdapter(mSectionsPagerAdapter);
    }
    public static void changeView(){
        switch (planSelect){
            case 0:
                textTitulo.setText("Gratis");
            case 1:
                textTitulo.setText("Doctor");
            case 2:
                textTitulo.setText("Clinica");

        }

    }

    public static class SectionsPagerAdapter extends FragmentPagerAdapter {

            List<android.support.v4.app.Fragment> fragments; //acá voy a guardar los fragments
             private int planSelect ;

             //constructor
            public SectionsPagerAdapter(FragmentManager fm) {
                super(fm);
                this.fragments = new ArrayList();
            }

            @Override
            public android.support.v4.app.Fragment getItem(int position) {
                //return PlaceholderFragment.newInstance(position + 1);
                planSelect = position;
                changeView();
                return fragments.get(position);
            }

            @Override
            public int getCount() {
                //Show 3 total pages.
                //return 3;
                return this.fragments.size();
            }

            public void addFragment(android.support.v4.app.Fragment xfragment) {
                this.fragments.add(xfragment);
            }



        public static class PlaceholderFragment extends android.support.v4.app.Fragment {

            /**
             * Agregue "color"
             */

            private static final String ARG_SECTION_NUMBER = "section_number";
            private static final String ARG_SECTION_TEXT = "section_label";
            private static final String BACKGROUND_COLOR = "color";

            private int section_number;
            private int color;
            private String section_label;

            public static PlaceholderFragment newInstance(int sectionNumber, int color) {

                PlaceholderFragment fragment = new PlaceholderFragment();   //instanciamos un nuevo fragment

                Bundle args = new Bundle();                                 //guardamos los parametros
                args.putInt(ARG_SECTION_NUMBER, sectionNumber);
                args.putInt(BACKGROUND_COLOR, color); //agrego ademas el color de fondo
                args.putInt(ARG_SECTION_TEXT, R.id.section_label); // agrego texto
                fragment.setArguments(args);
                fragment.setRetainInstance(true);     //agrego para que no se pierda los valores de la instancia
                return fragment;
            }


            @Override
            public void onCreate(Bundle savedInstanceState) {
                super.onCreate(savedInstanceState);
                //cuando crea una instancia de tipo PlaceholderFragment
                //si lo enviamos parametros, guarda esos
                //si no le envio nada, toma el color gris y un número aleatroio
                if (getArguments() != null) {
                    this.section_number = getArguments().getInt(ARG_SECTION_NUMBER);
                    this.section_label = getArguments().getString(ARG_SECTION_TEXT);
                    this.color = getArguments().getInt(BACKGROUND_COLOR);
                } else {
                    this.color = Color.GRAY;
                    this.section_number = (int) (Math.random() * 5);
                }

            }


            @Override
            public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                     Bundle savedInstanceState) {
                View rootView = inflater.inflate(R.layout.fragment_main, container, false);

                TextView section = (TextView) rootView.findViewById(R.id.section_label);
                section.setText("" + section_number);
                rootView.setBackgroundColor(this.color);


                return rootView;
            }

        }

    }

    public void register(UsuarioDatos usuarioDatos, Context context) {

        final Intent intent = new Intent(this, Dashboard.class);
        final Intent intent2 = new Intent(this, Registro.class);

        final SharedPreferences.Editor editorSharedPref = context.getSharedPreferences(PREFS_FILE, context.MODE_PRIVATE).edit();

        Call<Usuario> call = cliente.registerUser(usuarioDatos.getCity(), usuarioDatos.getAddress(), usuarioDatos.getPostalCode(), usuarioDatos.getDateBirth(), usuarioDatos.getName(), usuarioDatos.getDocumentIdentification(), usuarioDatos.getNumberDocument(), usuarioDatos.getEmail(), usuarioDatos.getProfession(), usuarioDatos.getNationality(), usuarioDatos.getProvince(), usuarioDatos.getTelephone(), tools.md5(usuarioDatos.getPassword()), usuarioDatos.getLastName(), usuarioDatos.getUser(), usuarioDatos.getId_planes());

        call.enqueue(new Callback<Usuario>() {
            @Override
            public void onResponse(Call<Usuario> call, Response<Usuario> response) {
                Log.e("onResponse", response.body().getMessage());

                UsuarioDatos user = new UsuarioDatos();
                Plan plans = new Plan();

                user.set_id(response.body().getData().get_id());
                user.setCity(response.body().getData().getCity());
                user.setAddress(response.body().getData().getAddress());
                user.setPostalCode(response.body().getData().getPostalCode());
                user.setDateBirth(response.body().getData().getDateBirth());
                user.setName(response.body().getData().getName());
                user.setDocumentIdentification(response.body().getData().getDocumentIdentification());
                user.setNumberDocument(response.body().getData().getNumberDocument());
                user.setEmail(response.body().getData().getEmail());
                user.setProfession(response.body().getData().getProfession());
                user.setNationality(response.body().getData().getNationality());
                user.setProvince(response.body().getData().getProvince());
                user.setTelephone(response.body().getData().getTelephone());
                user.setPassword(response.body().getData().getPassword());
                user.setLastName(response.body().getData().getLastName());
                user.setUser(response.body().getData().getUser());

//                plans.setId(response.body().getData().getPlanes().getId());
//                plans.setDatePurchase(response.body().getData().getPlanes().getDatePurchase());
//                plans.setFinishDate(response.body().getData().getPlanes().getFinishDate());
//                plans.setRecordLimit(response.body().getData().getPlanes().getRecordLimit());
//                plans.setValidityDays(response.body().getData().getPlanes().getValidityDays());


                Boolean sesion = true;

                editorSharedPref.putString("_id", user.get_id());
                editorSharedPref.putString("city", user.getCity());
                editorSharedPref.putString("address", user.getAddress());
                editorSharedPref.putString("postalCode", user.getPostalCode());
                editorSharedPref.putString("dateBirth", user.getDateBirth());
                editorSharedPref.putString("name", user.getName());
                editorSharedPref.putString("documentIdentification", user.getDocumentIdentification());
                editorSharedPref.putString("numberDocument", user.getNumberDocument());
                editorSharedPref.putString("email", user.getEmail());
                editorSharedPref.putString("profession", user.getProfession());
                editorSharedPref.putString("nationality", user.getNationality());
                editorSharedPref.putString("province", user.getProvince());
                editorSharedPref.putString("telephone", user.getTelephone());
                editorSharedPref.putString("password", user.getPassword());
                editorSharedPref.putString("lastName", user.getLastName());
                editorSharedPref.putString("user", user.getUser());

//                editorSharedPref.putString("planFinishDate", plans.getFinishDate());
//                editorSharedPref.putString("planRecordLimit", plans.getRecordLimit());
//                editorSharedPref.putString("planValidityDays", plans.getValidityDays());
//                editorSharedPref.putString("planID", plans.getId());

                editorSharedPref.putBoolean("sesion", sesion);

                editorSharedPref.commit();

                startActivity(intent);
            }

            @Override
            public void onFailure(Call<Usuario> call, Throwable t) {
                Log.e("onFailure", t.toString());
                Toast.makeText(getBaseContext(), "Por favor verifique sus datos", Toast.LENGTH_SHORT).show();
                startActivity(intent2);
            }
        });
    }

}


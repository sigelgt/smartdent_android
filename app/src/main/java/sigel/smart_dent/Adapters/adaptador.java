package sigel.smart_dent.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import sigel.smart_dent.R;
import sigel.smart_dent.beans.recyclev;

/**
 * Created by eduardogomez on 12/05/16.
 */
public class adaptador extends RecyclerView.Adapter<adaptador.recycleViewHolder> {

    List<recyclev> listarecycle;

    public adaptador(List<recyclev> listarecycle)
    {
        this.listarecycle = listarecycle;
    }

    @Override
    public recycleViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_articulo, parent, false);
        recycleViewHolder holder = new recycleViewHolder(v);
        return holder;
    }

    @Override
    public void onBindViewHolder(recycleViewHolder holder, int position) {
        holder.imgrecycle.setImageResource(listarecycle.get(position).getFoto());
        holder.txtrecycle.setText(listarecycle.get(position).getNombre());

    }

    @Override
    public int getItemCount() {
        return listarecycle.size();
    }

    public static class recycleViewHolder extends RecyclerView.ViewHolder
    {
        ImageView imgrecycle;
        TextView txtrecycle;

        public recycleViewHolder(View itemView) {
            super(itemView);
            imgrecycle = (ImageView) itemView.findViewById(R.id.imgrecycle);
            txtrecycle = (TextView) itemView.findViewById(R.id.txtrecycle);
        }
    }

}

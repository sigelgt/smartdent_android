package sigel.smart_dent.adapters;

import android.content.Context;
import android.content.Intent;
import android.os.Parcelable;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import sigel.smart_dent.modules.patient_management.patients.PacienteAgregar;
import sigel.smart_dent.R;
import sigel.smart_dent.models.Paciente;

/**
 * Created by pablin on 23/05/2016.
 */
public class PacienteAdapter extends RecyclerView.Adapter<PacienteAdapter.PacienteViewHolder>{

    private LayoutInflater layoutInflater;
    private Context mContext;

    private ArrayList<Paciente> pacienteList = new ArrayList<>();

    public void setPacienteList(ArrayList<Paciente> pacienteList){
        this.pacienteList = pacienteList;
        notifyItemRangeChanged(0,pacienteList.size());
    }

    public PacienteAdapter(Context context){
        this.mContext = context;
        this.layoutInflater = LayoutInflater.from(context);
    }



    @Override
    public PacienteViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = layoutInflater.inflate(R.layout.paciente_listview_row, parent, false);
        final PacienteViewHolder pacienteHolder = new PacienteViewHolder(view);

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int position = pacienteHolder.getAdapterPosition();

                //Intent intent = new Intent(mContext, PacienteListarDetalle.class);
                Intent intent = new Intent(mContext, PacienteAgregar.class);
                intent.putExtra(PacienteAgregar.EXTRA_PACIENTE, (Parcelable) pacienteList.get(position));
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                mContext.startActivity(intent);

            }
        });

        return pacienteHolder;

    }

    static class PacienteViewHolder extends RecyclerView.ViewHolder {

        ImageView txtImagen;
        TextView txtNombres, txtApellidos, txtEdad;

        public PacienteViewHolder(View itemView) {
            super(itemView);

            txtImagen = (ImageView) itemView.findViewById(R.id.paciente_listar_imagen);
            txtNombres = (TextView) itemView.findViewById(R.id.paciente_listar_text_view_nombres);
            //txtApellidos = (TextView) itemView.findViewById(R.id.paciente_listar_text_view_apellidos);

        }
    }


    @Override
    public void onBindViewHolder(PacienteViewHolder holder, int position) {

        Paciente paciente = pacienteList.get(position);

        Picasso.with(layoutInflater.getContext()).load(paciente.getImagen())
                .into(holder.txtImagen, new com.squareup.picasso.Callback() {
                    @Override
                    public void onSuccess() {
                        Log.e("AdapterPeliculas", "onSuccess");
                    }

                    @Override
                    public void onError() {
                        Log.e("AdapterPeliculas", "onError");
                    }
                });

        holder.txtNombres.setText(new StringBuilder()
                // Month is 0 based so add 1
                .append(paciente.getNombres()).append(" ")
                .append(paciente.getApellidos()));


        //holder.txtApellidos.setText(paciente.getApellidos());

    }

    @Override
    public int getItemCount() {
        return pacienteList.size();
    }
}

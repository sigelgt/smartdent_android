package sigel.smart_dent.tools;

/**
 * Created by juanlopez on 8/2/16.
 */
public class Constants {

    public static final String ACTION_RUN_ISERVICE = "sigel.smart_dent.action.RUN_INTENT_SERVICE";
    public static final String ACTION_PROGRESS_EXIT = "sigel.smart_dent.action.PROGRESS_EXIT";
    public static final String EXTRA_PROGRESS = "sigel.smart_dent.extra.PROGRESS";
}

package sigel.smart_dent;

import android.app.Dialog;
import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by pablin on 9/06/2016.
 */
public class CoverFlowAdapter extends BaseAdapter {

        private ArrayList<Diente> data;
        private AppCompatActivity activity;

        public CoverFlowAdapter(AppCompatActivity context, ArrayList<Diente> objects) {
            this.activity = context;
            this.data = objects;
        }

        @Override
        public int getCount() {
            return data.size();
        }

        @Override
        public Diente getItem(int position) {
            return data.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            ViewHolder viewHolder;

            if (convertView == null) {
                LayoutInflater inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = inflater.inflate(R.layout.activity_paciente_examen_endodoncia_content_item_flow_view, null, false);

                viewHolder = new ViewHolder(convertView);
                convertView.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder) convertView.getTag();
            }

            viewHolder.dienteImagen.setImageResource(data.get(position).getImageSource());
            viewHolder.dienteNombre.setText(data.get(position).getName());

            convertView.setOnClickListener(onClickListener(position));

            return convertView;
        }

        private View.OnClickListener onClickListener(final int position) {
            return new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    final Dialog dialog = new Dialog(activity);
                    dialog.setContentView(R.layout.dialog_diente_informacion);
                    dialog.setCancelable(true); // dimiss when touching outside
                    dialog.setTitle("Detalle del diente");

                    TextView text = (TextView) dialog.findViewById(R.id.name);
                    text.setText(getItem(position).getName());
                    ImageView image = (ImageView) dialog.findViewById(R.id.image);
                    image.setImageResource(getItem(position).getImageSource());

                    dialog.show();
                }
            };
        }


        private static class ViewHolder {
            private TextView dienteNombre;
            private ImageView dienteImagen;

            public ViewHolder(View v) {
                dienteImagen = (ImageView) v.findViewById(R.id.image);
                dienteNombre = (TextView) v.findViewById(R.id.name);
            }
        }
}

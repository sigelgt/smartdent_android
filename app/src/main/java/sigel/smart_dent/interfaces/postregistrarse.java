package sigel.smart_dent.interfaces;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;
import sigel.smart_dent.beans.Response;
import sigel.smart_dent.beans.usuario;

/**
 * Created by eduardogomez on 09/03/16.
 */
public interface postregistrarse {
        @FormUrlEncoded
        @POST("usuarios")
        Call<Response> insertUser(

                @Field("usuario")String usuario,
                @Field("pass") String pass,
                @Field("Confirmar_pass") String confirmar_pass,
                @Field("nombre") String nombre,
                @Field("apellido") String apellido,
                @Field("fecha_nacimiento") String fecha_nacimiento,
                @Field("nacionalidad") String nacionalidad,
                @Field("tipo") String tipo,
                @Field("profesion") String profesion,
                @Field("pais") String pais,
                @Field("direccion") String dirección,
                @Field("ciudad") String ciudad,
                @Field("provincia") String provincia,
                @Field("fecha_creacion") String fecha_creacion,
                @Field("codigo_postal") String codigo_postal,
                @Field("telefono") String telefono);
}

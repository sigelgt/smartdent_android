package sigel.smart_dent.interfaces;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;
import sigel.smart_dent.beans.Response;

/**
 * Created by eduardogomez on 14/03/16.
 */
public interface postarticulo {
    @FormUrlEncoded
    @POST("articulos")
    Call<Response> insertUser(

            @Field("nombre") String nombre,
            @Field("descripcion") String descripcion,
            @Field("usuario_id") String usuario_id,
            @Field("proveedor_id") String proveedor_id,
            @Field("fecha_compra") String fecha_compra,
            @Field("documento") String documento,
            @Field("fecha_expiracion") String fecha_expiracion,
            @Field("cantidad") String cantidad,
            @Field("existencias") String existencias,
            @Field("valor_total") String valor_total,
            @Field("valor_unitario") String valor_unitario,
            @Field("fecha_creacion") String fecha_creacion);
}

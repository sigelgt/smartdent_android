package sigel.smart_dent.interfaces;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;
import sigel.smart_dent.beans.Response;

/**
 * Created by eduardogomez on 12/03/16.
 */
public interface postproveedor {
    @FormUrlEncoded
    @POST("proveedores")
    Call<Response> insertUser(

            @Field("nombre") String nombre,
            @Field("nit") String nit,
            @Field("representante_legal") String representante_legal,
            @Field("contacto_comercial") String contacto_comercial,
            @Field("telefono") String telefono,
            @Field("direccion") String direccion,
            @Field("email") String email,
            @Field("fecha_creacion") String fecha_creacion);
}

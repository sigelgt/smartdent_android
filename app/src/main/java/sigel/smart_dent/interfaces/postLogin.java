package sigel.smart_dent.interfaces;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import sigel.smart_dent.beans.usuario;

/**
 * Created by eduardogomez on 03/03/16.
 */
public interface postLogin {
    @GET("usuarios/{user}/{pass}")
    Call<usuario> loginUser(@Path("user") String user,@Path("pass") String pass);
}

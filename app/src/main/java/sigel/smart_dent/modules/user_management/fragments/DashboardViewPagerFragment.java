package sigel.smart_dent.modules.user_management.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import sigel.smart_dent.R;
import sigel.smart_dent.models.Paciente;
import  sigel.smart_dent.modules.clinic_management.appointments.fragments.AppointmentListFragment;

/**
 * Created by Juan on 19/06/2016.
 */
public class DashboardViewPagerFragment extends Fragment {
    //public static final String KEY_RECIPE_INDEX = "recipe_index";

    private ArrayList<Paciente> mPatientList;
    private int mCurrentPage;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        mCurrentPage = getArguments().getInt("current");
        Log.d("CURRENTPAGE",mCurrentPage + "");
        View view = inflater.inflate(R.layout.generic_viewpager_fragment,container,false);


        //int index = getArguments().getInt(ViewPagerFragment.KEY_RECIPE_INDEX);

       // getActivity().setTitle("SmartDent");

        final DashboardPatientListFragment list1 = new DashboardPatientListFragment();
        list1.setPatientList(mPatientList);
        list1.setFragmentTestId(1);

        final DashboardPatientListFragment list2 = new DashboardPatientListFragment();
        list2.setPatientList(mPatientList);
        list2.setFragmentTestId(2);
        //final DashboardPatientListFragment list2 = new DashboardPatientListFragment();
        //Bundle ingredientsBundle = new Bundle();
        //ingredientsBundle.putInt(KEY_RECIPE_INDEX,index);
        //ingredientsFragment.setArguments(ingredientsBundle);

        final AppointmentListFragment appointmentListFragment = new AppointmentListFragment();
        //Bundle directionsBundle = new Bundle();
        //directionsBundle.putInt(KEY_RECIPE_INDEX,index);
        //directionsFragment.setArguments(directionsBundle);

        ViewPager viewPager = (ViewPager) view.findViewById(R.id.genericViewPager);
        viewPager.setAdapter(new FragmentPagerAdapter(getChildFragmentManager()) {
            @Override
            public Fragment getItem(int position) {
                return position == 0 ? appointmentListFragment : list1;
            }

            @Override
            public int getCount() {
                return 2;
            }

            @Override
            public CharSequence getPageTitle(int position) {
                return position == 0 ? getResources().getString(R.string.appointments_title) : getResources().getString(R.string.patients_title);
            }
        });

        viewPager.setCurrentItem(mCurrentPage);
        TabLayout tabLayout = (TabLayout) view.findViewById(R.id.genericTabLayout);
        tabLayout.setTabMode(TabLayout.MODE_FIXED);
        tabLayout.setupWithViewPager(viewPager);

        return view;
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    public void setPatientList(ArrayList<Paciente> list){
        mPatientList = list;
    }

    public void setCurrentPage(int pageNumber){
        mCurrentPage = pageNumber;
    }
}

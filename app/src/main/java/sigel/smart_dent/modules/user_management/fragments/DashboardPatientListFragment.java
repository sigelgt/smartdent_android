package sigel.smart_dent.modules.user_management.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import sigel.smart_dent.R;
import sigel.smart_dent.models.Paciente;
import sigel.smart_dent.modules.patient_management.patients.PacienteAgregar;
import sigel.smart_dent.modules.user_management.adapters.DashboardPatientListAdapter;

/**
 * Created by Juan on 10/07/2016.
 */
public class DashboardPatientListFragment extends Fragment {

    private int fragmentTestId;


    private ArrayList<Paciente> patientList = new ArrayList<Paciente>();

    public interface DashboarPatientListInterface {
        void onPatientSelect(int position);
    }

    public DashboarPatientListInterface mListener;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        mListener = (DashboarPatientListInterface) getActivity();
        View view = inflater.inflate(R.layout.fragment_generic_recyclerview, container, false);
        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.genericRecyclerView);
        DashboardPatientListAdapter dashboardPatientListAdapter = new DashboardPatientListAdapter(mListener,patientList);
        recyclerView.setAdapter(dashboardPatientListAdapter);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);

        FloatingActionButton fab = (FloatingActionButton) view.findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getContext(), PacienteAgregar.class);
                startActivity(intent);
            }
        });
        return view;
    }

    public void setPatientList(ArrayList<Paciente> list){
        this.patientList = list;
    }

    public void setFragmentTestId (int id) {
        this.fragmentTestId = id;
    }
}

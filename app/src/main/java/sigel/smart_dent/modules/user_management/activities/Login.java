package sigel.smart_dent.modules.user_management.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import sigel.smart_dent.R;
import sigel.smart_dent.modules.user_management.Plan;
import sigel.smart_dent.modules.user_management.Usuario;
import sigel.smart_dent.modules.user_management.UsuarioDatos;
import sigel.smart_dent.rest.APIPlug;
import sigel.smart_dent.rest.ServiceGenerator;
import sigel.smart_dent.syn.Syn;
import sigel.smart_dent.tools.Tools;


public class Login extends AppCompatActivity {

    Typeface fontAvenirMedium;
    Button btnLogin;
    EditText txtEmail, txtPass;
    Button mButtonCreateAccount;

    Tools tools;
    Syn syn;

    private GoogleApiClient client;

    APIPlug cliente = ServiceGenerator.createService(APIPlug.class);
    private static final String PREFS_FILE = "sigel.smart_dent.preferences";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        fontAvenirMedium = Typeface.createFromAsset(getAssets(), "Avenir-Medium.ttf");

        tools = new Tools();

        syn = new Syn();

        if (Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

        txtEmail = (EditText) findViewById(R.id.txtEmail);
        txtPass = (EditText) findViewById(R.id.txtPass);
        btnLogin = (Button) findViewById(R.id.btnLogin);
        mButtonCreateAccount = (Button) findViewById(R.id.createAccountButton);

        txtEmail.setTypeface(fontAvenirMedium);
        txtPass.setTypeface(fontAvenirMedium);
        btnLogin.setTypeface(fontAvenirMedium);
        mButtonCreateAccount.setTypeface(fontAvenirMedium);

       if (syn.validateSession(this.getBaseContext())){
            Intent intent = new Intent(Login.this, Dashboard.class);
            startActivity(intent);
        }


        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String email = txtEmail.getText().toString();
                String pass = tools.md5(txtPass.getText().toString());

                email = "demo@smartdent.gt";
                pass = tools.md5("Juan");

                if (!tools.isOnline(getApplicationContext())) {
                    Toast.makeText(getApplication(), "Verifique su acceso a internet", Toast.LENGTH_SHORT).show();
                } else {
                    if((email != null) && (pass != null)){
                        login(email, pass,getApplicationContext());
                    }else{
                        Toast.makeText(getBaseContext(), "Por favor ingrese sus datos", Toast.LENGTH_SHORT).show();
                    }
                }

            }
        });

        mButtonCreateAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Login.this, Registro.class);
                startActivity(intent);
            }
        });

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
    }

    @Override
    public void onStart() {
        super.onStart();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client.connect();
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "Login Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app deep link URI is correct.
                Uri.parse("android-app://sigel.smart_dent/http/host/path")
        );
        AppIndex.AppIndexApi.start(client, viewAction);
    }

    @Override
    public void onStop() {
        super.onStop();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "Login Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app deep link URI is correct.
                Uri.parse("android-app://sigel.smart_dent/http/host/path")
        );
        AppIndex.AppIndexApi.end(client, viewAction);
        client.disconnect();
    }


    @Override
    protected void onResume(){
        super.onResume();

        if (syn.validateSession(getApplicationContext())){
            Intent intent = new Intent(Login.this, Dashboard.class);
            startActivity(intent);
        }
    }
    public void login(String email, String password, Context context) {
        tools = new Tools();

        final SharedPreferences.Editor editorSharedPref = context.getSharedPreferences(PREFS_FILE, context.MODE_PRIVATE).edit();

        Call<Usuario> call = cliente.loginUser(email, password);

        call.enqueue(new Callback<Usuario>() {

            @Override
            public void onResponse(Call<Usuario> call, Response<Usuario> response) {
                try {
                    if (response.body().getMessage().equals("active")) {
                        UsuarioDatos user = new UsuarioDatos();
                        Plan plans = new Plan();

                        user.set_id(response.body().getData().get_id());
                        user.setCity(response.body().getData().getCity());
                        user.setAddress(response.body().getData().getAddress());
                        user.setPostalCode(response.body().getData().getPostalCode());
                        user.setDateBirth(response.body().getData().getDateBirth());
                        user.setName(response.body().getData().getName());
                        user.setDocumentIdentification(response.body().getData().getDocumentIdentification());
                        user.setNumberDocument(response.body().getData().getNumberDocument());
                        user.setEmail(response.body().getData().getEmail());
                        user.setProfession(response.body().getData().getProfession());
                        user.setNationality(response.body().getData().getNationality());
                        user.setProvince(response.body().getData().getProvince());
                        user.setTelephone(response.body().getData().getTelephone());
                        user.setPassword(response.body().getData().getPassword());
                        user.setLastName(response.body().getData().getLastName());
                        user.setUser(response.body().getData().getUser());

//                        plans.setId(response.body().getData().getPlanes().getId());
//                        plans.setDatePurchase(response.body().getData().getPlanes().getDatePurchase());
//                        plans.setFinishDate(response.body().getData().getPlanes().getFinishDate());
//                        plans.setRecordLimit(response.body().getData().getPlanes().getRecordLimit());
//                        plans.setValidityDays(response.body().getData().getPlanes().getValidityDays());


                        Boolean sesion = true;

                        editorSharedPref.putString("_id", user.get_id());
                        editorSharedPref.putString("city", user.getCity());
                        editorSharedPref.putString("address", user.getAddress());
                        editorSharedPref.putString("postalCode", user.getPostalCode());
                        editorSharedPref.putString("dateBirth", user.getDateBirth());
                        editorSharedPref.putString("name", user.getName());
                        editorSharedPref.putString("documentIdentification", user.getDocumentIdentification());
                        editorSharedPref.putString("numberDocument", user.getNumberDocument());
                        editorSharedPref.putString("email", user.getEmail());
                        editorSharedPref.putString("profession", user.getProfession());
                        editorSharedPref.putString("nationality", user.getNationality());
                        editorSharedPref.putString("province", user.getProvince());
                        editorSharedPref.putString("telephone", user.getTelephone());
                        editorSharedPref.putString("password", user.getPassword());
                        editorSharedPref.putString("lastName", user.getLastName());
                        editorSharedPref.putString("user", user.getUser());

//                        editorSharedPref.putString("planFinishDate", plans.getFinishDate());
//                        editorSharedPref.putString("planRecordLimit", plans.getRecordLimit());
//                        editorSharedPref.putString("planValidityDays", plans.getValidityDays());
//                        editorSharedPref.putString("planID", plans.getId());

                        editorSharedPref.putBoolean("sesion", sesion);

                        editorSharedPref.commit();

                        Intent intent = new Intent(Login.this, Dashboard.class);
                        startActivity(intent);
                        finish();
                    }
                }catch(Exception e){
                    Log.e("ERROR! catch", e.toString());
                }
            }

            @Override
            public void onFailure(Call<Usuario> call, Throwable t) {
                Log.e("FAILURE!", t.toString());
                Toast.makeText(getBaseContext(), "Por favor verifique sus datos", Toast.LENGTH_SHORT).show();
            }
        });
    }
}

package sigel.smart_dent.modules.user_management;

/**
 * Created by pablin on 7/07/2016.
 */
public class Usuario {

    private String message;
    private UsuarioDatos data, ops;


    public String getMessage() {return message;}

    public void setMessage(String message) {
        this.message = message;
    }

    public UsuarioDatos getOps() {return ops;}

    public void setOps(UsuarioDatos ops) {this.ops = ops;}

    // public void setMessage(String message) {this.message = message;}

    public UsuarioDatos getData() {
        return data;
    }

    public void setData(UsuarioDatos data) {
        this.data = data;
    }
}

package sigel.smart_dent.modules.user_management.activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import sigel.smart_dent.R;
import sigel.smart_dent.models.Paciente;
import sigel.smart_dent.modules.patient_management.patients.PacienteAgregar;
import sigel.smart_dent.modules.user_management.fragments.DashboardPatientListFragment;
import sigel.smart_dent.modules.user_management.fragments.DashboardViewPagerFragment;
import sigel.smart_dent.rest.APIPlug;
import sigel.smart_dent.rest.ServiceGenerator;
import sigel.smart_dent.service.NetworkConnectivityCheck;
import sigel.smart_dent.sqlite.OperationsDB;
import sigel.smart_dent.syn.Syn;


/**
 * Created by Juan on 6/07/2016.
 */
public class Dashboard extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, DashboardPatientListFragment.DashboarPatientListInterface {

    private NetworkConnectivityCheck networkConnectivityCheck;

    public Typeface fontAvenirMedium;
    public Typeface fontAvenirLight;
    public ActionBarDrawerToggle mToggle;

    public static ArrayList<Paciente> patientList = new ArrayList<>();
    public static String isOnline;

    static List<Paciente> pacientesList;
    static OperationsDB operationsDB;
    static String idUsuario;

    DashboardViewPagerFragment viewPager;
    DashboardViewPagerFragment savedFragment;
    Syn syn;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getActivityLayout());

        viewPager = new DashboardViewPagerFragment();
        savedFragment = (DashboardViewPagerFragment) getSupportFragmentManager().findFragmentByTag("DASHBOARDPAGER");

        isOnline = "";

        syn =  new Syn();

        operationsDB = OperationsDB.obtenerInstancia(getApplicationContext());

        idUsuario = syn.userId(getApplicationContext());

        networkConnectivityCheck = new NetworkConnectivityCheck();
        networkConnectivityCheck.register(this);

        this.setAssets();
        this.setLayoutMenus();

        if (isOnline == "ONLINE"){
            listPatientsService();
        }else {
            listPatientsDB();
        }
        dashboardViewPagerFragment();

        setTitle("SmartDent");
    }

    @Override
    protected void onResume() {
        super.onResume();
        networkConnectivityCheck.register(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        networkConnectivityCheck.unregister(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        //networkConnectivityCheck.unregister(this);
    }

    public void dashboardViewPagerFragment(){
        if(savedFragment==null) {
            Bundle bundle = new Bundle();
            bundle.putInt("current",getIntent().getIntExtra("currentPage",0));
            viewPager.setArguments(bundle);
            viewPager.setPatientList(patientList);
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.add(R.id.dashboardPlaceHolder, viewPager, "DASHBOARDPAGER");
            fragmentTransaction.commit();
        }
    }

    public void setAssets(){
        fontAvenirMedium = Typeface.createFromAsset(getAssets(), "Avenir-Medium.ttf");
        fontAvenirLight = Typeface.createFromAsset(getAssets(), "Avenir-Light.ttf");
        overrideFonts(this, findViewById(getActivityLayout()) );
    }

    public void setLayoutMenus(){
        Toolbar toolbar = (Toolbar) findViewById(R.id.dashboard_toolbar);
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.dashboard_drawer_layout);
        mToggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(mToggle);
        mToggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.dashboard_nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    public void overrideFonts(final Context context, final View v) {
        try {
            if (v instanceof ViewGroup) {
                ViewGroup vg = (ViewGroup) v;
                for (int i = 0; i < vg.getChildCount(); i++) {
                    View child = vg.getChildAt(i);
                    overrideFonts(context, child);
                }
            } else if (v instanceof TextView) {
                //((TextView) v).setTypeface(Typeface.createFromAsset(context.getAssets(), "font.ttf"));
                ((TextView) v).setTypeface(fontAvenirMedium);
            } else if (v instanceof Switch){
                ((Switch) v).setTypeface(fontAvenirMedium);
            } else if(v instanceof EditText){
                ((EditText) v).setTypeface(fontAvenirMedium);
            } else if (v instanceof RadioButton){
                ((RadioButton) v).setTypeface(fontAvenirMedium);
            } else if(v instanceof CheckBox){
                ((CheckBox) v).setTypeface(fontAvenirMedium);
            } else if(v instanceof Spinner){
                //((Spinner) v).setTypeface(fontAvenirMedium);
            }
        } catch (Exception e) {
        }
    }

    protected int getActivityLayout(){
        return R.layout.activity_dashboard;
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.menu_paciente_listar_detalle, menu);
        return true;
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {

        int id = item.getItemId();
        if (id == R.id.paciente_listado) {
            Intent intent = new Intent(this, Dashboard.class);
            intent.putExtra("currentPage",1);
            startActivity(intent);
        } else if (id == R.id.usuario_dashboard) {
            mToggle.syncState();
            //startActivity(new Intent(this, Dashboard.class));
        } else if (id == R.id.salir) {
            syn.closeSession(getBaseContext());
            startActivity(new Intent(this, Login.class));
        } else {
            //Toast.makeText(getBaseContext(), "OTRA OPCION", Toast.LENGTH_SHORT).show();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.dashboard_drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onPatientSelect(int position) {
        Intent intent = new Intent(Dashboard.this, PacienteAgregar.class);
        Paciente pac = patientList.get(position);
        intent.putExtra(PacienteAgregar.EXTRA_PACIENTE,pac);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }
    public static void listPatientsDB(){
        patientList = new ArrayList<>(operationsDB.obtenerPatients());
    }

    public static void listPatientsService(){

        patientList = new ArrayList<>();

        APIPlug cliente = ServiceGenerator.createService(APIPlug.class);

        Call<List<Paciente>> call = cliente.getPacientes(idUsuario);

        call.enqueue(new Callback<List<Paciente>>() {
            @Override
            public void onResponse(Call<List<Paciente>> call, Response<List<Paciente>> response) {

                pacientesList = response.body();
                for (int x = 0; x < pacientesList.size(); x++) {
                    operationsDB.deletePatients(pacientesList.get(x).get_id().toString());
                    Paciente paciente = new Paciente();
                    paciente.set_id(pacientesList.get(x).get_id().toString());
                    paciente.set_id_doctor(pacientesList.get(x).get_id_doctor().toString());
                    paciente.setImagen(pacientesList.get(x).getImagen().toString());
                    paciente.setNombres(pacientesList.get(x).getNombres().toString());
                    paciente.setApellidos(pacientesList.get(x).getApellidos().toString());
                    paciente.setFecha_nacimiento(pacientesList.get(x).getFecha_nacimiento().toString());
                    paciente.setEdad(pacientesList.get(x).getEdad().toString());
                    paciente.setGenero(pacientesList.get(x).getGenero().toString());
                    paciente.setNacionalidad(pacientesList.get(x).getNacionalidad().toString());
                    paciente.setEstado_civil(pacientesList.get(x).getEstado_civil().toString());
                    paciente.setDireccion(pacientesList.get(x).getDireccion().toString());
                    paciente.setEmail(pacientesList.get(x).getEmail().toString());
                    paciente.setOcupacion(pacientesList.get(x).getOcupacion().toString());
                    paciente.setNumero_telefono(pacientesList.get(x).getNumero_telefono().toString());
                    paciente.setReferido_por(pacientesList.get(x).getReferido_por().toString());
                    paciente.setContacto(pacientesList.get(x).getContacto().toString());
                    paciente.setContacto_numero_telefono(pacientesList.get(x).getContacto_numero_telefono().toString());
                    paciente.setConsideraciones_medicas(pacientesList.get(x).getConsideraciones_medicas().toString());
                    paciente.setMotivo_consulta(pacientesList.get(x).getMotivo_consulta().toString());
                    paciente.setEstado("0");

                    patientList.add(paciente);

                    operationsDB.insertPatients(paciente,paciente.get_id());
                }
            }

            @Override
            public void onFailure(Call<List<Paciente>> call, Throwable t) {
                Log.e("onFailure Pacientes",t.toString());
            }
        });
    }
}

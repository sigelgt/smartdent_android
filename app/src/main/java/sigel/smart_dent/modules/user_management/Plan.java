package sigel.smart_dent.modules.user_management;

/**
 * Created by juanlopez on 7/22/16.
 */
public class Plan {
    private String id,datePurchase,finishDate,validityDays,recordLimit;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDatePurchase() {
        return datePurchase;
    }

    public void setDatePurchase(String datePurchase) {
        this.datePurchase = datePurchase;
    }

    public String getFinishDate() {
        return finishDate;
    }

    public void setFinishDate(String finishDate) {
        this.finishDate = finishDate;
    }

    public String getValidityDays() {
        return validityDays;
    }

    public void setValidityDays(String validityDays) {
        this.validityDays = validityDays;
    }

    public String getRecordLimit() {
        return recordLimit;
    }

    public void setRecordLimit(String recordLimit) {
        this.recordLimit = recordLimit;
    }
}

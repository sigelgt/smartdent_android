package sigel.smart_dent.modules.user_management.activities;

import android.app.DialogFragment;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;
import com.weiwangcn.betterspinner.library.material.MaterialBetterSpinner;


import sigel.smart_dent.Planes;
import sigel.smart_dent.R;
import sigel.smart_dent.tools.DatePickerFragment;
import sigel.smart_dent.tools.Tools;


public class Registro extends AppCompatActivity {

    String[] SPINNERLIST = {"Guatemala", "El Salvador", "Mexico", "Honduras"};

    EditText txtusuario;
    EditText txtpass;
    EditText txtconfirmar_pass;
    EditText txtnombre;
    EditText txtapellido;
    EditText txtfecha_nacimiento;
    Spinner  txttipo_identificación;
    EditText txtnumero_identificacion;
    EditText txtprofesion;
    EditText txtdireccion;
    EditText txtciudad;
    EditText txtprovincia;
    EditText txtcodigo_postal;
    EditText txttelefono;

    Button btnregistro;
    Tools tools;

    MaterialBetterSpinner materialDesignSpinner;

    private static final String PREFS_FILE = "sigel.smart_dent.preferences";

    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);


        setTitle("Crear Cuenta");
        tools = new Tools();


        if (Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

        //Guardar Info de registro
        txtusuario = (EditText) findViewById(R.id.txtusuario);
        txtpass = (EditText) findViewById(R.id.txtpass);
        txtconfirmar_pass = (EditText) findViewById(R.id.txtconfirmar_pass);
        txtnombre = (EditText) findViewById(R.id.txtnombre);
        txtapellido = (EditText) findViewById(R.id.txtapellido);
        txtfecha_nacimiento = (EditText) findViewById(R.id.fecha_nacimiento_re);
        txtprofesion = (EditText) findViewById(R.id.txtprofesion);
        txtdireccion = (EditText) findViewById(R.id.txtdireccion);
        txtciudad = (EditText) findViewById(R.id.txtciudad);
        txtprovincia = (EditText) findViewById(R.id.txtprovincia);
        txtcodigo_postal = (EditText) findViewById(R.id.txtcodigo_postal);
        txttelefono = (EditText) findViewById(R.id.txttelefono);

        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(this,
                android.R.layout.simple_dropdown_item_1line, SPINNERLIST);
        materialDesignSpinner = (MaterialBetterSpinner)
                findViewById(R.id.android_material_design_spinner);
        materialDesignSpinner.setAdapter(arrayAdapter);

        txtfecha_nacimiento.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDatePickerDialog();
            }
        });

        //Link para layout registro
        btnregistro = (Button) findViewById(R.id.btnregistro);

        btnregistro.setOnClickListener(new View.OnClickListener() {


            @Override
            public void onClick(View v) {

                String usuario = txtusuario.getText().toString();
                String email = txtusuario.getText().toString();
                String pass = txtpass.getText().toString();
                String confirmar_pass = txtconfirmar_pass.getText().toString();
                String nombre = txtnombre.getText().toString();
                String apellido = txtapellido.getText().toString();
                String fecha_nacimiento = txtfecha_nacimiento.getText().toString();
                String nacionalidad = materialDesignSpinner.toString();
                String profesion = txtprofesion.getText().toString();
                String dirección = txtdireccion.getText().toString();
                String ciudad = txtciudad.getText().toString();
                String provincia = txtprovincia.getText().toString();
                String codigo_postal = txtcodigo_postal.getText().toString();
                String telefono = txttelefono.getText().toString();

                Intent intent;
                intent = new Intent(Registro.this, Planes.class);


                intent.putExtra("usuario", usuario);
                intent.putExtra("pass", pass);
                intent.putExtra("email", email);
                intent.putExtra("confirmar_pass", confirmar_pass);
                intent.putExtra("nombre", nombre);
                intent.putExtra("apellido", apellido);
                intent.putExtra("fecha_nacimiento", fecha_nacimiento);
                intent.putExtra("nacionalidad", nacionalidad);
                intent.putExtra("profesion", profesion);
                intent.putExtra("dirección", dirección);
                intent.putExtra("ciudad", ciudad);
                intent.putExtra("provincia", provincia);
                intent.putExtra("codigo_postal", codigo_postal);
                intent.putExtra("telefono", telefono);

                startActivity(intent);
            }
        });



        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client2 = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
    }
    public void showDatePickerDialog() {
        DialogFragment newFragment = new DatePickerFragment();
        newFragment.show(getFragmentManager(), "datePicker");
    }



    @Override
    public void onStart() {
        super.onStart();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client2.connect();
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "Registro Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app deep link URI is correct.
                Uri.parse("android-app://sigel.smart_dent/http/host/path")
        );
        AppIndex.AppIndexApi.start(client2, viewAction);
    }

    @Override
    public void onStop() {
        super.onStop();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "Registro Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app deep link URI is correct.
                Uri.parse("android-app://sigel.smart_dent/http/host/path")
        );
        AppIndex.AppIndexApi.end(client2, viewAction);
        client2.disconnect();
    }
}

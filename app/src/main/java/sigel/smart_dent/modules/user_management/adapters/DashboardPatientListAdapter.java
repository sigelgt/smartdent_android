package sigel.smart_dent.modules.user_management.adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import sigel.smart_dent.R;
import sigel.smart_dent.models.Paciente;
import sigel.smart_dent.modules.user_management.fragments.DashboardPatientListFragment;

/**
 * Created by Juan on 10/07/2016.
 */
public class DashboardPatientListAdapter extends RecyclerView.Adapter {
    private DashboardPatientListFragment.DashboarPatientListInterface mListener;
    private ArrayList<Paciente> mPacientList = new ArrayList<>();

    Context mCtx;

    public Typeface fontAvenirMedium;
    public Typeface fontAvenirLight;
    public Typeface fontAvenirBook;

    public DashboardPatientListAdapter(DashboardPatientListFragment.DashboarPatientListInterface listener, ArrayList<Paciente> list ){
        mListener = listener;
        this.mPacientList = list;
        //notifyItemRangeChanged(0,mPacientList.size());
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        mCtx = parent.getContext();
        View view = LayoutInflater.from(mCtx).inflate(R.layout.patients_list_item, parent, false);

        fontAvenirMedium = Typeface.createFromAsset(mCtx.getAssets(), "Avenir-Medium.ttf");
        fontAvenirLight = Typeface.createFromAsset(mCtx.getAssets(), "Avenir-Light.ttf");
        fontAvenirBook = Typeface.createFromAsset(mCtx.getAssets(), "Avenir-Book.ttf");

        return new DashboardPatientListViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ((DashboardPatientListViewHolder) holder).bindView(position);
    }

    @Override
    public int getItemCount() {
        return mPacientList.size();
    }


    private class DashboardPatientListViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private TextView mPatientName;
        private int mPosition;

        public DashboardPatientListViewHolder(View itemView) {
            super(itemView);
            mPatientName = (TextView) itemView.findViewById(R.id.patientNameLabel);
            mPatientName.setTypeface(fontAvenirMedium);
            itemView.setOnClickListener(this);
        }

        public void bindView (int position){
            mPosition = position;
            String patientName = mPacientList.get(position).getNombres() + " " + mPacientList.get(position).getApellidos() ;
            //String patientName = "Patient Name";
            mPatientName.setText(patientName);
        }

        @Override
        public void onClick(View v) {
            onPatientSelect(mPosition, v);
        }
    }

    public void onPatientSelect(int position, View view){
        mListener.onPatientSelect(position);
    }
}

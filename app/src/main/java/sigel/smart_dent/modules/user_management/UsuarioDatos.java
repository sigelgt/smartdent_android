package sigel.smart_dent.modules.user_management;


/**
 * Created by pablin on 7/07/2016.
 */
public class UsuarioDatos {

    private String _id,city, address, postalCode, dateBirth, name, documentIdentification, numberDocument, email, profession, nationality, province, telephone,password,confirm_password,lastName,user,id_planes;

    private boolean estado;

    private Plan planes;

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getDateBirth() {
        return dateBirth;
    }

    public void setDateBirth(String dateBirth) {
        this.dateBirth = dateBirth;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDocumentIdentification() {
        return documentIdentification;
    }

    public void setDocumentIdentification(String documentIdentification) {
        this.documentIdentification = documentIdentification;
    }

    public String getNumberDocument() {
        return numberDocument;
    }

    public void setNumberDocument(String numberDocument) {
        this.numberDocument = numberDocument;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getProfession() {
        return profession;
    }

    public void setProfession(String profession) {
        this.profession = profession;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setConfirm_Password(String confirm_password) {
        this.confirm_password = confirm_password;
    }
    public String getConfirm_Password() {
        return confirm_password;
    }


    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getId_planes() {
        return id_planes;
    }

    public void setId_planes(String id_planes) {
        this.id_planes = id_planes;
    }

    public boolean isEstado() {
        return estado;
    }

    public void setEstado(boolean estado) {
        this.estado = estado;
    }

    public Plan getPlanes() {
        return planes;
    }

    // public Plan getPlanes() {return planes;}

   // public void setPlanes(Plan planes) {this.planes = planes;}

}

package sigel.smart_dent.modules.clinic_management;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import sigel.smart_dent.R;

public class articulo extends AppCompatActivity {

    Button crear_nuevo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_articulo);
       /* crear = (Button) findViewById(R.id.button);
                Intent intent = new Intent(articulo.this, articulos.class);

        startActivity(intent);*/
        crear_nuevo = (Button) findViewById(R.id.btn_nuevo);

        crear_nuevo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Creamos el intent
                Intent intent = new Intent(articulo.this, articulos.class);

                //Iniciamos la nueva actividad
                startActivity(intent);

            }
        });

    }
}

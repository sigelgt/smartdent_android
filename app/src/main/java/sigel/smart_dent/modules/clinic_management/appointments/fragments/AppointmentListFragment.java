package sigel.smart_dent.modules.clinic_management.appointments.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import sigel.smart_dent.R;
import sigel.smart_dent.modules.clinic_management.appointments.adapters.AppointmentAdapter;

/**
 * Created by Juan on 11/07/2016.
 */
public class AppointmentListFragment extends Fragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_generic_recyclerview, container, false);
        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.genericRecyclerView);
        AppointmentAdapter appointmentAdapter = new AppointmentAdapter();
        recyclerView.setAdapter(appointmentAdapter);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);

        FloatingActionButton fab = (FloatingActionButton) view.findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Snackbar.make(view, "Actividad en construccion", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
        return view;
    }
}

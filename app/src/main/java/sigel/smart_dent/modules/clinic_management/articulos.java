package sigel.smart_dent.modules.clinic_management;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import android.widget.ImageView;

import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;

import java.io.File;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.GsonConverterFactory;
import retrofit2.Retrofit;
import sigel.smart_dent.R;
import sigel.smart_dent.beans.Response;
import sigel.smart_dent.interfaces.postarticulo;

public class articulos extends AppCompatActivity{

    private String APP_DIRECTORY = "myPictureApp";
    private String MEDIA_DIRECTORY = APP_DIRECTORY + "media";
    private String TEMPORAL_PICTURE_NAME = "temporal.jpg";

    private final int PHOTO_CODE = 100;
    private final int SELECT_PICTURE = 200;

    private ImageView imagenView;


    EditText txtnombre;
    EditText txtdescripcion;
    EditText txtusuario_id;
    EditText txtproveedor_id;
    EditText txtfecha_compra;
    EditText txtdocumento;
    EditText txtfecha_expiracion;
    EditText txtcantidad;
    EditText txtexistencias;
    EditText txtvalor_total;
    EditText txtvalor_unitario;
    EditText txtfecha_creacion;
    Button btnregistro;

    private GoogleApiClient articulo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_articulos);



        if (Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }


        //Guardar Info de registro
        txtnombre = (EditText) findViewById(R.id.txtnombre);
        txtdescripcion = (EditText) findViewById(R.id.txtdescripcion);
        txtusuario_id = (EditText) findViewById(R.id.txtusuario_id);
        txtproveedor_id = (EditText) findViewById(R.id.txtproveedor_id);
        txtfecha_compra = (EditText) findViewById(R.id.txtfecha_compra);
        txtdocumento = (EditText) findViewById(R.id.txtdocumento);
        txtfecha_expiracion= (EditText) findViewById(R.id.txtfecha_expiracion);
        txtcantidad = (EditText) findViewById(R.id.txtcantidad);
        txtexistencias = (EditText) findViewById(R.id.txtexistencias);
        txtvalor_total = (EditText) findViewById(R.id.txtvalor_total);
        txtvalor_unitario = (EditText) findViewById(R.id.txtvalor_unitario);
        txtfecha_creacion = (EditText) findViewById(R.id.txtfecha_creacion);
        //Link para layout registro



        btnregistro = (Button) findViewById(R.id.btnregistro);
        btnregistro.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick (View v){

                Log.i("trace", "en el evento on click");

                String nombre = txtnombre.getText().toString();
                String descripcion = txtdescripcion.getText().toString();
                String usuario_id = txtusuario_id.getText().toString();
                String proveedor_id = txtproveedor_id.getText().toString();
                String fecha_compra = txtfecha_compra.getText().toString();
                String documento = txtdocumento.getText().toString();
                String fecha_expiracion = txtfecha_expiracion.getText().toString();
                String cantidad = txtcantidad.getText().toString();
                String existencias = txtexistencias.getText().toString();
                String valor_total = txtvalor_total.getText().toString();
                String valor_unitario = txtvalor_unitario.getText().toString();
                String fecha_creacion = txtfecha_creacion.getText().toString();

                Log.i("trace","ya obtube todos los valores");

                if (!isOnline()) {
                    Log.i("trace","NO TIENE INTERNET");
                    Toast.makeText(getApplication(), "Verifique su acceso a internet", Toast.LENGTH_SHORT).show();
                } else {
                    Log.i("trace","SI TIENE INTERNET");
                    articulo1(nombre, descripcion, usuario_id, proveedor_id, fecha_compra, documento, fecha_expiracion, cantidad, existencias, valor_total, valor_unitario, fecha_creacion);
                }


                //Creamos el intent
                 Intent intent = new Intent(articulos.this,articulo.class);

                //Iniciamos la nueva actividad
                  startActivity(intent);

            }
        });

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        articulo = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();

        imagenView = (ImageView) findViewById(R.id.setPicture);
        Button button = (Button) findViewById(R.id.btn_captura);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final CharSequence[] options = {"Tomar foto", "Elegir de galeria", "Cancelar"};
                final AlertDialog.Builder builder = new AlertDialog.Builder(articulos.this);
                builder.setTitle("Elige una opcion :D");
                builder.setItems(options, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int seleccion) {
                        if (options[seleccion] == "Tomar foto") {
                            openCamera();
                        } else if (options[seleccion] == "Elegir de galeria") {
                            Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                            intent.setType("image/*");
                            startActivityForResult(intent.createChooser(intent, "Selecciona app de imagen"), SELECT_PICTURE);
                        } else if (options[seleccion] == "Cancelar") {
                            dialog.dismiss();
                        }
                    }
                });
                builder.show();
            }
        });


    }

    private void openCamera() {
        File file = new File(Environment.getExternalStorageDirectory(), MEDIA_DIRECTORY);
        file.mkdirs();

        String path = Environment.getExternalStorageDirectory() + File.separator + MEDIA_DIRECTORY + File.separator + TEMPORAL_PICTURE_NAME;

        File newFile = new File(path);

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(newFile));
        startActivityForResult(intent, PHOTO_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode){
            case PHOTO_CODE:
                if(resultCode == RESULT_OK){
                    String dir = Environment.getExternalStorageDirectory() + File.separator
                            + MEDIA_DIRECTORY + File.separator + TEMPORAL_PICTURE_NAME;
                    decodeBitmap(dir);
                }
                break;

            case SELECT_PICTURE:
                if (resultCode == RESULT_OK){
                    Uri path = data.getData();
                    imagenView.setImageURI(path);
                }
        }
    }

    private void decodeBitmap(String dir) {
        Bitmap bitmap;
        bitmap = BitmapFactory.decodeFile(dir);

        imagenView.setImageBitmap(bitmap);
    }

    public void articulo1(String nombre, String descripcion, String usuario_id, String proveedor_id, String fecha_compra, String documento, String fecha_expiracion, String cantidad, String existencias, String valor_total, String valor_unitario, String fecha_creacion) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://tigoembajador.cloudapp.net:3000/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        postarticulo postInterface = retrofit.create(postarticulo.class);

        Call<Response> call = postInterface.insertUser(nombre, descripcion, usuario_id, proveedor_id, fecha_compra, documento, fecha_expiracion, cantidad, existencias, valor_total,valor_unitario, fecha_creacion);

        call.enqueue(new Callback<Response>() {
            @Override
            public void onResponse(Call<sigel.smart_dent.beans.Response> call, retrofit2.Response<Response> response) {
                Log.e("onResponse", response.body().getInsertedCount().toString());
            }

            @Override
            public void onFailure(Call<sigel.smart_dent.beans.Response> call, Throwable t) {
                Log.e("onFailure", t.toString());
            }
        });

    }

    public boolean isOnline() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getApplication().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnectedOrConnecting()) {
            return true;
        } else {
            return false;
        }
    }


    @Override
    public void onStart() {
        super.onStart();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        articulo.connect();
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "Registro Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app deep link URI is correct.
                Uri.parse("android-app://sigel.smart_dent/http/host/path")
        );
        AppIndex.AppIndexApi.start(articulo, viewAction);
    }

    @Override
    public void onStop() {
        super.onStop();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "Registro Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app deep link URI is correct.
                Uri.parse("android-app://sigel.smart_dent/http/host/path")
        );
        AppIndex.AppIndexApi.end(articulo, viewAction);
        articulo.disconnect();
    }



}


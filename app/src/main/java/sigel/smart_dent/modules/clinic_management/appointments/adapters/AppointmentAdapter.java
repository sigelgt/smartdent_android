package sigel.smart_dent.modules.clinic_management.appointments.adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import sigel.smart_dent.R;
import sigel.smart_dent.modules.patient_management.patients.models.AppointmentsExample;

/**
 * Created by Juan on 11/07/2016.
 */
public class AppointmentAdapter extends RecyclerView.Adapter {

    Context mCtx;

    public Typeface fontAvenirMedium;
    public Typeface fontAvenirLight;
    public Typeface fontAvenirBook;

    public AppointmentAdapter(){

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        mCtx = parent.getContext();
        View view = LayoutInflater.from(mCtx).inflate(R.layout.appointment_list_item, parent, false);

        fontAvenirMedium = Typeface.createFromAsset(mCtx.getAssets(), "Avenir-Medium.ttf");
        fontAvenirLight = Typeface.createFromAsset(mCtx.getAssets(), "Avenir-Light.ttf");
        fontAvenirBook = Typeface.createFromAsset(mCtx.getAssets(), "Avenir-Book.ttf");

        return new AppointmentListViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ((AppointmentListViewHolder) holder).bindView(position);
    }

    @Override
    public int getItemCount() {
        return AppointmentsExample.appointmentIds.length;
    }

    private class AppointmentListViewHolder  extends RecyclerView.ViewHolder implements View.OnClickListener {
        private int mPosition;
        private TextView mProdedureNameLabel;
        private TextView mPatientNameLabel;
        private TextView mAppointmentPlaceLabel;
        private TextView mAppointmentTimeLabel;



        public AppointmentListViewHolder(View itemView) {
            super(itemView);


            mProdedureNameLabel = (TextView) itemView.findViewById(R.id.procedureNameLabel);
            mProdedureNameLabel.setTypeface(fontAvenirLight);

            mPatientNameLabel = (TextView) itemView.findViewById(R.id.patienNameLabel);
            mPatientNameLabel.setTypeface(fontAvenirMedium);

            mAppointmentPlaceLabel = (TextView) itemView.findViewById(R.id.appointmentPlaceLabel);
            mAppointmentPlaceLabel.setTypeface(fontAvenirLight);

            mAppointmentTimeLabel = (TextView) itemView.findViewById(R.id.appointmentTimeLabel);
            mAppointmentTimeLabel.setTypeface(fontAvenirLight);

            itemView.setOnClickListener(this);

        }

        public void bindView (int position){
            mPosition = position;
            mProdedureNameLabel.setText(AppointmentsExample.procedures[position]);
            mPatientNameLabel.setText(AppointmentsExample.names[position]);
            mAppointmentPlaceLabel.setText(AppointmentsExample.places[position]);
            mAppointmentTimeLabel.setText(AppointmentsExample.time[position]);
        }

        @Override
        public void onClick(View v) {
            Toast.makeText(mCtx,"Activity in construction", Toast.LENGTH_SHORT).show();
        }
    }
}

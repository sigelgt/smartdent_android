package sigel.smart_dent.modules.clinic_management;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.GsonConverterFactory;
import retrofit2.Retrofit;
import sigel.smart_dent.R;
import sigel.smart_dent.beans.Response;
import sigel.smart_dent.interfaces.postproveedor;
import sigel.smart_dent.menu;


public class proveedores extends AppCompatActivity {


    EditText txtnombre;
    EditText txtnit;
    EditText txtrepresentante_legal;
    EditText txtcontacto_comercial;
    EditText txttelefono;
    EditText txtdireccion;
    EditText txtemail;
    EditText txtfecha_creacion;

    Button btnregistro;
    //Button back;

    //private GoogleApiClient cliente3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_proveedores);

/*
        if (Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }
*/
        //Guardar Info de proveedores
        txtnombre = (EditText) findViewById(R.id.txtnombre);
        txtnit = (EditText) findViewById(R.id.txtnit);
        txtrepresentante_legal = (EditText) findViewById(R.id.txtrepresentante_legal);
        txtcontacto_comercial = (EditText) findViewById(R.id.txtcontacto_comercial);
        txttelefono = (EditText) findViewById(R.id.txttelefono);
        txtdireccion = (EditText) findViewById(R.id.txtdireccion);
        txtemail = (EditText) findViewById(R.id.txtemail);
        txtfecha_creacion = (EditText) findViewById(R.id.txtfecha_creacion);


        //Link para layout proveedores
        btnregistro = (Button) findViewById(R.id.btnregistro);

        btnregistro.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                Log.i("trace","en el evento on click");

                String nombre = txtnombre.getText().toString();
                String nit = txtnit.getText().toString();
                String representante_legal = txtrepresentante_legal.getText().toString();
                String contacto_comercial = txtcontacto_comercial.getText().toString();
                String telefono = txttelefono.getText().toString();
                String direccion = txtdireccion.getText().toString();
                String email = txtemail.getText().toString();
                String fecha_creacion = txtfecha_creacion.getText().toString();

                Log.i("trace","ya obtube todos los valores");

                if (!isOnline()) {
                    Log.i("trace","NO TIENE INTERNET");
                    Toast.makeText(getApplication(), "Verifique su acceso a internet", Toast.LENGTH_SHORT).show();
                } else {
                    Log.i("trace","SI TIENE INTERNET");
                    crear_proveedor(nombre, nit, representante_legal, contacto_comercial, telefono, direccion, email, fecha_creacion);
                }

                Intent intent = new Intent(proveedores.this, articulo.class);

                //Iniciamos la nueva actividad
                startActivity(intent);

               // Link para layout
               /* back = (Button) findViewById(R.id.btn2);*/

               /* back.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //Crearmos el Intent
                        Intent intent = new Intent(proveedores.this, articulo.class);

                        //Iniciamos la nueva actividad
                        startActivity(intent);
                    }
                });*/

            }
        });

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        //cliente3 = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
    }

    public void crear_proveedor(String nombre, String nit, String representante_legal, String contacto_comercial, String telefono, String direccion, String email, String fecha_creacion) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://tigoembajador.cloudapp.net:3000/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        postproveedor postInterface = retrofit.create(postproveedor.class);

        Call<Response> call = postInterface.insertUser(nombre, nit, representante_legal, contacto_comercial, telefono, direccion, email, fecha_creacion);

        call.enqueue(new Callback<Response>() {
            @Override
            public void onResponse(Call<sigel.smart_dent.beans.Response> call, retrofit2.Response<Response> response) {
                Log.e("onResponse", response.body().getInsertedCount().toString());
                if (response.body().getInsertedCount().equals("active")) {
                    Toast.makeText(getBaseContext(),
                            "Proveedor creado correctamente", Toast.LENGTH_SHORT)
                            .show();
                    //Creamos el intent
                    Intent intent = new Intent(proveedores.this, menu.class);

                    //Iniciamos la nueva actividad
                    startActivity(intent);
                }else {
                    Toast.makeText(getBaseContext(),
                            "Creacion de Proveedor Incorrecto", Toast.LENGTH_SHORT)
                            .show();
                }
            }

            @Override
            public void onFailure(Call<sigel.smart_dent.beans.Response> call, Throwable t) {
                Log.e("onFailure", t.toString());
                Toast.makeText(getBaseContext(),
                        "Creacion de Proveedor Incorrecto", Toast.LENGTH_SHORT)
                        .show();
            }
        });

    }

    public boolean isOnline() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getApplication().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnectedOrConnecting()) {
            return true;
        } else {
            return false;
        }
    }

/*
    @Override
    public void onStart() {
        super.onStart();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        cliente3.connect();
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "Registro Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app deep link URI is correct.
                Uri.parse("android-app://sigel.smart_dent/http/host/path")
        );
        AppIndex.AppIndexApi.start(cliente3, viewAction);
    }

    @Override
    public void onStop() {
        super.onStop();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "Registro Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app deep link URI is correct.
                Uri.parse("android-app://sigel.smart_dent/http/host/path")
        );
        AppIndex.AppIndexApi.end(cliente3, viewAction);
        cliente3.disconnect();
    }
*/
}

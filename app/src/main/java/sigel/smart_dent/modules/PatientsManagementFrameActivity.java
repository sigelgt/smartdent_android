package sigel.smart_dent.modules;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;

import sigel.smart_dent.R;
import sigel.smart_dent.modules.patient_management.appointments.PacienteAgregarCita;
import sigel.smart_dent.modules.patient_management.atm.PacienteATM;
import sigel.smart_dent.modules.patient_management.dental_record.activities.PacienteHistoriaOdontologica;
import sigel.smart_dent.modules.patient_management.endodontics.activities.Endodonthics;
import sigel.smart_dent.modules.patient_management.medical_record.PacienteHistoriaMedica;
import sigel.smart_dent.modules.patient_management.odontogram.activities.OdontogramCanvas;
import sigel.smart_dent.modules.patient_management.odontology.PacienteExamenOrtodoncia;
import sigel.smart_dent.modules.patient_management.plaque.activities.Plaque;
import sigel.smart_dent.modules.patient_management.plaque.activities.PlaqueCanvas;
import sigel.smart_dent.modules.user_management.activities.Dashboard;
import sigel.smart_dent.modules.user_management.activities.Login;

/**
 * Created by Juan on 20/06/2016.
 */
public abstract class PatientsManagementFrameActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    public Typeface fontAvenirMedium;
    public Typeface fontAvenirLight;
    public static String patientId = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        fontAvenirMedium = Typeface.createFromAsset(getAssets(), "Avenir-Medium.ttf");
        fontAvenirLight = Typeface.createFromAsset(getAssets(), "Avenir-Light.ttf");
        overrideFonts(this, findViewById(getActivityLayout()) );
        setContentView(this.getActivityLayout());

        if(getIntent().hasExtra("PATIENT_ID")){
            setPatientId(getIntent().getExtras().getString("PATIENT_ID"));
        }


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.custom_main_menu_icon);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        setTitle(this.setViewTitle());
    }

    public void checkSwitchSection(Switch item, LinearLayout section){
        if(!item.isChecked()){
            section.setVisibility(View.GONE);
        }else{
            section.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_paciente_listar_detalle, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){

        int id = item.getItemId();

        if(id == R.id.paciente_menu_agregar_cita){
            startActivity(linkIntent(this, PacienteAgregarCita.class));
        }else if(id == R.id.paciente_menu_historia_medica){
            startActivity(linkIntent(this, PacienteHistoriaMedica.class));
        }else if(id == R.id.paciente_menu_historia_odontologica){
            startActivity(linkIntent(this, PacienteHistoriaOdontologica.class));
        }else if(id == R.id.paciente_placa_bacteriana){
            Log.e("PLAQUE", "a wilis");
            startActivity(linkIntent(this, PlaqueCanvas.class));
        }else if(id == R.id.paciente_menu_examen_atm){
            startActivity(linkIntent(this, PacienteATM.class));
        }else if(id == R.id.paciente_menu_examen_ortodoncia){
            startActivity(linkIntent(this, PacienteExamenOrtodoncia.class));
        }else if(id == R.id.paciente_menu_examen_endodoncia) {
            startActivity(linkIntent(this, Endodonthics.class));
        }else if(id == R.id.paciente_menu_odontograma){
                startActivity(linkIntent(this, OdontogramCanvas.class));
        }else{
            //Toast.makeText(getBaseContext(), "OTRA OPCION MENU LISTAR OTRA COSA", Toast.LENGTH_SHORT).show();
        }
        return true;
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.paciente_listado) {
            Intent intent = new Intent(this, Dashboard.class);
            intent.putExtra("currentPage",1);
            startActivity(intent);
        } else if (id == R.id.usuario_dashboard) {
            startActivity(new Intent(this, Dashboard.class));
        } else if (id == R.id.salir) {
            startActivity(new Intent(this, Login.class));
        } else {
            //Toast.makeText(getBaseContext(), "OTRA OPCION", Toast.LENGTH_SHORT).show();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    // FUNCIONES ABASTRACTAS PARA IMPLEMENTAR EN CADA FORM

    //Retorno el titulo para mi vista
    public abstract String setViewTitle();

    //Retorno el id del LAuyout de mi vista
    protected abstract int getActivityLayout();


    public void overrideFonts(final Context context, final View v) {
        try {
            if (v instanceof ViewGroup) {
                ViewGroup vg = (ViewGroup) v;
                for (int i = 0; i < vg.getChildCount(); i++) {
                    View child = vg.getChildAt(i);
                    overrideFonts(context, child);
                }
            } else if (v instanceof TextView ) {
                //((TextView) v).setTypeface(Typeface.createFromAsset(context.getAssets(), "font.ttf"));
                ((TextView) v).setTypeface(fontAvenirMedium);
            } else if (v instanceof Switch){
                ((Switch) v).setTypeface(fontAvenirMedium);
            } else if(v instanceof EditText){
                ((EditText) v).setTypeface(fontAvenirMedium);
            } else if (v instanceof RadioButton){
                ((RadioButton) v).setTypeface(fontAvenirMedium);
            } else if(v instanceof CheckBox){
                ((CheckBox) v).setTypeface(fontAvenirMedium);
            } else if(v instanceof Spinner){
                //((Spinner) v).setTypeface(fontAvenirMedium);
            }
        } catch (Exception e) {
        }
    }

    public void setPatientId(String id){
        patientId = id;
    }

    public String getPatientId(){
        return patientId;
    }

    public Intent linkIntent(Context ctx,  Class<?> cls ){
        Intent intent = new Intent(ctx,cls);
        if(this.getPatientId() != null){
            String pid = this.getPatientId();
            intent.putExtra("PATIENT_ID",pid);
        }
        return intent;

    }


}
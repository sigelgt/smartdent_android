package sigel.smart_dent.modules.patient_management.odontology.models;

/**
 * Created by eduardogomez on 13/07/16.
 */

public class PacienteHistoriaOdontologicaModel {

    private Boolean sensibility_hot_beverages,sensibility_sweet_food,sensibility_chewing, labio_superior,sensibility_cold_beverages,sensibility_air, labio_inferior, frenillos,
            paladar, piso_de_boca, mucosa, trauma_oclusal, lengua, color_encias, edema_gingival, inflamacion_gingival, hemorragia_encias, parulis, otros;

    public Boolean getSensibility_hot_beverages(){return sensibility_hot_beverages;}
    public void setSensibility_hot_beverages(Boolean sensibility_hot_beverages) {this.sensibility_hot_beverages = sensibility_hot_beverages;}

    public Boolean getSensibility_sweet_food(){return sensibility_sweet_food;}
    public void setSensibility_sweet_food(Boolean sensibility_sweet_food){this.sensibility_sweet_food = sensibility_sweet_food;}

    public Boolean getSensibility_chewing(){return sensibility_chewing;}
    public void setSensibility_chewing(Boolean sensibility_chewing){this.sensibility_chewing = sensibility_chewing;}

    public Boolean getSensibility_cold_beverages(){return sensibility_cold_beverages;}
    public void setSensibility_cold_beverages(Boolean sensibility_cold_beverages){this.sensibility_cold_beverages = sensibility_cold_beverages;}

    public Boolean getSensibility_air(){return sensibility_air;}
    public void setSensibility_air(Boolean sensibility_air){this.sensibility_air = sensibility_air;}

    public Boolean getLabio_superior(){return labio_superior;}
    public void setLabio_superior(Boolean labio_superior){this.labio_superior = labio_superior;}

    public Boolean getLabio_inferior(){return labio_inferior;}
    public void setLabio_inferior(Boolean labio_inferior){this.labio_inferior = labio_inferior;}

    public Boolean getFrenillos(){return frenillos;}
    public void setFrenillos(Boolean frenillos){this.frenillos = frenillos;}

    public Boolean getPaladar(){return paladar;}
    public void setPaladar(Boolean paladar){this.paladar = paladar;}

    public Boolean getPiso_de_boca(){return piso_de_boca; }
    public void setPiso_de_boca(Boolean piso_de_boca){this.piso_de_boca = piso_de_boca;}

    public Boolean getMucosa(){return mucosa;}
    public void setMucosa(Boolean mucosa){this.mucosa = mucosa;}

    public Boolean getTrauma_oclusal(){return trauma_oclusal;}
    public void setTrauma_oclusal(Boolean trauma_oclusal){this.trauma_oclusal = trauma_oclusal;}

    public Boolean getLengua(){return lengua;}
    public void setLengua(Boolean lengua){this.lengua = lengua;}

    public Boolean getColor_encias(){return color_encias;}
    public void setColor_encias(Boolean color_encias){this.color_encias = color_encias;}

    public Boolean getEdema_gingival(){return edema_gingival;}
    public void setEdema_gingival(Boolean edema_gingival){this.edema_gingival = edema_gingival;}

    public Boolean getInflamacion_gingival(){return inflamacion_gingival;}
    public void setInflamacion_gingival(Boolean inflamacion_gingival){this.inflamacion_gingival = inflamacion_gingival;}

    public Boolean getHemorragia_encias(){return hemorragia_encias;}
    public void setHemorragia_encias(Boolean hemorragia_encias){this.hemorragia_encias = hemorragia_encias;}

    public Boolean getParulis(){return parulis;}
    public void setParulis(Boolean parulis){this.parulis = parulis;}

    public Boolean getOtros(){return otros;}
    public void setOtros(Boolean otros){this.otros = otros;}



}

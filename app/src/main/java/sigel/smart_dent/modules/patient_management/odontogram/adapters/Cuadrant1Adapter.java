package sigel.smart_dent.modules.patient_management.odontogram.adapters;

import sigel.smart_dent.R;
import sigel.smart_dent.modules.patient_management.odontogram.fragments.Cuadrante1Fragment;
import sigel.smart_dent.modules.patient_management.odontogram.models.Theeth;

/**
 * Created by Juan on 16/06/2016.
 */
public class Cuadrant1Adapter extends OdontogramCuadrantsAdapter {

    private final Cuadrante1Fragment.OnOdontogramItemSelectedInterface mListener;

    public Cuadrant1Adapter(Cuadrante1Fragment.OnOdontogramItemSelectedInterface listener) {
        mListener = listener;
    }

    @Override
    public void onItemSelected(int index) {
        mListener.onCudrant1GridItemSelected(index);
    }

    @Override
    public int getBucalResourceId(int position) {
        return Theeth.resourcesId1b[position];
    }

    @Override
    public int getOclusalResourceId(int position) {
        return Theeth.resourcesId1o[position];
    }

    @Override
    public String getIndexName(int position) {
        return Theeth.cudarante1b[position];
    }

    @Override
    public int getLayoutId() {
        return R.layout.item_cuadrant_odontogram;
    }
}

package sigel.smart_dent.modules.patient_management.plaque.adapters;

import android.app.FragmentManager;
import android.content.Context;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import java.util.ArrayList;

import sigel.smart_dent.R;
import sigel.smart_dent.modules.patient_management.plaque.dialogs.PlaqueDialog;
import sigel.smart_dent.modules.patient_management.plaque.models.PlaqueDiagram;
import sigel.smart_dent.modules.patient_management.plaque.models.ThootPlaque;

/**
 * Created by Juan on 1/07/2016.
 */
public class PlaqueAdapter extends RecyclerView.Adapter {

    private String[] mToothList;
    private View thootPlaqueView;
    private Context mContext;
    private FragmentManager mFrMngr;
    private FragmentActivity mFragmentActivity;
    private ArrayList<ThootPlaque> items;
    private ThootPlaque selectedThoot;
    private PlaqueDiagram diagram;
    private int cuadrant;

    public PlaqueAdapter (Context context){
        diagram = new PlaqueDiagram();
        mContext = context;
        mFragmentActivity = (FragmentActivity) context;
    }

    public void setItems(ArrayList<ThootPlaque> list){
        this.items = list;
    }

    public void setCuadrant(int cuadrantNumber){
        cuadrant = cuadrantNumber;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_plaque, parent, false);
        return new PlaqueHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        selectedThoot = items.get(position);
    }

    @Override
    public int getItemCount() {
        return 13;
    }

    public void setThootList(String[] list){
        mToothList = list;
    }


    private class PlaqueHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        private int mIndex;
        private ImageView mTop;
        private ImageView mBottom;
        private ImageView mLeft;
        private ImageView mRight;
        private ImageView mCenter;



        public PlaqueHolder(View itemView) {
            super(itemView);
            mTop = (ImageView) itemView.findViewById(R.id.plaqueTop);
            mBottom = (ImageView) itemView.findViewById(R.id.plaqueBottom);
            mLeft = (ImageView) itemView.findViewById(R.id.plaqueLeft);
            mRight = (ImageView) itemView.findViewById(R.id.plaqueRight) ;
            mCenter = (ImageView) itemView.findViewById(R.id.plaqueCenter);
             itemView.setOnClickListener(this);
        }

        public void bindView (int position){
            ThootPlaque thootPlaque = (ThootPlaque) items.get(position);
            if(thootPlaque.getOclusal()) { mCenter.setVisibility(View.VISIBLE); } else { mCenter.setVisibility(View.INVISIBLE); }
            if(thootPlaque.getBucal()) { mBottom.setVisibility(View.VISIBLE); } else { mBottom.setVisibility(View.INVISIBLE); }
            if(thootPlaque.getLingual()) { mTop.setVisibility(View.VISIBLE); } else { mTop.setVisibility(View.INVISIBLE); }
            if(thootPlaque.getMesial()) { mLeft.setVisibility(View.VISIBLE); } else { mLeft.setVisibility(View.INVISIBLE); }
            if(thootPlaque.getDistal()) { mRight.setVisibility(View.VISIBLE); } else { mRight.setVisibility(View.INVISIBLE); }
            this.mIndex = position;

            //Log.e("INDEX", String.valueOf(this.mIndex));



        }

        @Override
        public void onClick(View v) {
            thootPlaqueView = v;
            PlaqueDialog dialog = new PlaqueDialog();
            dialog.setPlagueDiagram(diagram);
            dialog.setPlaqueItemView(v);
            dialog.setPlaqueItems(items, mIndex);

            int position = getLayoutPosition(); // gets item position
            ThootPlaque user = items.get(position);
            // We can access the data within the views


            Log.e("INDEX->", String.valueOf(this.mIndex));
            Log.e("INDEX POSITION->", String.valueOf(position));
            dialog.show(mFragmentActivity.getSupportFragmentManager(),"PLAQUE_DIALOG_DIAGRAM");

        }
    }


}

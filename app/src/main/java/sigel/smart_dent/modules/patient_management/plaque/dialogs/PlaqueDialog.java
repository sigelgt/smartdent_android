package sigel.smart_dent.modules.patient_management.plaque.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import java.util.ArrayList;

import sigel.smart_dent.R;
import sigel.smart_dent.modules.patient_management.plaque.models.PlaqueDiagram;
import sigel.smart_dent.modules.patient_management.plaque.models.ThootPlaque;

/**
 * Created by Juan on 1/07/2016.
 */
public class PlaqueDialog   extends DialogFragment {
    ArrayList mSelectedItems;
    AlertDialog.Builder mDialogBuilder;
    PlaqueDiagram mDiagram;
    View passedView;
    static ArrayList<ThootPlaque> mItems;
    int mPosition;
    int cuadrantNumber;

    public interface SelectToothFragmentListener {
        public void onToothConditionListCloseButton();
        public void onCariesSelect();
    }
    SelectToothFragmentListener mListener;


    public void setCuadrantNumber(int number){
        cuadrantNumber = number;
    }

    public void setPlagueDiagram(PlaqueDiagram diagram){
        mDiagram = diagram;
    }

    public void setPlaqueItemView(View v){
        passedView = v;
    }

    public void setPlaqueItems(ArrayList<ThootPlaque> items, int position){
        mItems = items;
        mPosition = position;
    }


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        mSelectedItems = new ArrayList();
        final ThootPlaque thoot = mItems.get(mPosition);

        Log.e("INDEX POS->", String.valueOf(this.mPosition));
        //Log.e("LISTO->",);
        // Use the Builder class for convenient dialog construction
        mDialogBuilder = new AlertDialog.Builder(getActivity());
        mDialogBuilder.setTitle(R.string.select_condition_title_text)
                .setMultiChoiceItems(R.array.array_plaque_sides, null,
                        new DialogInterface.OnMultiChoiceClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which, boolean isChecked) {
                                if (isChecked) {
                                    // If the user checked the item, add it to the selected items
                                    mSelectedItems.add(which);
                                    switch (which){
                                        case 0:
                                            ((ImageView) passedView.findViewById(R.id.plaqueTop)).setVisibility(View.VISIBLE);
                                            thoot.setLingual(true);
                                            break;
                                        case 1:
                                            ((ImageView) passedView.findViewById(R.id.plaqueBottom)).setVisibility(View.VISIBLE);
                                            thoot.setBucal(true);
                                            break;
                                        case 2:
                                            ((ImageView) passedView.findViewById(R.id.plaqueLeft)).setVisibility(View.VISIBLE);
                                            thoot.setDistal(true);
                                            break;
                                        case 3:
                                            ((ImageView) passedView.findViewById(R.id.plaqueRight)).setVisibility(View.VISIBLE);
                                            thoot.setMesial(true);
                                            break;
                                        case 4:
                                            ((ImageView) passedView.findViewById(R.id.plaqueCenter)).setVisibility(View.VISIBLE);
                                            thoot.setOclusal(true);
                                            break;
                                    }
                                    //mItems.set(mPosition,thoot);

                                } else if (mSelectedItems.contains(which)) {
                                    // Else, if the item is already in the array, remove it
                                    mSelectedItems.remove(Integer.valueOf(which));
                                    switch (which){
                                        case 0:
                                            ((ImageView) passedView.findViewById(R.id.plaqueTop)).setVisibility(View.INVISIBLE);
                                            thoot.setBucal(false);
                                            break;
                                        case 1:
                                            ((ImageView) passedView.findViewById(R.id.plaqueBottom)).setVisibility(View.INVISIBLE);
                                            thoot.setBucal(false);
                                            break;
                                        case 2:
                                            ((ImageView) passedView.findViewById(R.id.plaqueLeft)).setVisibility(View.INVISIBLE);
                                            thoot.setDistal(false);
                                            break;
                                        case 3:
                                            ((ImageView) passedView.findViewById(R.id.plaqueRight)).setVisibility(View.INVISIBLE);
                                            thoot.setMesial(false);
                                            break;
                                        case 4:
                                            ((ImageView) passedView.findViewById(R.id.plaqueCenter)).setVisibility(View.INVISIBLE);
                                            thoot.setOclusal(false);
                                            break;
                                    }
                                    mItems.set(mPosition,thoot);
                                }
                            }
                        })
                .setPositiveButton(R.string.done_dialog_button_label, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        //mListener.onToothConditionListCloseButton();
                        Log.e("LISTO->", "guardar objeto");
                    }
                });
                    /*.setNegativeButton(R.string.cancel_dialog_button_label, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            mListener.onDialogNegativeClick(SelectConditionDialogFragment.this);
                        }
                    })*/
        // Create the AlertDialog object and return it

        return mDialogBuilder.create();
    }
}

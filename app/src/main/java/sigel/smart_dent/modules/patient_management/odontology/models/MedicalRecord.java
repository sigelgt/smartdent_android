package sigel.smart_dent.modules.patient_management.odontology.models;

/**
 * Created by eduardogomez on 13/07/16.
 */

public class MedicalRecord {

    public class sensibilidad {
        public boolean sensibility_hot_beverages;
        public boolean sensibility_chewing;
        public boolean sensibility_sweet_food;
        public boolean sensibility_cold_beverages;
        public boolean sensibility_air;
    }
}

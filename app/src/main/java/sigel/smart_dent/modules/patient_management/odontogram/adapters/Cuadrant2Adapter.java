package sigel.smart_dent.modules.patient_management.odontogram.adapters;

import sigel.smart_dent.R;
import sigel.smart_dent.modules.patient_management.odontogram.fragments.Cuadrante2Fragment;
import sigel.smart_dent.modules.patient_management.odontogram.models.Theeth;

/**
 * Created by Juan on 16/06/2016.
 */
public class Cuadrant2Adapter extends OdontogramCuadrantsAdapter {

    private final Cuadrante2Fragment.OnOdontogramItemSelectedInterface mListener;

    public Cuadrant2Adapter(Cuadrante2Fragment.OnOdontogramItemSelectedInterface listener) {
        mListener = listener;
    }

    @Override
    public void onItemSelected(int index) {
        mListener.onCudrant2GridItemSelected(index);
    }

    @Override
    public int getBucalResourceId(int position) {
        return Theeth.resourcesId2b[position];
    }

    @Override
    public int getOclusalResourceId(int position) {
        return Theeth.resourcesId2o[position];
    }

    @Override
    public String getIndexName(int position) {
        return Theeth.cudarante2b[position];
    }

    @Override
    public int getLayoutId() {
        return R.layout.item_cuadrant_odontogram;
    }
}

package sigel.smart_dent.modules.patient_management.endodontics;

import android.os.Bundle;
import android.util.Log;
import android.view.View;

import java.util.ArrayList;

import it.moondroid.coverflow.components.ui.containers.FeatureCoverFlow;
import sigel.smart_dent.CoverFlowAdapter;
import sigel.smart_dent.Diente;
import sigel.smart_dent.R;
import sigel.smart_dent.modules.PatientsManagementFrameActivity;
import sigel.smart_dent.modules.patient_management.odontology.PacienteExamenOrtodoncia;

/**
 * Created by pablin on 25/05/2016.
 */
public class PacienteExamenEndodoncia extends PatientsManagementFrameActivity implements View.OnClickListener{

    private FeatureCoverFlow coverFlow;
    private CoverFlowAdapter adapter;
    private ArrayList<Diente> dientes;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        coverFlow = (FeatureCoverFlow) findViewById(R.id.coverflow);



        adapter = new CoverFlowAdapter(this, dientes);
        coverFlow.setAdapter(adapter);
        coverFlow.setOnScrollPositionListener(onScrollListener());

    }

    private FeatureCoverFlow.OnScrollPositionListener onScrollListener(){
        return new FeatureCoverFlow.OnScrollPositionListener(){

            @Override
            public void onScrolledToPosition(int position) {
                Log.d("ExamenEndodoncia", "posicion: "+position);
            }

            @Override
            public void onScrolling() {
                Log.d("ExamenEndodoncia", "scrolling");
            }
        };
    }



    @Override
    public String setViewTitle() {
        return "Examen Endodoncia";
    }

    @Override
    protected int getActivityLayout() {
        return R.layout.activity_paciente_examen_endodoncia;
    }

    @Override
    public void onClick(View v) {
        new PacienteExamenOrtodoncia();
    }

}

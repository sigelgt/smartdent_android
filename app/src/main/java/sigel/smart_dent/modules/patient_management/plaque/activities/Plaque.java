package sigel.smart_dent.modules.patient_management.plaque.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import sigel.smart_dent.R;
import sigel.smart_dent.modules.patient_management.plaque.adapters.PlaqueFragmentPageAdapter;
import sigel.smart_dent.modules.patient_management.plaque.fragments.PlaqueCuadrantFragment;

/**
 * Created by pablin on 5/10/2016.
 */
public class Plaque extends AppCompatActivity {

    @Override
    public void onCreate(@Nullable  Bundle savedInstanceState){
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_paciente_plaque);

        //Obtener
        ViewPager viewPagerPlaque = (ViewPager) findViewById(R.id.viewpager);
        viewPagerPlaque.setAdapter(new PlaqueFragmentPageAdapter(getSupportFragmentManager(), Plaque.this));

        TabLayout tabLayout = (TabLayout) findViewById(R.id.sliding_tabs);
        tabLayout.setupWithViewPager(viewPagerPlaque);
    }



}

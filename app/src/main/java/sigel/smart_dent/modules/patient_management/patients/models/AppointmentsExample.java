package sigel.smart_dent.modules.patient_management.patients.models;

/**
 * Created by Juan on 19/06/2016.
 */
public class AppointmentsExample {
    public static int[] appointmentIds = new int[]{
            10,
            11,
            14,
            20,
            18,
            23,
            24
    };

    public static String[] names = new String[]{
            "Juan Perez",
            "Luis Morales",
            "Maria Lopez",
            "Pedro Garcia",
            "Lourdes Barrera",
            "Hector Hernandez",
            "Karina Ruano"
    };

    public static String[] procedures = new String[]{
            "Limpieza Dental",
            "Extracción",
            "Ortodoncia",
            "Tratamiento de canales",
            "Extraxión",
            "Revisión / Primera cita",
            "Revisión / Seguimiento"
    };

    public static String[] places = new String[]{
            "Clinica Zona 9",
            "Clinica Zona 9",
            "Clinica Zona 9",
            "Clinica Zona 14",
            "Clinica Zona 14",
            "Clinica Zona 10",
            "Clinica Zona 10"
    };

    public static String[] time = new String[]{
            "09:00 AM",
            "11:00 AM",
            "11:30 AM",
            "02:00 PM",
            "04:00 PM",
            "04:30 PM",
            "05:30 PM"
    };
}

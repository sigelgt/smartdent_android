package sigel.smart_dent.modules.patient_management.odontogram.models;

import sigel.smart_dent.R;

/**
 * Created by Juan on 16/06/2016.
 */
public class Theeth {

    public static String[] cuadrantes = new String[]{"1","2","3","4"};

    public static String[] cudarante1b = new String[]{"1_8","1_7","1_6","1_5","1_4","1_3","1_2","1_1"};
    public static String[] cudarante2b = new String[]{"2_1","2_2","2_3","2_4","2_5","2_6","2_7","2_8"};
    public static String[] cudarante3b = new String[]{"3_8","3_7","3_6","3_5","3_4","3_3","3_2","3_1"};
    public static String[] cudarante4b = new String[]{"4_1","4_2","4_3","4_4","4_5","4_6","4_7","4_8"};

    public static String[] cudarante5b = new String[]{"5_5","5_4","5_3","5_2","5_1"};
    public static String[] cudarante6b = new String[]{"6_1","6_2","6_3","6_4","6_5"};
    public static String[] cudarante7b = new String[]{"7_5","7_4","7_3","7_2","7_1"};
    public static String[] cudarante8b = new String[]{"8_1","8_2","8_3","8_4","8_5"};


    public static String[] cudarante1o = new String[]{"1_8","1_7","1_6","1_5","1_4","1_3","1_2","1_1"};
    public static String[] cudarante2o = new String[]{"2_1","2_2","2_3","2_4","2_5","2_6","2_7","2_8"};
    public static String[] cudarante3o = new String[]{"3_8","3_7","3_6","3_5","3_4","3_3","3_2","3_1"};
    public static String[] cudarante4o = new String[]{"4_1","4_2","4_3","4_4","4_5","4_6","4_7","4_8"};

/*
    public static int[] resourcesId1b = new int[]{R.drawable.b-01-08-01,R.drawable.b-01-07-01,R.drawable.b-01-06-01,R.drawable.b-01-05-01,R.drawable.b-01-04-01,R.drawable.b-01-03-01,R.drawable.b-01-02-01,R.drawable.b-01-01-01};
    public static int[] resourcesId2b = new int[]{R.drawable.b-02-01-01,R.drawable.b-02-02-01,R.drawable.b-02-03-01,R.drawable.b-02-04-01,R.drawable.b-02-05-01,R.drawable.b-02-06-01,R.drawable.b-02-07-01,R.drawable.b-02-08-01};
    public static int[] resourcesId3b = new int[]{R.drawable.b-03-08-01,R.drawable.b-03-07-01,R.drawable.b-03-06-01,R.drawable.b-03-05-01,R.drawable.b-03-04-01,R.drawable.b-03-03-01,R.drawable.b-03-02-01,R.drawable.b-03-01-01};
    public static int[] resourcesId4b = new int[]{R.drawable.b-04-01-01,R.drawable.b-04-02-01,R.drawable.b-04-03-01,R.drawable.b-04-04-01,R.drawable.b-04-05-01,R.drawable.b-04-06-01,R.drawable.b-04-07-01,R.drawable.b-04-08-01};

    public static int[] resourcesId1o = new int[]{R.drawable.o-01-08-02,R.drawable.o-01-07-02,R.drawable.o-01-06-02,R.drawable.o-01-05-02,R.drawable.o-01-04-02,R.drawable.o-01-03-02,R.drawable.o-01-02-02,R.drawable.o-01-01-02};
    public static int[] resourcesId2o = new int[]{R.drawable.o-02-01-02,R.drawable.o-02-02-02,R.drawable.o-02-03-02,R.drawable.o-02-04-02,R.drawable.o-02-05-02,R.drawable.o-02-06-02,R.drawable.o-02-07-02,R.drawable.o-02-08-02};
    public static int[] resourcesId3o = new int[]{R.drawable.o-03-08-02,R.drawable.o-03-07-02,R.drawable.o-03-06-02,R.drawable.o-03-05-02,R.drawable.o-03-04-02,R.drawable.o-03-03-02,R.drawable.o-03-02-02,R.drawable.o-03-01-02};
    public static int[] resourcesId4o = new int[]{R.drawable.o-04-01-02,R.drawable.o-04-02-02,R.drawable.o-04-03-02,R.drawable.o-04-04-02,R.drawable.o-04-05-02,R.drawable.o-04-06-02,R.drawable.o-04-07-02,R.drawable.o-04-08-02};
*/

    /*
    public static int[] resourcesId1b = new int[]{R.drawable.bucal_1_8,R.drawable.bucal_1_7,R.drawable.bucal_1_6,R.drawable.bucal_1_5,R.drawable.bucal_1_4,R.drawable.bucal_1_3,R.drawable.bucal_1_2,R.drawable.bucal_1_1};
    public static int[] resourcesId2b = new int[]{R.drawable.bucal_2_1,R.drawable.bucal_2_2,R.drawable.bucal_2_3,R.drawable.bucal_2_4,R.drawable.bucal_2_5,R.drawable.bucal_2_6,R.drawable.bucal_2_7,R.drawable.bucal_2_8};
    public static int[] resourcesId3b = new int[]{R.drawable.bucal_3_8,R.drawable.bucal_3_7,R.drawable.bucal_3_6,R.drawable.bucal_3_5,R.drawable.bucal_3_4,R.drawable.bucal_3_3,R.drawable.bucal_3_2,R.drawable.bucal_3_1};
    public static int[] resourcesId4b = new int[]{R.drawable.bucal_4_1,R.drawable.bucal_4_2,R.drawable.bucal_4_3,R.drawable.bucal_4_4,R.drawable.bucal_4_5,R.drawable.bucal_4_6,R.drawable.bucal_4_7,R.drawable.bucal_4_8};

    public static int[] resourcesId1o = new int[]{R.drawable.oclusal_1_8,R.drawable.oclusal_1_7,R.drawable.oclusal_1_6,R.drawable.oclusal_1_5,R.drawable.oclusal_1_4,R.drawable.oclusal_1_3,R.drawable.oclusal_1_2,R.drawable.oclusal_1_1};
    public static int[] resourcesId2o = new int[]{R.drawable.oclusal_2_1,R.drawable.oclusal_2_2,R.drawable.oclusal_2_3,R.drawable.oclusal_2_4,R.drawable.oclusal_2_5,R.drawable.oclusal_2_6,R.drawable.oclusal_2_7,R.drawable.oclusal_2_8};
    public static int[] resourcesId3o = new int[]{R.drawable.oclusal_3_8,R.drawable.oclusal_3_7,R.drawable.oclusal_3_6,R.drawable.oclusal_3_5,R.drawable.oclusal_3_4,R.drawable.oclusal_3_3,R.drawable.oclusal_3_2,R.drawable.oclusal_3_1};
    public static int[] resourcesId4o = new int[]{R.drawable.oclusal_4_1,R.drawable.oclusal_4_2,R.drawable.oclusal_4_3,R.drawable.oclusal_4_4,R.drawable.oclusal_4_5,R.drawable.oclusal_4_6,R.drawable.oclusal_4_7,R.drawable.oclusal_4_8};
    */

    public static int[] resourcesId1b = new int[]{R.drawable.b_01_08_01,R.drawable.b_01_07_01,R.drawable.b_01_06_01,R.drawable.b_01_05_01,R.drawable.b_01_04_01,R.drawable.b_01_03_01,R.drawable.b_01_02_01,R.drawable.b_01_01_01};
    public static int[] resourcesId2b = new int[]{R.drawable.b_02_08_01,R.drawable.b_02_07_01,R.drawable.b_02_06_01,R.drawable.b_02_05_01,R.drawable.b_02_04_01,R.drawable.b_02_03_01,R.drawable.b_02_02_01,R.drawable.b_02_01_01};
    public static int[] resourcesId3b = new int[]{R.drawable.b_03_08_01,R.drawable.b_03_07_01,R.drawable.b_03_06_01,R.drawable.b_03_05_01,R.drawable.b_03_04_01,R.drawable.b_03_03_01,R.drawable.b_03_02_01,R.drawable.b_03_01_01};
    public static int[] resourcesId4b = new int[]{R.drawable.b_04_08_01,R.drawable.b_04_07_01,R.drawable.b_04_06_01,R.drawable.b_04_05_01,R.drawable.b_04_04_01,R.drawable.b_04_03_01,R.drawable.b_04_02_01,R.drawable.b_04_01_01};

    public static int[] resourcesId1o = new int[]{R.drawable.o_01_08_02,R.drawable.o_01_07_02,R.drawable.o_01_06_02,R.drawable.o_01_05_02,R.drawable.o_01_04_02,R.drawable.o_01_03_02,R.drawable.o_01_02_02,R.drawable.o_01_01_02};
    public static int[] resourcesId2o = new int[]{R.drawable.o_02_08_02,R.drawable.o_02_07_02,R.drawable.o_02_06_02,R.drawable.o_02_05_02,R.drawable.o_02_04_02,R.drawable.o_02_03_02,R.drawable.o_02_02_02,R.drawable.o_02_01_02};
    public static int[] resourcesId3o = new int[]{R.drawable.o_03_08_02,R.drawable.o_03_07_02,R.drawable.o_03_06_02,R.drawable.o_03_05_02,R.drawable.o_03_04_02,R.drawable.o_03_03_02,R.drawable.o_03_02_02,R.drawable.o_03_01_02};
    public static int[] resourcesId4o = new int[]{R.drawable.o_04_08_02,R.drawable.o_04_07_02,R.drawable.o_04_06_02,R.drawable.o_04_05_02,R.drawable.o_04_04_02,R.drawable.o_04_03_02,R.drawable.o_04_02_02,R.drawable.o_04_01_02};


    public static String[] getCuadranteNames(int index, String cara){
        if(cara == "oclusal"){
            switch (index){
                case 1: return cudarante1o;
                case 2: return cudarante2o;
                case 3: return cudarante3o;
                case 4: return cudarante4o;
                default: return cudarante1o;
            }
        }else{
            switch (index){
                case 1: return cudarante1b;
                case 2: return cudarante2b;
                case 3: return cudarante3b;
                case 4: return cudarante4b;
                default: return cudarante1b;
            }
        }
    }

    public static int[] getCuadranteResourceIds(int index, String cara){
        if(cara == "oclusal"){
            switch (index){
                case 1:     return resourcesId1o;
                case 2:     return resourcesId2o;
                case 3:     return resourcesId3o;
                case 4:     return resourcesId4o;
                default:    return resourcesId1o;
            }
        }else{
            switch (index){
                case 1:     return resourcesId1b;
                case 2:     return resourcesId2b;
                case 3:     return resourcesId3b;
                case 4:     return resourcesId4b;
                default:    return resourcesId1b;
            }
        }
    }
}

package sigel.smart_dent.modules.patient_management.odontology;

import android.app.Dialog;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.NumberPicker;

import sigel.smart_dent.R;
import sigel.smart_dent.modules.PatientsManagementFrameActivity;

/**
 * Created by pablin on 27/05/2016.
 */
public class PacienteExamenOdontologico extends PatientsManagementFrameActivity implements NumberPicker.OnValueChangeListener, View.OnClickListener{


    static Dialog d;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);



        /*NumberPicker np_horizontal = (NumberPicker) findViewById(R.id.number_picker_sm_horizontal);
        np_horizontal.setMinValue(0);
        np_horizontal.setMaxValue(20);
        //np.setValue(myCurrentValue - minValue);
        np_horizontal.setWrapSelectorWheel(false);
        np_horizontal.setOnValueChangedListener(this);


        NumberPicker np_vertical = (NumberPicker) findViewById(R.id.number_picker_sm_vertical);
        np_vertical.setMinValue(0);
        np_vertical.setMaxValue(20);
        //np.setValue(myCurrentValue - minValue);
        np_vertical.setWrapSelectorWheel(false);
        np_vertical.setOnValueChangedListener(this);*/

        /*
        Button btn_sobre_mordida_horizontal = (Button) findViewById(R.id.sobre_mordida_horizontal);
        Button btn_sobre_mordida_vertical = (Button) findViewById(R.id.sobre_mordida_vertical);

        btn_sobre_mordida_horizontal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mostrarSeleccionador();
            }
        });

        btn_sobre_mordida_vertical.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mostrarSeleccionador();
            }
        });
        */


    }

    @Override
    public String setViewTitle() {
        return "Examen de Ortodoncia";
    }

    @Override
    protected int getActivityLayout() {
        return R.layout.activity_examen_ortodoncia;
    }


    @Override
    public void onClick(View v) {
        new PacienteExamenOdontologico();
    }


    @Override
    public void onValueChange(NumberPicker picker, int oldVal, int newVal) {

    }

    private void mostrarSeleccionador() {

        final Dialog d = new Dialog(PacienteExamenOdontologico.this);
        d.setTitle("Milimetros");;
        d.setContentView(R.layout.dialog_number_picker);

        Button establecer = (Button) d.findViewById(R.id.number_picker_est);
        Button cancelar = (Button) d.findViewById(R.id.number_picker_can);

        //final int minValue = -20;
        final int maxValue = 20;

        NumberPicker np = (NumberPicker) d.findViewById(R.id.number_picker_sm);
        np.setMinValue(0);
        np.setMaxValue(maxValue);
        //np.setValue(myCurrentValue - minValue);
        np.setWrapSelectorWheel(false);
        np.setOnValueChangedListener(this);

        /*
        np.setFormatter(new NumberPicker.Formatter() {
            @Override
            public String format(int index) {
                return Integer.toString(index - minValue);
            }
        });
*/
        establecer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }


/*
            @Override
            public void onClick(View v) {
                //tv.setText(String.valueOf(np.getValue())); //set the value to textview
                d.dismiss();
            }*/
        });
        establecer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                d.dismiss(); // dismiss the dialog
            }
        });


        d.show();


    }


}




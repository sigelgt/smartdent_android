package sigel.smart_dent.modules.patient_management.patients;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import sigel.smart_dent.R;

public class paciente extends AppCompatActivity {

    Button crear_nuevo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_paciente);

        crear_nuevo = (Button) findViewById(R.id.btn_nuevo);

        crear_nuevo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Creamos el intent
                Intent intent = new Intent(paciente.this, pacientes.class);

                //Iniciamos la nueva actividad
                startActivity(intent);

            }
        });
    }
}

package sigel.smart_dent.modules.patient_management.medical_record.models;

/**
 * Created by Juan on 22/06/2016.
 */

public class MedicalRecord {


    public class alergias {
        public boolean alergia;
        public boolean alergiaAnestesia;
        public boolean antibioticos;
        public boolean alergiaOtros;
        public boolean alergiaAntibioticos;
        public String otrosComentario;
    }

    public alergias alergia = new alergias();
    public boolean anemia;
    public boolean artritis;
    public boolean asma;
    public boolean cancer;
    public boolean cefalea;
    public boolean diabetes;
    public boolean embarazo;
    public boolean encefalitis;
    public boolean enfermedad_renal;
    public boolean epilepsia;
    public boolean fiebre_reumatica;
    public boolean hepatitis;
    public boolean problema_endocrinos;


}

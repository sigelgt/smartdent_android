package sigel.smart_dent.modules.patient_management.plaque.old_files;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

/**
 * Created by pablin on 1/06/2016.
 */
public class PacientePlacaBacterianaPagerAdapter extends FragmentPagerAdapter {

    public PacientePlacaBacterianaPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int index) {

        switch (index) {
            case 0:
                // Top Rated fragment activity
                //return new PacientePlacaBacterianaMaxilarDerecha();
            case 1:
                // Games fragment activity
                //return new PacientePlacaBacterianaMaxilarIzquierda();
            case 2:
                // Movies fragment activity
                //return new PacientePlacaBacterianaMandibularDerecha();
            case 3:
                // Movies fragment activity
                //return new PacientePlacaBacterianaMandibularIzquierda();
        }

        return null;
    }

    @Override
    public int getCount() {
        // get item count - equal to number of tabs
        return 4;
    }

}
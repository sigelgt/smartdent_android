package sigel.smart_dent.modules.patient_management.plaque.old_files;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import sigel.smart_dent.R;

/**
 * Created by pablin on 1/06/2016.
 */
public class PacientePlacaBacterianaMaxilarIzquierda extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {List<CharSequence> list = new ArrayList<>();

    @Override
    protected  void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_paciente_placa_bacteriana_maxilar_izquierda);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);


        list.add("Oclusal");
        list.add("Distal");
        list.add("Mesial");
        list.add("Lingual");
        list.add("Bucal");

        onClickButton();
    }

    public void onClickButton(){
        findViewById(R.id.button1_1).setOnClickListener(mButton1_OnClickListener);
        findViewById(R.id.button1_2).setOnClickListener(mButton1_OnClickListener);
        findViewById(R.id.button1_3).setOnClickListener(mButton1_OnClickListener);
        findViewById(R.id.button1_4).setOnClickListener(mButton1_OnClickListener);
        findViewById(R.id.button1_5).setOnClickListener(mButton1_OnClickListener);
        findViewById(R.id.button1_6).setOnClickListener(mButton1_OnClickListener);
        findViewById(R.id.button1_7).setOnClickListener(mButton1_OnClickListener);
        findViewById(R.id.button1_8).setOnClickListener(mButton1_OnClickListener);
        findViewById(R.id.button1_1_d).setOnClickListener(mButton1_OnClickListener);
        findViewById(R.id.button1_2_d).setOnClickListener(mButton1_OnClickListener);
        findViewById(R.id.button1_3_d).setOnClickListener(mButton1_OnClickListener);
        findViewById(R.id.button1_4_d).setOnClickListener(mButton1_OnClickListener);
        findViewById(R.id.button1_5_d).setOnClickListener(mButton1_OnClickListener);
    }


    //Global On click listener for all views
    final View.OnClickListener mButton1_OnClickListener = new View.OnClickListener() {
        public void onClick(final View v) {
            switch (v.getId()) {
                case R.id.button1_1:
                    //Inform the user the button1 has been clicked
                    popMenu(R.id.button1_1);
                    //Toast.makeText(PacientePlacaBacterianaDerecha.this, "Button0 clicked.", Toast.LENGTH_SHORT).show();
                    break;
                case R.id.button1_2:
                    //Inform the user the button2 has been clicked
                    popMenu(R.id.button1_2);
                    //Toast.makeText(PacientePlacaBacterianaDerecha.this, "Button0 clicked.", Toast.LENGTH_SHORT).show();
                    break;
                case R.id.button1_3:
                    //Inform the user the button3 has been clicked
                    popMenu(R.id.button1_3);
                    //Toast.makeText(PacientePlacaBacterianaDerecha.this, "Button0 clicked.", Toast.LENGTH_SHORT).show();
                    break;
                case R.id.button1_4:
                    //Inform the user the button4 has been clicked
                    popMenu(R.id.button1_4);
                    //Toast.makeText(PacientePlacaBacterianaDerecha.this, "Button0 clicked.", Toast.LENGTH_SHORT).show();
                    break;
                case R.id.button1_5:
                    //Inform the user the button5 has been clicked
                    popMenu(R.id.button1_5);
                    //Toast.makeText(PacientePlacaBacterianaDerecha.this, "Button0 clicked.", Toast.LENGTH_SHORT).show();
                    break;
                case R.id.button1_6:
                    //Inform the user the button5 has been clicked
                    popMenu(R.id.button1_6);
                    //Toast.makeText(PacientePlacaBacterianaDerecha.this, "Button0 clicked.", Toast.LENGTH_SHORT).show();
                    break;
                case R.id.button1_7:
                    //Inform the user the button5 has been clicked
                    popMenu(R.id.button1_7);
                    //Toast.makeText(PacientePlacaBacterianaDerecha.this, "Button0 clicked.", Toast.LENGTH_SHORT).show();
                    break;
                case R.id.button1_8:
                    //Inform the user the button5 has been clicked
                    popMenu(R.id.button1_8);
                    //Toast.makeText(PacientePlacaBacterianaDerecha.this, "Button0 clicked.", Toast.LENGTH_SHORT).show();
                    break;
                case R.id.button1_1_d:
                    //Inform the user the button1_d has been clicked
                    popMenu(R.id.button1_1_d);
                    //Toast.makeText(PacientePlacaBacterianaDerecha.this, "Button0 clicked.", Toast.LENGTH_SHORT).show();
                    break;
                case R.id.button1_2_d:
                    //Inform the user the button1_d has been clicked
                    popMenu(R.id.button1_2_d);
                    //Toast.makeText(PacientePlacaBacterianaDerecha.this, "Button0 clicked.", Toast.LENGTH_SHORT).show();
                    break;
                case R.id.button1_3_d:
                    //Inform the user the button1_d has been clicked
                    popMenu(R.id.button1_3_d);
                    //Toast.makeText(PacientePlacaBacterianaDerecha.this, "Button0 clicked.", Toast.LENGTH_SHORT).show();
                    break;
                case R.id.button1_4_d:
                    //Inform the user the button1_d has been clicked
                    popMenu(R.id.button1_4_d);
                    //Toast.makeText(PacientePlacaBacterianaDerecha.this, "Button0 clicked.", Toast.LENGTH_SHORT).show();
                    break;
                case R.id.button1_5_d:
                    //Inform the user the button1_d has been clicked
                    popMenu(R.id.button1_5_d);
                    //Toast.makeText(PacientePlacaBacterianaDerecha.this, "Button0 clicked.", Toast.LENGTH_SHORT).show();
                    break;
            }
        }
    };

    public void popMenu(final int idButton){
        // Intialize  readable sequence of char values
        final CharSequence[] dialogList=  list.toArray(new CharSequence[list.size()]);
        final AlertDialog.Builder builderDialog = new AlertDialog.Builder(PacientePlacaBacterianaMaxilarIzquierda.this);
        builderDialog.setTitle("Seleccione el area afeactada");
        int count = dialogList.length;
        boolean[] is_checked = new boolean[count]; // set is_checked boolean false;

        // Creating multiple selection by using setMutliChoiceItem method
        builderDialog.setMultiChoiceItems(dialogList, is_checked,
                new DialogInterface.OnMultiChoiceClickListener() {
                    public void onClick(DialogInterface dialog,
                                        int whichButton, boolean isChecked) {
                    }
                });

        builderDialog.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        ListView list = ((AlertDialog) dialog).getListView();
                        // make selected item in the comma seprated string
                        StringBuilder stringBuilder = new StringBuilder();
                        for (int i = 0; i < list.getCount(); i++) {
                            boolean checked = list.isItemChecked(i);

                            if (checked) {
                                if (stringBuilder.length() > 0) stringBuilder.append(",");
                                stringBuilder.append(list.getItemAtPosition(i));

                            }
                        }
                        Log.e("et", stringBuilder.toString().trim());
                        switch (stringBuilder.toString().trim()) {
                            case "Oclusal":
                                findViewById(idButton).setBackgroundResource(R.mipmap.ic_oclusal);
                                break;
                            case "Oclusal,Bucal":
                                findViewById(idButton).setBackgroundResource(R.mipmap.ic_oclusal_bucal);
                                break;
                            case "Oclusal,Distal":
                                findViewById(idButton).setBackgroundResource(R.mipmap.ic_oclusal_distal);
                                break;
                            case "Oclusal,Lingual":
                                findViewById(idButton).setBackgroundResource(R.mipmap.ic_oclusal_lingual);
                                break;
                            case "Oclusal,Mesial":
                                findViewById(idButton).setBackgroundResource(R.mipmap.ic_oclusal_mesial);
                                break;
                            case "Oclusal,Distal,Lingual":
                                findViewById(idButton).setBackgroundResource(R.mipmap.ic_oclusal_lingual_distal);
                                break;
                            case "Oclusal,Mesial,Lingual":
                                findViewById(idButton).setBackgroundResource(R.mipmap.ic_oclusal_lingual_mesial);
                                break;
                            case "Oclusal,Lingual,Bucal":
                                findViewById(idButton).setBackgroundResource(R.mipmap.ic_oclusal_lingual_bucal);
                                break;
                            case "Oclusal,Mesial,Lingual,Bucal":
                                findViewById(idButton).setBackgroundResource(R.mipmap.ic_oclusal_lingual_bucal_mesial);
                                break;
                            case "Oclusal,Distal,Lingual,Bucal":
                                findViewById(idButton).setBackgroundResource(R.mipmap.ic_oclusal_lingual_bucal_distal);
                                break;
                            case "Oclusal,Distal,Mesial,Lingual":
                                findViewById(idButton).setBackgroundResource(R.mipmap.ic_oclusal_lingual_mesial_distal);
                                break;
                            case "Oclusal,Distal,Mesial,Bucal":
                                findViewById(idButton).setBackgroundResource(R.mipmap.ic_oclusal_mesial_bucal_distal);
                                break;
                            case "Distal,Mesial,Bucal":
                                findViewById(idButton).setBackgroundResource(R.mipmap.ic_mesial_bucal_distal);
                                break;
                            case "Distal,Lingual,Bucal,":
                                findViewById(idButton).setBackgroundResource(R.mipmap.ic_bucal_distal_lingual);
                                break;
                            case "Distal,Mesial,Lingual":
                                findViewById(idButton).setBackgroundResource(R.mipmap.ic_mesial_bucal_distal);
                                break;
                            case "Mesial,Lingual,Bucal":
                                findViewById(idButton).setBackgroundResource(R.mipmap.ic_lingual_mesial_bucal);
                                break;
                            case "Oclusal,Distal,Bucal":
                                findViewById(idButton).setBackgroundResource(R.mipmap.ic_oclusal_bucal_distal);
                                break;
                            case "Distal":
                                findViewById(idButton).setBackgroundResource(R.mipmap.ic_mesial);
                                break;
                            case "Mesial":
                                findViewById(idButton).setBackgroundResource(R.mipmap.ic_distal);
                                break;
                            case "Lingual":
                                findViewById(idButton).setBackgroundResource(R.mipmap.ic_lingual);
                                break;
                            case "Bucal":
                                findViewById(idButton).setBackgroundResource(R.mipmap.ic_bucal);
                                break;
                            case "Oclusal,Distal,Mesial,Lingual,Bucal":
                                findViewById(idButton).setBackgroundResource(R.mipmap.ic_todo);
                                break;
                            default:
                                findViewById(idButton).setBackgroundResource(R.mipmap.ic_odonto);
                                break;
                        }
                    }
                });

        builderDialog.setNegativeButton("Cancel",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        ((TextView) findViewById(R.id.text)).setText("Click here to open Dialog");
                    }
                });
        AlertDialog alert = builderDialog.create();
        alert.show();
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        return false;
    }
}

package sigel.smart_dent.modules.patient_management.plaque.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import sigel.smart_dent.R;
import sigel.smart_dent.modules.patient_management.odontogram.models.Theeth;

/**
 * Created by Juan on 1/07/2016.
 */
public class PlaqueViewPagerFragment extends Fragment {
    public Theeth mTheeth;
    private ArrayList<Fragment> cuadrantes =  new ArrayList<Fragment>();
    private ArrayList<String> cuadranteTitles;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.generic_viewpager_fragment,container,false);

        //FragmentManager fragmentManager = getFragmentManager();

        for(int i = 1; i < 5; i++){
            PlaqueCuadrantFragment cuadrante = new PlaqueCuadrantFragment();

            Bundle args = new Bundle();
            args.putInt("id", i);
            //FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            cuadrante.setCuadrantTitle("Cuadrante " + (i));
            cuadrante.setCuadrantNumber(i);
            cuadrante.setArguments(args);
            //fragmentTransaction.add(cuadrante, String.valueOf(i));
            //fragmentTransaction.commit();
            cuadrantes.add(cuadrante);
        }

        ViewPager viewPager = (ViewPager) view.findViewById(R.id.genericViewPager);
        FragmentPagerAdapter adapter = getViewPagerFragmentAdapter();
        viewPager.setAdapter(adapter);
        TabLayout tabLayout = (TabLayout) view.findViewById(R.id.genericTabLayout);
        tabLayout.setupWithViewPager(viewPager);
        return view;
    }

    private FragmentPagerAdapter getViewPagerFragmentAdapter (){
        return new FragmentPagerAdapter(getChildFragmentManager()) {

            @Override
            public int getCount() {
                return cuadrantes.size();
            }

            @Override
            public Fragment getItem(int position) {
                //Log.e("FRAG POS->", String.valueOf(cuadrantes.get(position)));
                int posnumber = position + 1;
                Log.e("FRAG POS->", String.valueOf(posnumber));
                return (PlaqueCuadrantFragment) cuadrantes.get(position);
            }

            @Override
            public CharSequence getPageTitle(int position) {
                return ((PlaqueCuadrantFragment) cuadrantes.get(position) ).getCuadrantTitle();
            }
        };
    }
}

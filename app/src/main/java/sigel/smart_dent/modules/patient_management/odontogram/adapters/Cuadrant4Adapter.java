package sigel.smart_dent.modules.patient_management.odontogram.adapters;

import sigel.smart_dent.R;
import sigel.smart_dent.modules.patient_management.odontogram.fragments.Cuadrante4Fragment;
import sigel.smart_dent.modules.patient_management.odontogram.models.Theeth;

/**
 * Created by Juan on 16/06/2016.
 */
public class Cuadrant4Adapter extends OdontogramCuadrantsAdapter {

    private final Cuadrante4Fragment.OnOdontogramItemSelectedInterface mListener;

    public Cuadrant4Adapter(Cuadrante4Fragment.OnOdontogramItemSelectedInterface listener) {
        mListener = listener;
    }

    @Override
    public void onItemSelected(int index) {
        mListener.onCudrant4GridItemSelected(index);
    }

    @Override
    public int getBucalResourceId(int position) {
        return Theeth.resourcesId4b[position];
    }

    @Override
    public int getOclusalResourceId(int position) {
        return Theeth.resourcesId4o[position];
    }

    @Override
    public String getIndexName(int position) {
        return Theeth.cudarante4b[position];
    }

    @Override
    public int getLayoutId() {
        return R.layout.item_cuadrant_odontogram;
    }
}

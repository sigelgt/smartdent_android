package sigel.smart_dent.modules.patient_management.odontogram.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import sigel.smart_dent.modules.user_management.activities.Dashboard;
import sigel.smart_dent.modules.user_management.activities.Login;
import sigel.smart_dent.modules.patient_management.odontology.PacienteExamenOrtodoncia;
import sigel.smart_dent.modules.patient_management.medical_record.PacienteHistoriaMedica;
import sigel.smart_dent.modules.patient_management.dental_record.activities.PacienteHistoriaOdontologica;
import sigel.smart_dent.modules.patient_management.patients.PacienteListar;
import sigel.smart_dent.modules.patient_management.plaque.old_files.PacientePlacaBacteriana;
import sigel.smart_dent.R;

public class ListOdontograms extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_odontograms);

        Intent odontogramIntent = new Intent(ListOdontograms.this, OdontogramCanvas.class);
        startActivity(odontogramIntent);


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent odontogramIntent = new Intent(ListOdontograms.this, OdontogramCanvas.class);
                startActivity(odontogramIntent);

                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_paciente_listar_detalle, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){

        int id = item.getItemId();

        if(id == R.id.paciente_menu_historia_medica){
            startActivity(new Intent(this, PacienteHistoriaMedica.class));
        }else if(id == R.id.paciente_menu_historia_odontologica){
            startActivity(new Intent(this, PacienteHistoriaOdontologica.class));
        }else if(id == R.id.paciente_placa_bacteriana){
            startActivity(new Intent(this, PacientePlacaBacteriana.class));
        }else if(id == R.id.paciente_menu_examen_atm){
            //startActivity(new Intent(this, PacienteATM.class));
        }else if(id == R.id.paciente_menu_examen_ortodoncia){
            startActivity(new Intent(this, PacienteExamenOrtodoncia.class));
        }else if(id == R.id.paciente_menu_examen_endodoncia){
            startActivity(new Intent(this, ListOdontograms.class));
        }else{
            //Toast.makeText(getBaseContext(), "OTRA OPCION MENU LISTAR OTRA COSA", Toast.LENGTH_SHORT).show();
        }
        return true;
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.paciente_listado) {
            startActivity(new Intent(this, PacienteListar.class));
        } else if (id == R.id.usuario_dashboard) {
            startActivity(new Intent(this, Dashboard.class));
        } else if (id == R.id.salir) {
            startActivity(new Intent(this, Login.class));
        } else {
            //Toast.makeText(getBaseContext(), "OTRA OPCION", Toast.LENGTH_SHORT).show();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}

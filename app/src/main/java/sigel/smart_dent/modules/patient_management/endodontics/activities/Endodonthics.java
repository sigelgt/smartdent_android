package sigel.smart_dent.modules.patient_management.endodontics.activities;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import sigel.smart_dent.R;
import sigel.smart_dent.modules.PatientsManagementFrameActivity;
import sigel.smart_dent.modules.patient_management.endodontics.fragments.EndodonthicsViewPager;

/**
 * Created by Juan on 24/06/2016.
 */

public class Endodonthics extends PatientsManagementFrameActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EndodonthicsViewPager savedFragment = (EndodonthicsViewPager) getSupportFragmentManager().findFragmentByTag("ENDODONTHICSPAGER");
        if(savedFragment==null) {
            EndodonthicsViewPager viewPager = new EndodonthicsViewPager();
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.add(R.id.endodonthicsPlaceHolder, viewPager, "ENDODONTHICSPAGER");
            fragmentTransaction.commit();
        }
        //ViewPagerFragment savedFragment = (ViewPagerFragment) getSupportFragmentManager().findFragmentByTag("VIEWPAGER");
    }

    @Override
    public String setViewTitle() {
        return getBaseContext().getString(R.string.page_title_examen_edodoncia);
    }

    @Override
    protected int getActivityLayout() {
        return R.layout.activity_paciente_endodonthics;
    }

    /*@Override
    public String setViewTitle() {
        return "Exámen de endodoncia";
    }

    @Override
    protected int getActivityLayout() {
        return R.layout.activity_paciente_endodonthics;
    }*/
}

package sigel.smart_dent.modules.patient_management.odontogram.adapters;

import sigel.smart_dent.R;
import sigel.smart_dent.modules.patient_management.odontogram.fragments.Cuadrante3Fragment;
import sigel.smart_dent.modules.patient_management.odontogram.models.Theeth;

/**
 * Created by Juan on 16/06/2016.
 */
public class Cuadrant3Adapter extends OdontogramCuadrantsAdapter {

    private final Cuadrante3Fragment.OnOdontogramItemSelectedInterface mListener;

    public Cuadrant3Adapter(Cuadrante3Fragment.OnOdontogramItemSelectedInterface listener) {
        mListener = listener;
    }

    @Override
    public void onItemSelected(int index) {
        mListener.onCudrant3GridItemSelected(index);
    }

    @Override
    public int getBucalResourceId(int position) {
        return Theeth.resourcesId3b[position];
    }

    @Override
    public int getOclusalResourceId(int position) {
        return Theeth.resourcesId3o[position];
    }

    @Override
    public String getIndexName(int position) {
        return Theeth.cudarante3b[position];
    }

    @Override
    public int getLayoutId() {
        return R.layout.item_cuadrant_odontogram;
    }
}

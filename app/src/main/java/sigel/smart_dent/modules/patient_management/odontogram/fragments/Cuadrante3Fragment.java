package sigel.smart_dent.modules.patient_management.odontogram.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import sigel.smart_dent.R;
import sigel.smart_dent.modules.patient_management.odontogram.adapters.Cuadrant3Adapter;

/**
 * Created by Juan on 16/06/2016.
 */
public class Cuadrante3Fragment extends Fragment {
    public interface OnOdontogramItemSelectedInterface {
        void onCudrant3GridItemSelected(int index);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        OnOdontogramItemSelectedInterface listener = (OnOdontogramItemSelectedInterface) getActivity();
        View view = inflater.inflate(R.layout.fragment_recyclerview_odontogram,container,false);
        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.recyclerViewOdontogram);
        Cuadrant3Adapter adapter = new Cuadrant3Adapter(listener);
        recyclerView.setAdapter(adapter);
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(getActivity(),8);
        recyclerView.setLayoutManager(layoutManager);
        return view;
    }
}

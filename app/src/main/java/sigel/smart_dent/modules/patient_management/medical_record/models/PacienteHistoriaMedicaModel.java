package sigel.smart_dent.modules.patient_management.medical_record.models;

import android.widget.TextView;

import static sigel.smart_dent.R.id.anemia;
import static sigel.smart_dent.R.id.artritis;
import static sigel.smart_dent.R.id.asma;
import static sigel.smart_dent.R.id.cancer;
import static sigel.smart_dent.R.id.cefalea;
import static sigel.smart_dent.R.id.diabetes;
import static sigel.smart_dent.R.id.embarazo;
import static sigel.smart_dent.R.id.encefalitis;
import static sigel.smart_dent.R.id.enfermedad_renal;
import static sigel.smart_dent.R.id.epilepsia;
import static sigel.smart_dent.R.id.fiebre_reumatica;
import static sigel.smart_dent.R.id.hepatitis;
import static sigel.smart_dent.R.id.infectocontagiosas;
import static sigel.smart_dent.R.id.presion_alta;
import static sigel.smart_dent.R.id.problemas_endocrinos;
import static sigel.smart_dent.R.id.tabaquismo;
import static sigel.smart_dent.R.id.trauma_craneo_facial;
import static sigel.smart_dent.R.id.tuberculosis;

/**
 * Created by pablin on 12/07/2016.
 */
public class PacienteHistoriaMedicaModel {

        private Boolean alergia,alergiaAnestesia,alergiaAntibioticos,alergiaOtros,anemia,artritis,asma,cancer,cefalea,diabetes,embarazo,encefalitis,enfermedad_renal,
        epilepsia,fiebre_reumatica,hepatitis,problemas_endocrinos,trauma_craneo_facial,tabaquismo,tuberculosis, infectocontagiosas,otros, ProblemasCardiacos,
                problemasCirculacion,presionalta,presionbaja,Infarto,SoploCardiaco;

       // private TextView alergiaOtrosComent;

   /* public PacienteHistoriaMedicaModel(TextView alergiaOtrosComent) {
        this.alergiaOtrosComent = alergiaOtrosComent;
    }*/

  //  public TextView getAlergiaOtrosComent(){return alergiaOtrosComent;}

    public Boolean getAlergia() {
        return alergia;
    }

    public Boolean getAlergiaAnestesia() {
        return alergiaAnestesia;
    }

    public Boolean getAlergiaAntibioticos() {
        return alergiaAntibioticos;
    }

    public Boolean getAlergiaOtros() {
        return alergiaOtros;
    }

    public Boolean getAnemia() {
        return anemia;
    }

    public Boolean getArtritis() {
        return artritis;
    }

    public Boolean getAsma() {
        return asma;
    }

    public Boolean getCancer() {
        return cancer;
    }

    public Boolean getCefalea() {
        return cefalea;
    }

    public Boolean getDiabetes() {
        return diabetes;
    }

    public Boolean getEmbarazo() {
        return embarazo;
    }

    public Boolean getEncefalitis() {
        return encefalitis;
    }

    public Boolean getEnfermedad_renal() {
        return enfermedad_renal;
    }

    public Boolean getEpilepsia() {
        return epilepsia;
    }


    public Boolean getFiebre_reumatica() {
        return fiebre_reumatica;
    }

    public Boolean getHepatitis() {
        return hepatitis;
    }

    public Boolean getProblemas_endocrinos() {
        return problemas_endocrinos;
    }

    public Boolean getTrauma_craneo_facial() {
        return trauma_craneo_facial;
    }

    public Boolean getTabaquismo() {
        return tabaquismo;
    }

    public Boolean getTuberculosis() {
        return tuberculosis;
    }

    public Boolean getInfectocontagiosas() {
        return infectocontagiosas;
    }

    public Boolean getOtros() {
        return otros;
    }

    public Boolean getProblemasCardiacos() {
        return ProblemasCardiacos;
    }

    public Boolean getProblemasCirculacion() {
        return problemasCirculacion;
    }

    public Boolean getPresionalta() {
        return presionalta;
    }

    public Boolean getPresionbaja() {
        return presionbaja;
    }

    public Boolean getInfarto() {
        return Infarto;
    }

    public Boolean getSoploCardiaco() {
        return SoploCardiaco;
    }

}

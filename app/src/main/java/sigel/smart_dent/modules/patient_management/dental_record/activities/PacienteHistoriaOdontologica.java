package sigel.smart_dent.modules.patient_management.dental_record.activities;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Switch;
import android.widget.Toast;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.GsonConverterFactory;
import retrofit2.Retrofit;
import sigel.smart_dent.R;
import sigel.smart_dent.beans.Response;
import sigel.smart_dent.modules.PatientsManagementFrameActivity;
import sigel.smart_dent.modules.patient_management.odontology.models.PacienteHistoriaOdontologicaModel;
import sigel.smart_dent.rest.APIPlug;
import sigel.smart_dent.rest.ServiceGenerator;

import static sigel.smart_dent.R.id.frenillos;
import static sigel.smart_dent.R.id.labio_inferior;
import static sigel.smart_dent.R.id.labio_superior;
import static sigel.smart_dent.R.id.mucosa;
import static sigel.smart_dent.R.id.sensibility_air;
import static sigel.smart_dent.R.id.sensibility_chewing;
import static sigel.smart_dent.R.id.sensibility_cold_beverages;
import static sigel.smart_dent.R.id.sensibility_hot_beverages;
import static sigel.smart_dent.R.id.sensibility_sweet_food;

/**
 * Created by pablin on 16/05/2016.
 */
public class PacienteHistoriaOdontologica extends PatientsManagementFrameActivity implements View.OnClickListener {

    private Switch sensibility_hot_beverages,sensibility_sweet_food,sensibility_chewing, labio_superior,sensibility_cold_beverages,sensibility_air, labio_inferior, frenillos,
            paladar, piso_de_boca, mucosa, trauma_oclusal, lengua, color_encias, edema_gingival, inflamacion_gingival, hemorragia_encias, parulis, otros;

    private Button boton_paciente_historia_odontologica_agregar;

    public static String patientId = null;
    public static final String EXTRA_PACIENTE = "PATIENT_ID";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(getIntent().hasExtra(EXTRA_PACIENTE)){
            patientId = getIntent().getExtras().getString(EXTRA_PACIENTE);
            Log.e("PATIENT_ID", patientId);

            getPatientMedicalRecord(patientId);


        }else{
            Toast.makeText(getBaseContext(), "Por favor seleccione un paciente", Toast.LENGTH_LONG).show();
            //throw new IllegalArgumentException("La actividad debe recibir un ID de paciente");
        }


        sensibility_hot_beverages = (Switch) findViewById(R.id.sensibility_hot_beverages);
        sensibility_sweet_food = (Switch) findViewById(R.id.sensibility_sweet_food);
        sensibility_chewing = (Switch) findViewById(R.id.sensibility_chewing);
        sensibility_cold_beverages = (Switch) findViewById(R.id.sensibility_cold_beverages);
        sensibility_air = (Switch) findViewById(R.id.sensibility_air);
        labio_superior = (Switch) findViewById(R.id.labio_superior);
        labio_inferior = (Switch) findViewById(R.id.labio_inferior);
        frenillos = (Switch) findViewById(R.id.frenillos);
        paladar = (Switch) findViewById(R.id.paladar);
        piso_de_boca = (Switch) findViewById(R.id.piso_de_boca);
        mucosa = (Switch) findViewById(R.id.mucosa);
        trauma_oclusal = (Switch) findViewById(R.id.trauma_oclusal);
        lengua = (Switch) findViewById(R.id.lengua);
        color_encias = (Switch) findViewById(R.id.color_encias);
        edema_gingival = (Switch) findViewById(R.id.edema_gingival);
        inflamacion_gingival = (Switch) findViewById(R.id.inflamacion_gingival);
        hemorragia_encias = (Switch) findViewById(R.id.hemorragia_encias);
        parulis = (Switch) findViewById(R.id.parulis);
        otros = (Switch) findViewById(R.id.otros);


        //attach a listener to check for changes in state
        boton_paciente_historia_odontologica_agregar = (Button) findViewById(R.id.boton_paciente_historia_odontologica_agregar);
        boton_paciente_historia_odontologica_agregar.setOnClickListener(this);
    }
    private void getPatientMedicalRecord(String patientId) {

        APIPlug cliente = ServiceGenerator.createService(APIPlug.class);

        Call<PacienteHistoriaOdontologicaModel> call = cliente.getPatientMedicalOdontologicoRecord(patientId);

        call.enqueue(new Callback<PacienteHistoriaOdontologicaModel>() {
            @Override
            public void onResponse(Call<PacienteHistoriaOdontologicaModel> call, retrofit2.Response<PacienteHistoriaOdontologicaModel> response) {
                Boolean sensibility_hot_beverages_r = response.body().getSensibility_hot_beverages();
                Boolean sensibility_sweet_food_r = response.body().getSensibility_sweet_food();
                Boolean sensibility_chewing_r = response.body().getSensibility_chewing();
                Boolean sensibility_cold_beverages_r = response.body().getSensibility_cold_beverages();
                Boolean sensibility_air_r = response.body().getSensibility_air();
                Boolean labio_superior_r = response.body().getLabio_superior();
                Boolean labio_inferior_r = response.body().getLabio_inferior();
                Boolean frenillos_r = response.body().getFrenillos();
                Boolean paladar_r = response.body().getPaladar();
                Boolean piso_de_boca_r = response.body().getPiso_de_boca();
                Boolean mucosa_r = response.body().getMucosa();
                Boolean trauma_oclusal_r = response.body().getTrauma_oclusal();
                Boolean lengua_r = response.body().getLengua();
                Boolean color_encias_r = response.body().getColor_encias();
                Boolean edema_gingival_r = response.body().getEdema_gingival();
                Boolean inflamacion_gingival_r = response.body().getInflamacion_gingival();
                Boolean hemorragia_encias_r = response.body().getHemorragia_encias();
                Boolean parulis_r = response.body().getParulis();
                Boolean otros_r = response.body().getOtros();



                if(sensibility_hot_beverages_r){sensibility_hot_beverages.setChecked(true);}
                if(sensibility_sweet_food_r){sensibility_sweet_food.setChecked(true);}
                if(sensibility_chewing_r){sensibility_chewing.setChecked(true);}
                if(sensibility_cold_beverages_r){sensibility_cold_beverages.setChecked(true);}
                if(sensibility_air_r){sensibility_air.setChecked(true);}
                if(labio_superior_r){labio_superior.setChecked(true);}
                if(labio_inferior_r){labio_inferior.setChecked(true);}
                if(frenillos_r){frenillos.setChecked(true);}
                if(paladar_r){paladar.setChecked(true);}
                if(piso_de_boca_r){piso_de_boca.setChecked(true);}
                if(mucosa_r){mucosa.setChecked(true);}
                if(trauma_oclusal_r){trauma_oclusal.setChecked(true);}
                if(lengua_r){lengua.setChecked(true);}
                if(color_encias_r){color_encias.setChecked(true);}
                if(edema_gingival_r){edema_gingival.setChecked(true);}
                if(inflamacion_gingival_r){inflamacion_gingival.setChecked(true);}
                if(hemorragia_encias_r){hemorragia_encias.setChecked(true);}
                if(parulis_r){parulis.setChecked(true);}
                if(otros_r){otros.setChecked(true);}

            }

            @Override
            public void onFailure(Call<PacienteHistoriaOdontologicaModel> call, Throwable t) {

            }
        });
    }

    @Override
    public String setViewTitle() {
        return "Historia Odontologica";
    }

    @Override
    protected int getActivityLayout() {
        return R.layout.activity_paciente_historia_odontologica;
    }


    @Override
    public void onClick(View v) {
        pacienteAgregarHistoriaOdontologica();
    }

    private void pacienteAgregarHistoriaOdontologica() {

        Boolean get_sensibility_hot_beverages = sensibility_hot_beverages.isChecked();
        Boolean get_sensibility_sweet_food = sensibility_sweet_food.isChecked();
        Boolean get_sensibility_chewing = sensibility_chewing.isChecked();
        Boolean get_sensibility_cold_beverages = sensibility_cold_beverages.isChecked();
        Boolean get_sensibility_air = sensibility_air.isChecked();
        Boolean get_labio_superior = labio_superior.isChecked();
        Boolean get_labio_inferior = labio_inferior.isChecked();
        Boolean get_frenillos = frenillos.isChecked();
        Boolean get_paladar = paladar.isChecked();
        Boolean get_piso_de_boca = piso_de_boca.isChecked();
        Boolean get_mucosa = mucosa.isChecked();
        Boolean get_trauma_oclusal = trauma_oclusal.isChecked();
        Boolean get_lengua = lengua.isChecked();
        Boolean get_color_encias = color_encias.isChecked();
        Boolean get_edema_gingival = edema_gingival.isChecked();
        Boolean get_inflamacion_gingival = inflamacion_gingival.isChecked();
        Boolean get_hemorragia_encias = hemorragia_encias.isChecked();
        Boolean get_parulis = parulis.isChecked();
        Boolean get_otros = otros.isChecked();


        agregarPacienteHistoriaOdontologica(get_sensibility_hot_beverages,get_sensibility_sweet_food, get_sensibility_chewing,  get_sensibility_cold_beverages, get_sensibility_air, get_labio_superior, get_labio_inferior, get_frenillos, get_paladar,
                get_piso_de_boca, get_mucosa, get_trauma_oclusal, get_lengua, get_color_encias, get_edema_gingival,
                get_inflamacion_gingival, get_hemorragia_encias, get_parulis, get_otros);


    }

    private void agregarPacienteHistoriaOdontologica(Boolean get_sensibility_hot_beverages, Boolean get_sensibility_sweet_food,Boolean get_sensibility_chewing,Boolean get_sensibility_cold_beverages,Boolean get_sensibility_air,
                                                     Boolean get_labio_superior, Boolean get_labio_inferior, Boolean get_frenillos, Boolean get_paladar, Boolean get_piso_de_boca, Boolean get_mucosa, Boolean get_trauma_oclusal,
                                                     Boolean get_lengua, Boolean get_color_encias, Boolean get_edema_gingival, Boolean get_inflamacion_gingival, Boolean get_hemorragia_encias, Boolean get_parulis, Boolean get_otros) {

        final ProgressDialog loading = ProgressDialog.show(this, "Grabando", "Por favor aguarde mientras finaliza el proceso", false, false);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://52.88.126.124:3000/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        APIPlug apiPlug = retrofit.create(APIPlug.class);

        Call<Response> call = apiPlug.agregarPacienteHistoriaOdontologica(patientId,get_sensibility_hot_beverages,get_sensibility_sweet_food,get_sensibility_chewing,get_sensibility_air,get_sensibility_cold_beverages
        ,get_labio_superior,get_labio_inferior,get_frenillos,get_paladar,get_piso_de_boca,get_mucosa,get_trauma_oclusal,get_lengua,get_color_encias,get_edema_gingival, get_inflamacion_gingival, get_hemorragia_encias,get_parulis,get_otros);

        call.enqueue(new Callback<Response>() {
            @Override
            public void onResponse(Call<sigel.smart_dent.beans.Response> call, retrofit2.Response<Response> response) {

                //Log.e("onResponse", response.body().getInsertedCount().toString());
                loading.dismiss();
                Toast.makeText(getBaseContext(), "Historia Médica creada correctamente!", Toast.LENGTH_SHORT).show();
                finish();
                startActivity(getIntent());
            }

            @Override
            public void onFailure(Call<sigel.smart_dent.beans.Response> call, Throwable t) {

                //Log.e("onFailure", t.toString());
                loading.dismiss();
                Toast.makeText(getBaseContext(), "Por favor intente de nuevo", Toast.LENGTH_SHORT).show();
                finish();
                startActivity(getIntent());
            }
        });

    }
}
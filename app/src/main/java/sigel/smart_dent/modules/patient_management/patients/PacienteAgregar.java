package sigel.smart_dent.modules.patient_management.patients;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.Calendar;
import java.util.GregorianCalendar;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import sigel.smart_dent.R;
import sigel.smart_dent.beans.Response;
import sigel.smart_dent.beans.ResponseFileUpload;
import sigel.smart_dent.models.Paciente;
import sigel.smart_dent.modules.PatientsManagementFrameActivity;
import sigel.smart_dent.rest.APIPlug;
import sigel.smart_dent.rest.ServiceGenerator;
import sigel.smart_dent.rest.ServiceGeneratorFileUpload;
import sigel.smart_dent.sqlite.OperationsDB;
import sigel.smart_dent.sqlite.columnsTables;
import sigel.smart_dent.syn.Syn;
import sigel.smart_dent.tools.Tools;

/**
 * Created by pablin on 17/05/2016.
 */
public class PacienteAgregar extends PatientsManagementFrameActivity implements AdapterView.OnItemSelectedListener, View.OnClickListener {

    Context mContext;

    View parentView;

    private ImageView foto;

    ImageView image_view;
    String imagePath;

    private ImageButton foto_galeria, foto_camara;

    private EditText motivo_consulta, nombres, apellidos, fecha_nacimiento, edad, numero_identificacion,direccion, email,
            numero_telefono, referido_por, contacto, contacto_numero_telefono, consideraciones_medicas, ocupacion, fName;

    private Spinner spinner_identificacion, spinner_genero, spinner_nacionalidad, spinner_estado_civil, spinner_ocupacion;
    private Button boton_paciente_agregar;
    private int anio, mes, dia;
    private Paciente detallePaciente;

    static final int DATE_PICKER_ID = 0;

    public static final String EXTRA_PACIENTE = "paciente";

    OperationsDB operationsDB;
    Tools tools;
    Syn syn;

    @Override
    protected void onCreate(Bundle savedInstanceState){

        super.onCreate(savedInstanceState);
        setContentView(getActivityLayout());

        mContext = getApplicationContext();
        parentView = findViewById(R.id.drawer_layout);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        syn = new Syn();
        tools = new Tools();

        operationsDB = OperationsDB.obtenerInstancia(getApplicationContext());

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        setTitle("Agregar paciente");

        //DATOS DE PACIENTE
        foto = (ImageView) findViewById(R.id.foto);

        foto_galeria = (ImageButton) findViewById(R.id.foto_galeria);
        foto_camara = (ImageButton) findViewById(R.id.foto_camara);

        motivo_consulta = (EditText) findViewById(R.id.motivo_consulta);
        nombres = (EditText) findViewById(R.id.nombres);
        //fName = (EditText) findViewById(R.id.fName);
        apellidos = (EditText) findViewById(R.id.apellidos);
        fecha_nacimiento = (EditText) findViewById(R.id.fecha_nacimiento);
        edad = (EditText) findViewById(R.id.edad);

        final Calendar calendar = Calendar.getInstance();
        anio = calendar.get(Calendar.YEAR);
        mes = calendar.get(Calendar.MONTH);
        dia = calendar.get(Calendar.DAY_OF_MONTH);

        //VALIDAR EL FORMATO DE LA FECHA
        fecha_nacimiento.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                showDialog(DATE_PICKER_ID);
            }
        });

        spinner_identificacion = (Spinner) findViewById(R.id.tipo_identificacion);
        numero_identificacion = (EditText) findViewById(R.id.numero_identificacion);

        spinner_genero = (Spinner) findViewById(R.id.genero);
        spinner_nacionalidad = (Spinner) findViewById(R.id.nacionalidad);
        spinner_estado_civil = (Spinner) findViewById(R.id.estado_civil);

        direccion = (EditText) findViewById(R.id.direccion);
        email = (EditText) findViewById(R.id.email);

        //spinner_ocupacion = (Spinner) findViewById(R.id.ocupacion);

        numero_telefono = (EditText) findViewById(R.id.numero_telefono);
        ocupacion = (EditText) findViewById(R.id.ocupacion);

        referido_por = (EditText) findViewById(R.id.referido_por);
        contacto = (EditText) findViewById(R.id.contacto);
        contacto_numero_telefono = (EditText) findViewById(R.id.contacto_numero_telefono);
        consideraciones_medicas = (EditText) findViewById(R.id.consideraciones_medicas);
        //DATOS DE PACIENTE

        //SPINNERS
        ArrayAdapter<CharSequence> adapter_identificacion = ArrayAdapter.createFromResource(this, R.array.tipo_identificacion_array, android.R.layout.simple_spinner_item);
        adapter_identificacion.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_identificacion.setAdapter(adapter_identificacion);

        ArrayAdapter<CharSequence> adapter_genero = ArrayAdapter.createFromResource(this, R.array.genero_array, android.R.layout.simple_spinner_item);
        adapter_genero.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_genero.setAdapter(adapter_genero);

        ArrayAdapter<CharSequence> adapter_nacionalidad = ArrayAdapter.createFromResource(this, R.array.nacionalidad_array, android.R.layout.simple_spinner_item);
        adapter_nacionalidad.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_nacionalidad.setAdapter(adapter_nacionalidad);

        ArrayAdapter<CharSequence> adapter_estado_civil = ArrayAdapter.createFromResource(this, R.array.estado_civil_array, android.R.layout.simple_spinner_item);
        adapter_estado_civil.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_estado_civil.setAdapter(adapter_estado_civil);

        spinner_identificacion.setOnItemSelectedListener(this);
        spinner_genero.setOnItemSelectedListener(this);
        spinner_nacionalidad.setOnItemSelectedListener(this);
        spinner_estado_civil.setOnItemSelectedListener(this);

        //SPINNERS

        //VERIFICAR SI VIENEN LOS DATOS
        //SI ASIGNAR A CADA ELEMENTO SU VALOR CORRESPONDIENTE
        if(getIntent().hasExtra(EXTRA_PACIENTE)){

            setTitle(R.string.patient_edit_title);

            detallePaciente = getIntent().getParcelableExtra(EXTRA_PACIENTE);
            this.setPatientId(detallePaciente.get_id());
            Picasso.with(this)
                    .load(detallePaciente.getImagen())
                    .into(foto);

            //fName.setText(detallePaciente.getNombres());
            motivo_consulta.setText(detallePaciente.getMotivo_consulta());
            nombres.setText(detallePaciente.getNombres());
            apellidos.setText(detallePaciente.getApellidos());
            edad.setText(detallePaciente.getEdad());
            fecha_nacimiento.setText(detallePaciente.getFecha_nacimiento());
            //tipo_identificacion.setText(detallePaciente.getTipo_identificacion());
            numero_identificacion.setText(detallePaciente.getNumero_identificacion());
            //genero.setText(detallePaciente.getGenero());
            //nacionalidad.setText(detallePaciente.getNacionalidad());
            //estado_civil.setText(detallePaciente.getEstado_civil());
            direccion.setText(detallePaciente.getDireccion());
            email.setText(detallePaciente.getEmail());
            ocupacion.setText(detallePaciente.getOcupacion());
            numero_telefono.setText(detallePaciente.getNumero_telefono());
            referido_por.setText(detallePaciente.getReferido_por());
            contacto.setText(detallePaciente.getContacto());
            contacto_numero_telefono.setText(detallePaciente.getContacto_numero_telefono());
            consideraciones_medicas.setText(detallePaciente.getConsideraciones_medicas());

        }else{
            //throw new IllegalArgumentException("La actividad paciente detalle debe recibir un paciente parcelable");
            //Log.d("ACCION", "NO HAGO NI MAI!!");
        }

        boton_paciente_agregar = (Button) findViewById(R.id.boton_paciente_agregar);
        boton_paciente_agregar.setOnClickListener(this);

        foto_galeria.setOnClickListener(this);
        foto_camara.setOnClickListener(this);
    }

    @Override
    public String setViewTitle() {
        return "Agregar Paciente";
    }

    @Override
    protected int getActivityLayout() {
        return R.layout.activity_paciente_agregar;
    }

    @Override
    protected Dialog onCreateDialog(int id){
        switch(id){
            case DATE_PICKER_ID:
                return new DatePickerDialog(this, pickerListener, anio, mes, dia);
        }
        return null;
    }

    private DatePickerDialog.OnDateSetListener pickerListener =  new DatePickerDialog.OnDateSetListener(){
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            updateDisplay(year, monthOfYear, dayOfMonth);
        }
    };

    private void updateDisplay(int mYear, int mMonth, int mDay) {
        try{
            int age = getAge(mYear, mMonth+1, mDay);
            this.edad.setText(String.valueOf(age));

        }catch (Exception e){
            Log.e("ERROR", e.getMessage());
        }

        this.fecha_nacimiento.setText(
                new StringBuilder()
                        // Month is 0 based so add 1
                        .append(mYear).append("-")
                        .append(String.format("%02d",mMonth + 1)).append("-")
                        .append(String.format("%02d",mDay))
        );
    }

    private int getAge(int year, int month, int day){

        Calendar birthCal = new GregorianCalendar(year, month, day);

        Calendar nowCal = new GregorianCalendar();

        int age = nowCal.get(Calendar.YEAR) - birthCal.get(Calendar.YEAR);

        boolean isMonthGreater = birthCal.get(Calendar.MONTH) >= nowCal
                .get(Calendar.MONTH);

        boolean isMonthSameButDayGreater = birthCal.get(Calendar.MONTH) == nowCal
                .get(Calendar.MONTH)
                && birthCal.get(Calendar.DAY_OF_MONTH) > nowCal
                .get(Calendar.DAY_OF_MONTH);

        if (isMonthGreater || isMonthSameButDayGreater) {
            age=age-1;
        }
        return age;

    }

    @Override
    public void onClick(View v) {

        int id = v.getId();

        if(id == R.id.boton_paciente_agregar){
            Log.e("ADD PATIENT->", "A");
            pacienteAgregar();
        }else if (id == R.id.foto_galeria){
            Log.e("ADD PATIENT->", "A");
            agregarImagenGaleria();
        }else if (id == R.id.foto_camara){
            Log.e("ADD PATIENT->", "A");
            agregarImagenCamara();

        }


    }

    private void agregarImagenGaleria() {

        final Intent galleryIntent = new Intent();
        galleryIntent.setType("image/*");
        galleryIntent.setAction(Intent.ACTION_PICK);

        final Intent chooserIntent = Intent.createChooser(galleryIntent, "Select image to upload");
        startActivityForResult(chooserIntent, 1001);


    }


    public void agregarImagenCamara() {


    }

    private void pacienteAgregar() {
        String get_imagen = "http://api.learn2crack.com/android/images/kitkat.png";
        String get_motivo_consulta = motivo_consulta.getText().toString();
        String get_nombres = nombres.getText().toString();
        String get_apellidos = apellidos.getText().toString();
        String get_fecha_nacimiento = fecha_nacimiento.getText().toString();
        String get_edad = edad.getText().toString();
        String get_tipo_identificacion = spinner_identificacion.getSelectedItem().toString();
        String get_numero_identificacion = numero_identificacion.getText().toString();
        String get_genero = spinner_genero.getSelectedItem().toString();
        String get_nacionalidad = spinner_nacionalidad.getSelectedItem().toString();
        String get_estado_civil = spinner_estado_civil.getSelectedItem().toString();
        String get_direccion = direccion.getText().toString();
        String get_email = email.getText().toString();
        //String get_ocupacion = spinner_ocupacion.getSelectedItem().toString();
        String get_ocupacion = ocupacion.getText().toString();
        String get_numero_telefono = numero_telefono.getText().toString();
        String get_referido_por = referido_por.getText().toString();
        String get_contacto = contacto.getText().toString();
        String get_contacto_numero_telefono = contacto_numero_telefono.getText().toString();
        String get_consideraciones_medicas = consideraciones_medicas.getText().toString();

        agregarPaciente(get_imagen, get_motivo_consulta, get_nombres, get_apellidos, get_fecha_nacimiento, get_edad, get_tipo_identificacion, get_numero_identificacion, get_genero, get_nacionalidad, get_estado_civil,
                get_direccion, get_email, get_ocupacion, get_numero_telefono, get_referido_por, get_contacto, get_contacto_numero_telefono, get_consideraciones_medicas);

    }

    private void agregarPaciente(String get_imagen, String get_motivo_consulta, String get_nombres, String get_apellidos, String get_fecha_nacimiento, String get_edad, String get_tipo_identificacion, String get_numero_identificacion, String get_genero, String get_nacionalidad, String get_estado_civil,
                                 String get_direccion, String get_email, String get_ocupacion, String get_numero_telefono, String get_referido_por, String get_contacto, String get_contacto_numero_telefono, String get_consideraciones_medicas) {

        String idUsuario = syn.userId(getApplicationContext());
        Paciente paciente = new Paciente();

        paciente.set_id_doctor(idUsuario);
        paciente.setImagen("http://api.learn2crack.com/android/images/kitkat.png");
        paciente.setMotivo_consulta(motivo_consulta.getText().toString());
        paciente.setFecha_nacimiento(fecha_nacimiento.getText().toString());
        paciente.setEdad(edad.getText().toString());
        paciente.setNombres(nombres.getText().toString());
        paciente.setApellidos(apellidos.getText().toString());
        paciente.setTipo_identificacion( spinner_identificacion.getSelectedItem().toString());
        paciente.setNumero_identificacion( numero_identificacion.getText().toString());
        paciente.setGenero( spinner_genero.getSelectedItem().toString());
        paciente.setNacionalidad( spinner_nacionalidad.getSelectedItem().toString());
        paciente.setEstado_civil( spinner_estado_civil.getSelectedItem().toString());
        paciente.setDireccion( direccion.getText().toString());
        paciente.setEmail( email.getText().toString());
        paciente.setOcupacion( ocupacion.getText().toString());
        paciente.setNumero_telefono( numero_telefono.getText().toString());
        paciente.setReferido_por( referido_por.getText().toString());
        paciente.setContacto( contacto.getText().toString());
        paciente.setContacto_numero_telefono( contacto_numero_telefono.getText().toString());
        paciente.setConsideraciones_medicas( consideraciones_medicas.getText().toString());

        agregarPaciente(paciente);
    }

    private void agregarPaciente(Paciente paciente) {

        final Paciente paciente1 = paciente;

        if (tools.isOnline(getApplicationContext())){
            final ProgressDialog loading = ProgressDialog.show(this, "Grabando", "Por favor aguarde mientras finaliza el proceso", false, false);

            APIPlug cliente = ServiceGenerator.createService(APIPlug.class);

            Call<Response> call = cliente.agregarPaciente(paciente.get_id_doctor(),paciente.getImagen(),paciente.getMotivo_consulta(),
                    paciente.getNombres(),paciente.getApellidos(),paciente.getFecha_nacimiento(),paciente.getEdad(),
                    paciente.getTipo_identificacion(),paciente.getNumero_identificacion(),paciente.getGenero(),paciente.getNacionalidad(),
                    paciente.getEstado_civil(),paciente.getDireccion(),paciente.getEmail(),paciente.getOcupacion(),
                    paciente.getNumero_telefono(),paciente.getReferido_por(),paciente.getContacto(),
                    paciente.getContacto_numero_telefono(),paciente.getConsideraciones_medicas());

            call.enqueue(new Callback<Response>() {
                @Override
                public void onResponse(Call<sigel.smart_dent.beans.Response> call, retrofit2.Response<Response> response) {
                    //Log.e("onResponse", response.body().getInsertedCount().toString());

                    Toast.makeText(getBaseContext(), "Paciente creado correctamente", Toast.LENGTH_SHORT).show();

                    paciente1.setEstado("0");
                    operationsDB.insertPatients(paciente1,response.body().getInsertedIds().toString());

                    //CARGAR IMAGEN

                    //Create Upload Server Client
                    APIPlug service = ServiceGeneratorFileUpload.createService(APIPlug.class);

                    //File creating from selected URL
                    File file = new File(imagePath);

                    // create RequestBody instance from file
                    RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file);

                    // MultipartBody.Part is used to send also the actual file name
                    MultipartBody.Part body = MultipartBody.Part.createFormData("uploaded_file", file.getName(), requestFile);

                    Call<ResponseFileUpload> resultCall = service.uploadFile(body);

                    // finally, execute the request
                    resultCall.enqueue(new Callback<ResponseFileUpload>() {
                        @Override
                        public void onResponse(Call<ResponseFileUpload> call, retrofit2.Response<ResponseFileUpload> response) {

                            //progressDialog.dismiss();

                            // Response Success or Fail
                            if (response.isSuccessful()) {
                                if (response.body().getResult().equals("success"))

                                    Snackbar.make(parentView, R.string.FileUploadSuccess, Snackbar.LENGTH_SHORT).show();
                                else
                                    Snackbar.make(parentView, R.string.FileUploadFail, Snackbar.LENGTH_SHORT).show();

                                loading.dismiss();
                            } else {
                                Snackbar.make(parentView, R.string.FileUploadFail, Snackbar.LENGTH_SHORT).show();
                            }

                            /**
                             * Update Views
                             */
                            //imagePath = "";
                            //textView.setVisibility(View.VISIBLE);
                            //imageView.setVisibility(View.INVISIBLE);

                            startActivity(getIntent());
                            finish();
                        }

                        @Override
                        public void onFailure(Call<ResponseFileUpload> call, Throwable t) {
                            //progressDialog.dismiss();
                        }
                    });

                    //CARGAR IMAGEN




                }

                @Override
                public void onFailure(Call<sigel.smart_dent.beans.Response> call, Throwable t) {
                    //Log.e("onFailure", t.toString());
                    loading.dismiss();
                    Toast.makeText(getBaseContext(), "Por favor intente de nuevo", Toast.LENGTH_SHORT).show();
                    startActivity(getIntent());
                    finish();
                }
            });
        }else {
            paciente1.setEstado("1");
            operationsDB.insertPatients(paciente1, columnsTables.Patients.generarIdPaciente());
            finish();
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }



    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        super.onActivityResult(requestCode, resultCode, data);

        if(resultCode == RESULT_OK && requestCode == 1001 ){

            if(data == null){
                Snackbar.make(parentView, "No se puede seleccionar la imagen", Snackbar.LENGTH_SHORT).show();
                return;
            }

            Uri selectedImageUri = data.getData();
            String[] filePathColumn = {MediaStore.Images.Media.DATA};

            Cursor cursor = getContentResolver().query(selectedImageUri, filePathColumn, null, null, null);

            if(cursor != null){
                cursor.moveToFirst();

                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                imagePath = cursor.getString(columnIndex);

                Picasso.with(mContext).load(new File(imagePath)).into(foto);

                Snackbar.make(parentView, "Click de nuevo en el boton para cambiar la imagen", Snackbar.LENGTH_SHORT).show();
                cursor.close();
            }else{
                Snackbar.make(parentView, "No se puede cargar la imagen", Snackbar.LENGTH_SHORT).show();
            }

        }

        /*else if(resultCode == RESULT_OK && requestCode == 1002 ){

            if(data == null){
                Snackbar.make(parentView, "Unable to get your image", Snackbar.LENGTH_INDEFINITE).show();
                return;
            }

            Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
            image_view.setImageBitmap(thumbnail);

            String pathToImage = fileUri.getPath();

            Snackbar.make(parentView, pathToImage, Snackbar.LENGTH_INDEFINITE).show();


            //filePath = data.getStringExtra("filePath");

            //Snackbar.make(parentView, filePath, Snackbar.LENGTH_INDEFINITE).show();

            //Log.e("FILEPATHFORRESULT", filePath.toString());

            /////
            Uri selectedImageUri = data.getData();
            String[] filePathColumn = {MediaStore.Images.Media.DATA};

            Cursor cursor = getContentResolver().query(selectedImageUri, filePathColumn, null, null, null);

            if(cursor != null){
                cursor.moveToFirst();

                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                imagePath = cursor.getString(columnIndex);

                Picasso.with(mContext).load(new File(imagePath)).into(image_view);

                Snackbar.make(parentView, "Click on button to reselect", Snackbar.LENGTH_INDEFINITE).show();
                cursor.close();
            }else{
                Snackbar.make(parentView, "Unable to load image", Snackbar.LENGTH_INDEFINITE).show();
            }
            ////



        }*/

    }



}
package sigel.smart_dent.modules.patient_management.plaque.adapters;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import sigel.smart_dent.modules.patient_management.plaque.fragments.PlaquePageFragment;

/**
 * Created by pablin on 5/10/2016.
 */
public class PlaqueFragmentPageAdapter extends FragmentPagerAdapter {

    final int PAGE_COUNT = 4;
    private String tabTitles[] =  new String[]{"Cuadrante 1", "Cuadrante 2", "Cuadrante 3", "Cuadrante 4" };
    private Context context;


    public PlaqueFragmentPageAdapter(FragmentManager fm, Context context) {
        super(fm);
        this.context = context;
    }

    public PlaqueFragmentPageAdapter(FragmentManager fm) {
        super(fm);
        //this.context = context;
    }

    @Override
    public Fragment getItem(int position) {
        return PlaquePageFragment.newInstance(position + 1);
    }

    @Override
    public int getCount() {
        return PAGE_COUNT;
    }

    @Override
    public CharSequence getPageTitle(int position){
        return tabTitles[position];
    }
}

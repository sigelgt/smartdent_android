package sigel.smart_dent.modules.patient_management.appointments;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import sigel.smart_dent.R;
import sigel.smart_dent.modules.PatientsManagementFrameActivity;

/**
 * Created by pablin on 30/09/2016.
 */
public class PacienteAgregarCita extends PatientsManagementFrameActivity implements View.OnClickListener {

    public static String patientId = null;
    public static final String EXTRA_PACIENTE = "PATIENT_ID";

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);

        if(getIntent().hasExtra(EXTRA_PACIENTE)){
            patientId = getIntent().getExtras().getString(EXTRA_PACIENTE);
            Log.e("PATIENT_ID", patientId);

        }else{
            Toast.makeText(getBaseContext(), "Por favor seleccione un paciente", Toast.LENGTH_LONG).show();
        }

    }

    @Override
    public void onClick(View v) {

    }

    @Override
    public String setViewTitle() {
        return "Agregar Cita";
    }

    @Override
    protected int getActivityLayout() {
        return R.layout.activity_paciente_agregar_cita;
    }


}

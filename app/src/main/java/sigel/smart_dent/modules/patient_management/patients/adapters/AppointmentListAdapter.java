package sigel.smart_dent.modules.patient_management.patients.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import sigel.smart_dent.R;
import sigel.smart_dent.modules.patient_management.patients.fragments.AppointmentListFragment;
import sigel.smart_dent.modules.patient_management.patients.models.AppointmentsExample;

/**
 * Created by Juan on 19/06/2016.
 */
public class AppointmentListAdapter extends RecyclerView.Adapter {
    private final AppointmentListFragment.AppointmentInterface mListener;

    public AppointmentListAdapter(AppointmentListFragment.AppointmentInterface listener) {
        mListener = listener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.appointment_list_item, parent, false);
        return new AppointmentListViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ((AppointmentListViewHolder) holder).bindView(position);
    }

    @Override
    public int getItemCount() {
        return mListener.getAppointmentsListCount();
    }


    private class AppointmentListViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private TextView mPatientNameLabel;
        private TextView mAppointmentPalceLabel;
        private TextView mAppointmentTime;
        private TextView mAppointmentProcedure;
        private int mAppointmentId;
        private int mIndex;

        public AppointmentListViewHolder(View itemView) {
            super(itemView);
            //this.mAppointmentId = (TextView) itemView.findViewById(R.id.patienNameLabel);
            this.mAppointmentProcedure = (TextView) itemView.findViewById(R.id.procedureNameLabel);
            this.mPatientNameLabel = (TextView) itemView.findViewById(R.id.patienNameLabel);
            this.mAppointmentPalceLabel = (TextView) itemView.findViewById(R.id.appointmentPlaceLabel);
            this.mAppointmentTime = (TextView) itemView.findViewById(R.id.appointmentTimeLabel);
            itemView.setOnClickListener(this);
        }

        public void bindView (int position){
            this.mIndex = position;
            /*this.mAppointmentProcedure.setText(mListener.getAppointmentProcedure(position));
            this.mPatientNameLabel.setText(mListener.getAppointmentPatientName(position));
            this.mAppointmentPalceLabel.setText(mListener.getAppointmentPlace(position));
            this.mAppointmentTime.setText(mListener.getAppointmentTime(position));
            this.mAppointmentId = mListener.getAppointmentId(position);
            */
            this.mAppointmentProcedure.setText(AppointmentsExample.procedures[position]);
            this.mPatientNameLabel.setText(AppointmentsExample.names[position]);
            this.mAppointmentPalceLabel.setText(AppointmentsExample.places[position]);
            this.mAppointmentTime.setText(AppointmentsExample.time[position]);
            this.mAppointmentId = AppointmentsExample.appointmentIds[position];
        }

        @Override
        public void onClick(View v) {
            mListener.onAppointmentItemSelected(this.mIndex);
        }


    }

}
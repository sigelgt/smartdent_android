package sigel.smart_dent.modules.patient_management.endodontics.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Switch;

import sigel.smart_dent.R;

public class EndodonthicsFormFragment extends Fragment {

    public String mThootId;
    public int mTheetImageId;

    //SECCION DE DOLOR
    private Switch mDolor;
    private Spinner mDolorIntensidad;
    private Spinner mInicioDolor;
    private Spinner mDolorDuracion;
    private Spinner mDolorFrecuencia;
    private Spinner mDolorTipo;
    private Spinner mDolorLocalizacion;
    private Spinner mDolorProvocadoPor;
    private Spinner mDolorAliviadoPor;

    //SECCION DE EXAMEN RADIOGRAFICO
    private Spinner mCalcificacion;
    private Spinner mFracturaRadicular;
    private Spinner mInstrumentoFracturado;
    private Spinner mLesionApical;
    private Spinner mLigamentoPeriodontal;
    private Switch mNodulosPulpares;
    private Spinner mPinIntraradicular;
    private Spinner mReabsorsion;
    private Spinner mTratamientoPrevio;
    private Spinner mDiagnostico;



    public String getThootId() {
        return mThootId;
    }

    public int getTheetImageId() {
        return mTheetImageId;
    }

    public String getThootTitle() {
        return String.valueOf(mThootId);
    }

    public EndodonthicsFormFragment() {
        // Required empty public constructor
    }

    public void setInitParams(String thootId, int resourceId) {
        this.mThootId = thootId;
        this.mTheetImageId = resourceId;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_endodonthics_form, container, false);
        //getActivity().setTitle(getThootTitle());

        mDolorIntensidad = initSpinner(view, R.id.endodoncia_intensidad_dolor_spinner, R.array.from1to10);
        mInicioDolor =  initSpinner(view, R.id.endodoncia_inicio_dolor_spinner, R.array.inicio_dolor);
        mDolorDuracion  =  initSpinner(view, R.id.endodoncia_duracion_dolor_spinner, R.array.duracion_dolor);
        mDolorFrecuencia  =  initSpinner(view, R.id.endodoncia_frecuencia_spinner, R.array.frecuencia_dolor);
        mDolorTipo  =  initSpinner(view, R.id.endodoncia_tipo_dolor_spinner, R.array.tipo_dolor);
        mDolorLocalizacion  =  initSpinner(view, R.id.endodoncia_localizacion_dolor_spinner, R.array.localizacion_dolor);
        mDolorProvocadoPor =  initSpinner(view, R.id.endodoncia_provocado_dolor_spinner, R.array.provocado_dolor);
        mDolorAliviadoPor =  initSpinner(view, R.id.endodoncia_aliviado_dolor_spinner, R.array.alividado_dolor);

        //EXAMEN RADIOGRAFICO
        mCalcificacion = initSpinner(view, R.id.endodoncia_calcificacion_spinner, R.array.endodoncia_calcificacion);
        mFracturaRadicular = initSpinner(view, R.id.endodoncia_fractura_radicular_spinner, R.array.fractura_radicular);
        mInstrumentoFracturado = initSpinner(view, R.id.endodoncia_instrumento_fracturado_spinner, R.array.instrumento_fracturado);
        mLesionApical = initSpinner(view, R.id.endodoncia_lesion_apical_spinner, R.array.lesion_apical);
        mLigamentoPeriodontal = initSpinner(view, R.id.endodoncia_ligamento_periodontal_spinner, R.array.ligamento_periodontal);
        mPinIntraradicular  = initSpinner(view, R.id.endodoncia_pin_intraradicular_spinner, R.array.pin_intraradicular);
        mReabsorsion  = initSpinner(view, R.id.endodoncia_reabsoricion_spinner, R.array.reabsorcion);
        mTratamientoPrevio  = initSpinner(view, R.id.endodoncia_tratamiento_previo_spinner, R.array.tratamiento_previo);
        mDiagnostico = initSpinner(view, R.id.endodoncia_diagnostico_examen_radiografico_spinner, R.array.endodoncia_diagnosticos);

        //checkSwitchSection(mProblemasCardiacos,mProblemasCardiacosSection);

        return view;
    }


    private Spinner initSpinner(View v, int layoutId, int arrayId){
        Spinner spinnerItem = (Spinner) v.findViewById(layoutId);
        ArrayAdapter<CharSequence> SpinnerAdapter = ArrayAdapter.createFromResource(getActivity(), arrayId, R.layout.custom_spinner_item);
        SpinnerAdapter.setDropDownViewResource(R.layout.custom_spinner_dropdown_item);
        spinnerItem.setAdapter(SpinnerAdapter);
        return spinnerItem;
    }



    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();
        //mListener = null;
        Log.d("NINJA", "on Detach del fragment : " + this.mThootId);
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.d("NINJA", "on Pause del fragment : " + this.mThootId);
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.d("NINJA", "on Stop del fragment : " + this.mThootId);
    }
}

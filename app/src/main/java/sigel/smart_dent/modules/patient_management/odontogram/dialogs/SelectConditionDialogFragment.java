package sigel.smart_dent.modules.patient_management.odontogram.dialogs;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.widget.Toast;

import java.util.ArrayList;

import sigel.smart_dent.R;

/**
 * Created by Juan on 17/06/2016.
 */
public class SelectConditionDialogFragment extends DialogFragment  {
    ArrayList mSelectedItems;
    AlertDialog.Builder mDialogBuilder;



    public interface SelectConditionDialogFragmentListener{
        public void onToothConditionsSelect(int index);

    }
    public SelectConditionDialogFragmentListener mListener;
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            mSelectedItems = new ArrayList();
            // Use the Builder class for convenient dialog construction
            mDialogBuilder = new AlertDialog.Builder(getActivity()).setTitle(R.string.odontogram_seccion_title)
                    .setItems(R.array.condition_categories, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            mListener.onToothConditionsSelect(which);
                            mSelectedItems.add(which);
                            dialog.dismiss();
                        }
                    })
                    .setPositiveButton(R.string.close_dialog_button_label, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                        }
                    });
            return mDialogBuilder.create();
        }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        // Verify that the host activity implements the callback interface
        try {
            // Instantiate the NoticeDialogListener so we can send events to the host
            mListener = (SelectConditionDialogFragmentListener) activity;
        } catch (ClassCastException e) {
            // The activity doesn't implement the interface, throw exception
            throw new ClassCastException(activity.toString()
                    + " must implement NoticeDialogListener");
        }
    }



    public void onClickPositiveButton(DialogInterface dialog, int id){

    }

    public void makeToast(String msg){
        Context ctx = getActivity().getBaseContext();
        Toast.makeText(ctx, msg, Toast.LENGTH_SHORT).show();
    }



}

package sigel.smart_dent.modules.patient_management.odontogram.activities;

import android.app.DialogFragment;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.widget.Toast;

import sigel.smart_dent.R;
import sigel.smart_dent.modules.patient_management.odontogram.dialogs.ToothConditionsDialog;
import sigel.smart_dent.modules.patient_management.odontogram.fragments.Cuadrante1Fragment;
import sigel.smart_dent.modules.patient_management.odontogram.fragments.Cuadrante2Fragment;
import sigel.smart_dent.modules.patient_management.odontogram.fragments.Cuadrante3Fragment;
import sigel.smart_dent.modules.patient_management.odontogram.fragments.Cuadrante4Fragment;
import sigel.smart_dent.modules.patient_management.odontogram.fragments.OdontogramViewPagerFragment;
import sigel.smart_dent.modules.patient_management.odontogram.dialogs.SelectConditionDialogFragment;
import sigel.smart_dent.modules.patient_management.odontogram.models.Theeth;

public class OdontogramCanvas extends FragmentActivity implements Cuadrante1Fragment.OnOdontogramItemSelectedInterface,
        Cuadrante2Fragment.OnOdontogramItemSelectedInterface,
        Cuadrante3Fragment.OnOdontogramItemSelectedInterface,
        Cuadrante4Fragment.OnOdontogramItemSelectedInterface,
        SelectConditionDialogFragment.SelectConditionDialogFragmentListener,
        ToothConditionsDialog.SelectToothFragmentListener
    {

        private static final String VIEWPAGER_FRAGMENT = "VIEWPAGER_FRAGMENT";
        private static final String DIALOGTAG = "NINJA";
        private static final String ONE_CUADRANT_ODONTOGRAM_FRAGMENT = "ONE_CUADRANT_ODONTOGRAM_FRAGMENT";
        private static final String TWO_CUADRANT_ODONTOGRAM_FRAGMENT = "TWO_CUADRANT_ODONTOGRAM_FRAGMENT";
        private static final String FOUR_CUADRANT_ODONTOGRAM_FRAGMENT = "FOUR_CUADRANT_ODONTOGRAM_FRAGMENT";

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_odontogram_canvas);

                OdontogramViewPagerFragment savedFragment = (OdontogramViewPagerFragment) getSupportFragmentManager().findFragmentByTag(this.ONE_CUADRANT_ODONTOGRAM_FRAGMENT);
                if(savedFragment==null) {
                    OdontogramViewPagerFragment fragment = new OdontogramViewPagerFragment();
                    FragmentManager fragmentManager = getSupportFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.add(R.id.odontogramaCanvasPlaceHolder, fragment, this.ONE_CUADRANT_ODONTOGRAM_FRAGMENT);
                    fragmentTransaction.commit();
                }



            //Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
            //setSupportActionBar(toolbar);



            /*FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
            fab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                }
            });*/


        }

        @Override
        public void onCudrant3GridItemSelected(int index) {

        }

        @Override
        public void onCudrant4GridItemSelected(int index) {

        }

        @Override
        public void onCudrant1GridItemSelected(int index) {
            theOnClickItemEvent(index, Theeth.cudarante1b);
        }

        @Override
        public void onCudrant2GridItemSelected(int index) {

        }

        public void theOnClickItemEvent(int index, String [] cuadrante){
            openSelectConditions();
        }

       public void testToast(String msg){
            Toast.makeText(this,msg,Toast.LENGTH_LONG).show();
        }


        @Override
        public void onToothConditionsSelect(int index) {

            switch (index){
                case 0:
                    //getBaseContext().
                    //getSupportFragmentManager();
                    ToothConditionsDialog dialog = new ToothConditionsDialog();
                    dialog.show(getSupportFragmentManager(),"ToothConditionList");
                    break;
                default:
                    break;
            }


        }

        @Override
        public void onToothConditionListCloseButton() {
            openSelectConditions();
        }

        @Override
        public void onCariesSelect() {
            testToast("click en caries");
        }

        public void openSelectConditions(){
            DialogFragment dialog = new SelectConditionDialogFragment();
            dialog.show(getFragmentManager(),"NoticeDialogFragment");
        }
    }


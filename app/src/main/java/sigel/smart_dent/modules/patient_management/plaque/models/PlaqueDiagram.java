package sigel.smart_dent.modules.patient_management.plaque.models;

import android.view.View;
import android.widget.ImageView;

/**
 * Created by Juan on 1/07/2016.
 */
public class PlaqueDiagram {

    private ImageView mTop;
    private ImageView mBottom;
    private ImageView mLeft;
    private ImageView mRight;
    private ImageView mCenter;

    public ImageView getTop() {	return mTop; }
    public void setTop(ImageView top) {	mTop = top;	}
    public void setTopVisible(){ mTop.setVisibility(View.VISIBLE); }
    public void unsetTopVisible(){ mTop.setVisibility(View.INVISIBLE); }



    public ImageView getBottom() {	return mBottom;	}
    public void setBottom(ImageView bottom) {	mBottom = bottom;	}
    public void setBottomVisible(){ mBottom.setVisibility(View.VISIBLE); }
    public void unsetBottomVisible(){ mBottom.setVisibility(View.INVISIBLE); }

    public ImageView getLeft() {	return mLeft;	}
    public void setLeft(ImageView left) {	mLeft = left;	}
    public void setLeftVisible(){ mLeft.setVisibility(View.VISIBLE); }
    public void unsetLeftVisible(){ mLeft.setVisibility(View.INVISIBLE); }


    public ImageView getRight() {	return mRight;	}
    public void setRight(ImageView right) {	mRight = right;	}
    public void setRightVisible(){ mRight.setVisibility(View.VISIBLE); }
    public void unsetRightVisible(){ mRight.setVisibility(View.INVISIBLE); }

    public ImageView getCenter() {	return mCenter;	}
    public void setCenter(ImageView center) {	mCenter = center;	}
    public void setCenterVisible(){ mTop.setVisibility(View.VISIBLE); }
    public void unsetCenterVisible(){ mTop.setVisibility(View.INVISIBLE); }
}

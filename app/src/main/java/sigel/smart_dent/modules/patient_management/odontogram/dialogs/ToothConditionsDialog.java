package sigel.smart_dent.modules.patient_management.odontogram.dialogs;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.support.v4.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;

import java.util.ArrayList;

import sigel.smart_dent.R;

/**
 * Created by Juan on 17/06/2016.
 */
public class ToothConditionsDialog extends DialogFragment {
    ArrayList mSelectedItems;
    AlertDialog.Builder mDialogBuilder;
    public interface SelectToothFragmentListener {
        public void onToothConditionListCloseButton();
        public void onCariesSelect();
    }

    SelectToothFragmentListener mListener;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        mSelectedItems = new ArrayList();
        // Use the Builder class for convenient dialog construction
        mDialogBuilder = new AlertDialog.Builder(getActivity());
        mDialogBuilder.setTitle(R.string.select_condition_title_text)
                    .setMultiChoiceItems(R.array.array_odontogram_tooth_condition_category_list, null,
                        new DialogInterface.OnMultiChoiceClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which,
                                                boolean isChecked) {
                                if (isChecked) {
                                    // If the user checked the item, add it to the selected items
                                    mSelectedItems.add(which);
                                } else if (mSelectedItems.contains(which)) {
                                    // Else, if the item is already in the array, remove it
                                    mSelectedItems.remove(Integer.valueOf(which));
                                }
                            }
                        })
                .setPositiveButton(R.string.done_dialog_button_label, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        mListener.onToothConditionListCloseButton();
                    }
                });
                    /*.setNegativeButton(R.string.cancel_dialog_button_label, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            mListener.onDialogNegativeClick(SelectConditionDialogFragment.this);
                        }
                    })*/
        // Create the AlertDialog object and return it

        return mDialogBuilder.create();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        // Verify that the host activity implements the callback interface
        try {
            // Instantiate the NoticeDialogListener so we can send events to the host
            mListener = (SelectToothFragmentListener) activity;
        } catch (ClassCastException e) {
            // The activity doesn't implement the interface, throw exception
            throw new ClassCastException(activity.toString()
                    + " must implement NoticeDialogListener");
        }
    }

    public void onSelectCondition(int index){

    }

    public void onClickPositiveButton(DialogInterface dialog, int id){

    }
}
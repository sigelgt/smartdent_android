package sigel.smart_dent.modules.patient_management.patients;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import sigel.smart_dent.modules.user_management.activities.Dashboard;
import sigel.smart_dent.modules.user_management.activities.Login;
import sigel.smart_dent.modules.patient_management.atm.PacienteATM;
import sigel.smart_dent.modules.patient_management.endodontics.PacienteExamenEndodoncia;
import sigel.smart_dent.modules.patient_management.odontology.PacienteExamenOrtodoncia;
import sigel.smart_dent.modules.patient_management.medical_record.PacienteHistoriaMedica;
import sigel.smart_dent.modules.patient_management.dental_record.activities.PacienteHistoriaOdontologica;
import sigel.smart_dent.modules.patient_management.plaque.old_files.PacientePlacaBacteriana;
import sigel.smart_dent.R;
import sigel.smart_dent.models.Paciente;

/**
 * Created by pablin on 25/05/2016.
 */
public class PacienteListarDetalle extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    public static final String EXTRA_PACIENTE = "PATIENT_ID";

    private Paciente detallePaciente;
    ImageView imagen;
    TextView nombres, apellidos, edad, tipo_identificacion, numero_identificacion, genero, nacionalidad, estado_civil,
            direccion, email, ocupacion, numero_telefono, referido_por, contacto, contacto_numero_telefono, consideraciones_especiales;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_paciente_listar_detalle);

        if(getIntent().hasExtra(EXTRA_PACIENTE)){
            detallePaciente = getIntent().getParcelableExtra(EXTRA_PACIENTE);
        }else{
                throw new IllegalArgumentException("La actividad paciente detalle debe recibir un paciente parcelable");
        }


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        setTitle("Información de paciente");

        imagen = (ImageView) findViewById(R.id.detalle_imagen);
        nombres = (TextView) findViewById(R.id.detalle_nombres);
        apellidos = (TextView) findViewById(R.id.detalle_apellidos);
        edad = (TextView) findViewById(R.id.detalle_edad);
        tipo_identificacion = (TextView) findViewById(R.id.detalle_tipo_identificacion);
        numero_identificacion = (TextView) findViewById(R.id.detalle_numero_identificacion);
        genero = (TextView) findViewById(R.id.detalle_genero);
        nacionalidad = (TextView) findViewById(R.id.detalle_nacionalidad);
        estado_civil = (TextView) findViewById(R.id.detalle_estado_civil);
        direccion = (TextView) findViewById(R.id.detalle_direccion);
        email = (TextView) findViewById(R.id.detalle_email);
        ocupacion = (TextView) findViewById(R.id.detalle_ocupacion);
        numero_telefono = (TextView) findViewById(R.id.detalle_numero_telefono);
        referido_por = (TextView) findViewById(R.id.detalle_referido_por);
        contacto = (TextView) findViewById(R.id.detalle_contacto);
        contacto_numero_telefono = (TextView) findViewById(R.id.detalle_contacto_numero_telefono);
        consideraciones_especiales = (TextView) findViewById(R.id.detalle_consideraciones_especiales);


        Picasso.with(this)
        .load(detallePaciente.getImagen())
        .into(imagen);

        nombres.setText(detallePaciente.getNombres());
        apellidos.setText(detallePaciente.getApellidos());
//        edad.setText(detallePaciente.getEdad());
//        tipo_identificacion.setText(detallePaciente.getTipo_identificacion());
//        numero_identificacion.setText(detallePaciente.getNumero_identificacion());
        genero.setText(detallePaciente.getGenero());
        nacionalidad.setText(detallePaciente.getNacionalidad());
        estado_civil.setText(detallePaciente.getEstado_civil());
        direccion.setText(detallePaciente.getDireccion());
        email.setText(detallePaciente.getEmail());
        ocupacion.setText(detallePaciente.getOcupacion());
        numero_telefono.setText(detallePaciente.getNumero_telefono());
        referido_por.setText(detallePaciente.getReferido_por());
        contacto.setText(detallePaciente.getContacto());
        contacto_numero_telefono.setText(detallePaciente.getContacto_numero_telefono());
        consideraciones_especiales.setText(detallePaciente.getConsideraciones_medicas());

    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_paciente_listar_detalle, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){

        int id = item.getItemId();

        if(id == R.id.paciente_menu_historia_medica){
            startActivity(new Intent(this, PacienteHistoriaMedica.class));
        }else if(id == R.id.paciente_menu_historia_odontologica){
            startActivity(new Intent(this, PacienteHistoriaOdontologica.class));
        }else if(id == R.id.paciente_menu_examen_atm){
            startActivity(new Intent(this, PacienteATM.class));
        }else if(id == R.id.paciente_placa_bacteriana){
            startActivity(new Intent(this, PacientePlacaBacteriana.class));
        }else if(id == R.id.paciente_menu_examen_ortodoncia){
            startActivity(new Intent(this, PacienteExamenOrtodoncia.class));
        }else if(id == R.id.paciente_menu_examen_endodoncia) {
            startActivity(new Intent(this, PacienteExamenEndodoncia.class));
        }else if(id == R.id.paciente_menu_odontograma){
            Toast.makeText(getBaseContext(), "CLICKED ON ODONTOGRAM", Toast.LENGTH_SHORT).show();
        } else {
            //Toast.makeText(getBaseContext(), "OTRA OPCION MENU LISTAR OTRA COSA", Toast.LENGTH_SHORT).show();
        }
        return true;
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.paciente_listado) {
            startActivity(new Intent(this, PacienteListar.class));
        } else if (id == R.id.usuario_dashboard) {
            startActivity(new Intent(this, Dashboard.class));
        } else if (id == R.id.salir) {
            startActivity(new Intent(this, Login.class));
        } else {
            //Toast.makeText(getBaseContext(), "OTRA OPCION", Toast.LENGTH_SHORT).show();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}

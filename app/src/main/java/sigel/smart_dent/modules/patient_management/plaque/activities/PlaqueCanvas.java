package sigel.smart_dent.modules.patient_management.plaque.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.view.View;

import sigel.smart_dent.R;
import sigel.smart_dent.modules.PatientsManagementFrameActivity;
import sigel.smart_dent.modules.patient_management.plaque.adapters.PlaqueFragmentPageAdapter;
import sigel.smart_dent.modules.patient_management.plaque.fragments.PlaqueCuadrantFragment;
import sigel.smart_dent.modules.patient_management.plaque.fragments.PlaqueViewPagerFragment;

/**
 * Created by Juan on 1/07/2016.
 */
public class PlaqueCanvas extends PatientsManagementFrameActivity implements PlaqueCuadrantFragment.plaqueCuadrantInterface {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        /*
        ViewPager viewPager = (ViewPager) findViewById(R.id.genericViewPager);
        viewPager.setAdapter(new PlaqueFragmentPageAdapter(getSupportFragmentManager()) {
        });

        TabLayout tabLayout = (TabLayout) findViewById(R.id.genericTabLayout);
        tabLayout.setupWithViewPager(viewPager);
        */
        //VERSION FUNCIONAL

        PlaqueViewPagerFragment savedFragment = (PlaqueViewPagerFragment) getSupportFragmentManager().findFragmentByTag("PLAQUE_PAGER");
        if(savedFragment==null) {
            PlaqueViewPagerFragment viewPager = new PlaqueViewPagerFragment();
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.add(R.id.plaquePlaceHolder, viewPager, "PLAQUE_PAGER");
            fragmentTransaction.commit();
        }


    }

    @Override
    public String setViewTitle() {
        return getResources().getString(R.string.title_activity_plaque_canvas);
    }

    @Override
    protected int getActivityLayout() {
        return R.layout.activity_paciente_plaque_canvas;
    }

    @Override
    public void onCuadrantListItemSelect(int index, View v) {

    }

}

package sigel.smart_dent.modules.patient_management.patients.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import sigel.smart_dent.R;
import sigel.smart_dent.modules.patient_management.patients.fragments.PatientsListFragment;

/**
 * Created by Juan on 19/06/2016.
 */
public class PatientListAdapter extends RecyclerView.Adapter {
    private final PatientsListFragment.OnPatientSelectedInterface mListener;

    public PatientListAdapter(PatientsListFragment.OnPatientSelectedInterface listener) {
        mListener = listener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        //View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item, parent, false);
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.patients_list_item, parent, false);
        return new PatientListViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ((PatientListViewHolder) holder).bindView(position);
    }

    @Override
    public int getItemCount() {
        return mListener.getPatientsListCount();
    }


    private class PatientListViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private TextView mTextView;
        private int mIndex;

        public PatientListViewHolder(View itemView) {
            super(itemView);
            this.mTextView = (TextView) itemView.findViewById(R.id.patienNameLabel);
            itemView.setOnClickListener(this);
        }

        public void bindView (int position){
            this.mIndex = position;
            this.mTextView.setText(mListener.getPatientName(mIndex));
        }

        @Override
        public void onClick(View v) {
            mListener.onListPatientSelected(this.mIndex);
        }


    }

}
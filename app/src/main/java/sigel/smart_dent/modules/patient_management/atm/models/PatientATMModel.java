package sigel.smart_dent.modules.patient_management.atm.models;

/**
 * Created by pablin on 13/07/2016.
 */
public class PatientATMModel {

    private Boolean htf, bruxismo, tratamiento_ortodoncia, ruido_articular, artritis,
            chasquido_izquierdo_ninguno, chasquido_izquierdo_apertura, chasquido_izquierdo_cierre, chasquido_derecho_ninguno, chasquido_derecho_apertura, chasquido_derecho_cierre,
            crepitacion_izquierdo_ninguno, crepitacion_izquierdo_articular, crepitacion_izquierdo_muscular, crepitacion_derecho_ninguno, crepitacion_derecho_articular, crepitacion_derecho_muscular,
            dolor_izquierdo_ninguno, dolor_izquierdo_articular, dolor_izquierdo_muscular, dolor_derecho_ninguno, dolor_derecho_articular, dolor_derecho_muscular,
            apertura_recto, apertura_ala_izquierda, apertura_ala_derecha, cerrado_recto, cerrado_ala_izquierda, cerrado_ala_derecha;

    public Boolean getHtf() { return htf; }

    public Boolean getBruxismo() {
        return bruxismo;
    }

    public Boolean getTratamiento_ortodoncia() {
        return tratamiento_ortodoncia;
    }

    public Boolean getRuido_articular() {
        return ruido_articular;
    }

    public Boolean getArtritis() {
        return artritis;
    }

    public Boolean getChasquido_izquierdo_ninguno() {
        return chasquido_izquierdo_ninguno;
    }

    public Boolean getChasquido_izquierdo_apertura() {
        return chasquido_izquierdo_apertura;
    }

    public Boolean getChasquido_izquierdo_cierre() {
        return chasquido_izquierdo_cierre;
    }

    public Boolean getChasquido_derecho_ninguno() {
        return chasquido_derecho_ninguno;
    }

    public Boolean getChasquido_derecho_apertura() {
        return chasquido_derecho_apertura;
    }

    public Boolean getChasquido_derecho_cierre() {
        return chasquido_derecho_cierre;
    }

    public Boolean getCrepitacion_izquierdo_ninguno() {
        return crepitacion_izquierdo_ninguno;
    }

    public Boolean getCrepitacion_izquierdo_articular() {
        return crepitacion_izquierdo_articular;
    }

    public Boolean getCrepitacion_izquierdo_muscular() {
        return crepitacion_izquierdo_muscular;
    }

    public Boolean getCrepitacion_derecho_ninguno() {
        return crepitacion_derecho_ninguno;
    }

    public Boolean getCrepitacion_derecho_articular() {
        return crepitacion_derecho_articular;
    }

    public Boolean getCrepitacion_derecho_muscular() {
        return crepitacion_derecho_muscular;
    }

    public Boolean getDolor_izquierdo_ninguno() {
        return dolor_izquierdo_ninguno;
    }

    public Boolean getDolor_izquierdo_articular() {
        return dolor_izquierdo_articular;
    }

    public Boolean getDolor_izquierdo_muscular() {
        return dolor_izquierdo_muscular;
    }

    public Boolean getDolor_derecho_ninguno() {
        return dolor_derecho_ninguno;
    }

    public Boolean getDolor_derecho_articular() {
        return dolor_derecho_articular;
    }

    public Boolean getDolor_derecho_muscular() {
        return dolor_derecho_muscular;
    }

    public Boolean getApertura_recto() {
        return apertura_recto;
    }

    public Boolean getApertura_ala_izquierda() {
        return apertura_ala_izquierda;
    }

    public Boolean getApertura_ala_derecha() {
        return apertura_ala_derecha;
    }

    public Boolean getCerrado_recto() {
        return cerrado_recto;
    }

    public Boolean getCerrado_ala_izquierda() {
        return cerrado_ala_izquierda;
    }

    public Boolean getCerrado_ala_derecha() {
        return cerrado_ala_derecha;
    }
}

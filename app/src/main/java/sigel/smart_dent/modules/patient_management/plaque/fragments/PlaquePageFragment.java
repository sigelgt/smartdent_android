package sigel.smart_dent.modules.patient_management.plaque.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import sigel.smart_dent.R;

/**
 * Created by pablin on 5/10/2016.
 */
public class PlaquePageFragment extends Fragment {

    public static final String ARG_PAGE = "ARG_PAGE";
    private int mPage;

    public static PlaquePageFragment newInstance(int page){
        Bundle args = new Bundle();
        args.putInt(ARG_PAGE, page);
        PlaquePageFragment fragment = new PlaquePageFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        mPage = getArguments().getInt(ARG_PAGE);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        View view = inflater.inflate(R.layout.activity_paciente_plaque_page_fragment, container, false);

        TextView textView = (TextView) view;
        textView.setText("Cuadrante # " + mPage);
        return view;
    }

}

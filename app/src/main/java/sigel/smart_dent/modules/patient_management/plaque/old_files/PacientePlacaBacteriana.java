package sigel.smart_dent.modules.patient_management.plaque.old_files;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.view.View;
import android.widget.Button;

import sigel.smart_dent.R;
import sigel.smart_dent.modules.PatientsManagementFrameActivity;

/**
 * Created by pablin on 1/06/2016.
 */
public class PacientePlacaBacteriana extends PatientsManagementFrameActivity implements NavigationView.OnNavigationItemSelectedListener, View.OnClickListener  {

    private Button boton_pb_maxilar_der, boton_pb_maxilar_izq, boton_pb_mandibular_der, boton_pb_mandibular_izq;
    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);

        boton_pb_maxilar_der = (Button) findViewById(R.id.boton_pb_maxilar_der);
        boton_pb_maxilar_izq = (Button) findViewById(R.id.boton_pb_maxilar_izq);
        boton_pb_mandibular_der = (Button) findViewById(R.id.boton_pb_mandibular_der);
        boton_pb_mandibular_izq = (Button) findViewById(R.id.boton_pb_mandibular_izq);

        boton_pb_maxilar_der.setOnClickListener(this);
        boton_pb_maxilar_izq.setOnClickListener(this);
        boton_pb_mandibular_der.setOnClickListener(this);
        boton_pb_mandibular_izq.setOnClickListener(this);


    }

    @Override
    public String setViewTitle() {
        return "Placa Bacteriana";
    }

    @Override
    protected int getActivityLayout() {
        return R.layout.activity_paciente_placa_bacteriana;
    }


    @Override
    public void onClick(View v) {


        switch(v.getId()){

            case R.id.boton_pb_maxilar_der: /** Start a new Activity MyCards.java */

                startActivity(new Intent(this, PacientePlacaBacterianaMaxilarDerecha.class));

                break;

            case R.id.boton_pb_maxilar_izq: /** Start a new Activity MyCards.java */

                //startActivity(new Intent(this, PacientePlacaBacterianaMaxilarDerecha.class));
                startActivity(new Intent(this, PacientePlacaBacterianaMaxilarIzquierda.class));


                break;

            case R.id.boton_pb_mandibular_der: /** Start a new Activity MyCards.java */
                //startActivity(new Intent(this, PacientePlacaBacterianaMaxilarDerecha.class));
                startActivity(new Intent(this, PacientePlacaBacterianaMandibularDerecha.class));

                break;

            case R.id.boton_pb_mandibular_izq: /** Start a new Activity MyCards.java */
                //startActivity(new Intent(this, PacientePlacaBacterianaMaxilarDerecha.class));
                startActivity(new Intent(this, PacientePlacaBacterianaMandibularIzquierda.class));

                break;
            default: /** AlerDialog when click on Exit */

                break;
        }



    }
}

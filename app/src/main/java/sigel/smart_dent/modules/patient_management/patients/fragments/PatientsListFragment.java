package sigel.smart_dent.modules.patient_management.patients.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import sigel.smart_dent.R;
import sigel.smart_dent.modules.patient_management.patients.adapters.PatientListAdapter;

/**
 * Created by Juan on 19/06/2016.
 */
public class PatientsListFragment extends Fragment {

    public interface OnPatientSelectedInterface {
        void onListPatientSelected (int index);
        String getPatientName (int index);
        int getPatientsListCount();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        //patienListRecyclerView
        View view = inflater.inflate(R.layout.fragment_recycleview,container,false);
        OnPatientSelectedInterface listener = (OnPatientSelectedInterface) getActivity();
        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.recyclerView);
        PatientListAdapter recyclerAdapter = new PatientListAdapter(listener);
        recyclerView.setAdapter(recyclerAdapter);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        return view;

    }
}

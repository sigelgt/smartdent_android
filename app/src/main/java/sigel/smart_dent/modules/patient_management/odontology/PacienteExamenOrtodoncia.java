package sigel.smart_dent.modules.patient_management.odontology;

import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.Switch;

import sigel.smart_dent.R;
import sigel.smart_dent.modules.PatientsManagementFrameActivity;

/**
 * Created by pablin on 27/05/2016.
 */
public class PacienteExamenOrtodoncia extends PatientsManagementFrameActivity implements NumberPicker.OnValueChangeListener, View.OnClickListener{


    //static Dialog d;

    private Switch mTejidoBlando, mExamenDental;
    private RadioButton mTejidoPerfilConvexo, mTejidoPërfilRecto, mTejidoPërfilConcavo;
    private Spinner mSobremordidaHorizontal, mSobremordidaVertical;



    private LinearLayout mTejidoBlandoSection, mExamenDentalSection;


    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);

        //Onstanciando los campos
        //mTejidoBlando           = (Switch) findViewById(R.id.tejido_blando);
        //mTejidoBlandoSection    = (LinearLayout) findViewById(R.id.tejido_blando_section);
        //checkSwitchSection(mTejidoBlando,mTejidoBlandoSection);
        /*mTejidoBlando.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkSwitchSection(mTejidoBlando,mTejidoBlandoSection);
            }
        });

        mTejidoPerfilConvexo    = (RadioButton) findViewById(R.id.tejido_blando_perfil_convexo);
        mTejidoPërfilRecto      = (RadioButton) findViewById(R.id.tejido_blando_perfil_recto);
        mTejidoPërfilConcavo    = (RadioButton) findViewById(R.id.tejido_blando_perfil_concavo);

        mExamenDental           = (Switch) findViewById(R.id.examen_dental);
        mExamenDentalSection    = (LinearLayout) findViewById(R.id.examen_dental_section);
        checkSwitchSection(mExamenDental,mExamenDentalSection);
        mExamenDental.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkSwitchSection(mExamenDental,mExamenDentalSection);
            }
        });
        */
        mSobremordidaHorizontal = (Spinner) findViewById(R.id.ortodoncia_sobremordida_horizontal);
        mSobremordidaVertical = (Spinner) findViewById(R.id.ortodoncia_sobremordida_vertical);

        ArrayAdapter<CharSequence> SpinnerAdapter = ArrayAdapter.createFromResource(this,
                R.array.from0to20mm, R.layout.custom_spinner_item);
        SpinnerAdapter.setDropDownViewResource(R.layout.custom_spinner_dropdown_item);

        mSobremordidaHorizontal.setAdapter(SpinnerAdapter);
        mSobremordidaVertical.setAdapter(SpinnerAdapter);

        //sobre_mordida_ht.setTypeface(fontAvenirLight);


        /* BAIRES
        NumberPicker np_horizontal = (NumberPicker) findViewById(R.id.number_picker_sm_horizontal);
        np_horizontal.setMinValue(0);
        np_horizontal.setMaxValue(20);
        //np.setValue(myCurrentValue - minValue);
        np_horizontal.setWrapSelectorWheel(false);
        np_horizontal.setOnValueChangedListener(this);


        NumberPicker np_vertical = (NumberPicker) findViewById(R.id.number_picker_sm_vertical);
        np_vertical.setMinValue(0);
        np_vertical.setMaxValue(20);
        //np.setValue(myCurrentValue - minValue);
        np_vertical.setWrapSelectorWheel(false);
        np_vertical.setOnValueChangedListener(this);
        */ //BAIRES

        /*
        Button btn_sobre_mordida_horizontal = (Button) findViewById(R.id.sobre_mordida_horizontal);
        Button btn_sobre_mordida_vertical = (Button) findViewById(R.id.sobre_mordida_vertical);

        btn_sobre_mordida_horizontal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mostrarSeleccionador();
            }
        });

        btn_sobre_mordida_vertical.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mostrarSeleccionador();
            }
        });
        */


    }

    @Override
    public String setViewTitle() {
        return "Examen de Ortodoncia";
    }

    @Override
    protected int getActivityLayout() {
        return R.layout.activity_examen_ortodoncia;
    }


    @Override
    public void onClick(View v) {

    }




    @Override
    public void onValueChange(NumberPicker picker, int oldVal, int newVal) {

    }






    private void mostrarSeleccionador() {


        /* BAIRES
        final Dialog d = new Dialog(PacienteExamenOrtodoncia.this);

        d.setTitle("Milimetros");;
        d.setContentView(R.layout.dialog_number_picker);

        Button establecer = (Button) d.findViewById(R.id.number_picker_est);
        Button cancelar = (Button) d.findViewById(R.id.number_picker_can);

        //final int minValue = -20;
        final int maxValue = 20;

        NumberPicker np = (NumberPicker) d.findViewById(R.id.number_picker_sm);
        np.setMinValue(0);
        np.setMaxValue(maxValue);
        //np.setValue(myCurrentValue - minValue);
        np.setWrapSelectorWheel(false);
        np.setOnValueChangedListener(this);
        */  //BAIRES

        /*
        np.setFormatter(new NumberPicker.Formatter() {
            @Override
            public String format(int index) {
                return Integer.toString(index - minValue);
            }
        });
*/
        /* BAIRES
        establecer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        */ //BAIRES


/*
            @Override
            public void onClick(View v) {
                //tv.setText(String.valueOf(np.getValue())); //set the value to textview
                d.dismiss();
            }*/
        /* BAIRES
        });
        */ //BAIRES


        /* BAIRES
        establecer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                d.dismiss(); // dismiss the dialog
            }
        });


        d.show();
        */ //Baires


    }


}




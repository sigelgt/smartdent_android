package sigel.smart_dent.modules.patient_management.plaque.models;

/**
 * Created by Juan on 1/07/2016.
 */
public class ThootPlaque {

    int thootId;
    String thoot;
    boolean oclusal = false;
    boolean lingual = false;
    boolean bucal = false;
    boolean mesial = false;
    boolean distal = false;

    public int getId()                      {   return thootId; }
    public void setId(int id)               {   this.thootId = id; }

    public String getThoot()                {   return thoot; }
    public void setThoot(String name)       {   this.thoot = name; }

    public boolean getOclusal()             {   return oclusal; }
    public void setOclusal(boolean oclusal) {   this.oclusal = oclusal; }

    public boolean getLingual()             {   return lingual; }
    public void setLingual(boolean lingual) {	this.lingual = lingual;	}

    public boolean getBucal() 				{	return bucal;	}
    public void setBucal(boolean bucal) 	{	this.bucal = bucal;	}

    public boolean getMesial() 				{	return mesial;	}
    public void setMesial(boolean mesial) 	{	this.mesial = mesial;	}

    public boolean getDistal() 				{	return distal;	}
    public void setDistal(boolean distal)	{	this.distal = distal;	}
}

package sigel.smart_dent.modules.patient_management.medical_record;

import android.app.ProgressDialog;
import android.database.sqlite.SQLiteDatabase;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.GsonConverterFactory;
import retrofit2.Retrofit;
import sigel.smart_dent.R;
import sigel.smart_dent.beans.Response;
import sigel.smart_dent.sqlite.BD_SQLite;
import sigel.smart_dent.modules.PatientsManagementFrameActivity;
import sigel.smart_dent.modules.patient_management.medical_record.models.MedicalRecord;
import sigel.smart_dent.modules.patient_management.medical_record.models.PacienteHistoriaMedicaModel;
import sigel.smart_dent.rest.APIPlug;
import sigel.smart_dent.rest.ServiceGenerator;


/**
 * Created by pablin on 16/05/2016.
 */
public class PacienteHistoriaMedica extends PatientsManagementFrameActivity implements View.OnClickListener  {

    SharedPreferences preferences;

    private Switch alergia, alergiaAnestesia, alergiaAntibioticos, anemia, artritis, asma, cancer, cefalea,
            diabetes, embarazo, encefalitis, enfermedad_renal, epilepsia, fiebre_reumatica, hepatitis, problemas_endocrinos,
            trauma_craneo_facial, tabaquismo, tuberculosis, infectocontagiosas, otros, alergiaOtros;

    private TextView alergiaOtrosComent;
    private Switch ProblemasCardiacos;

    private Switch PresionAlta;
    private Switch PresionBaja;
    private Switch ProblemasCirculacion;
    private Switch Infarto;
    private Switch SoploCardiaco;
    private Switch ArteriasCoronarias;
    private Switch InsuficienciaCardiaca;

    private LinearLayout alergiaSection;
    private LinearLayout.LayoutParams params;
    private LinearLayout mProblemasCardiacosSection;


    private Button boton_paciente_historia_medica_agregar;


    public static String patientId = null;
    public static final String EXTRA_PACIENTE = "PATIENT_ID";

   @Override
    protected void onCreate(Bundle savedInstanceState) {
       super.onCreate(savedInstanceState);

       if(getIntent().hasExtra(EXTRA_PACIENTE)){
           patientId = getIntent().getExtras().getString(EXTRA_PACIENTE);
           Log.e("PATIENT_ID", patientId);

           //getPatientMedicalRecord(patientId);

       }else{
           Toast.makeText(getBaseContext(), "Por favor seleccione un paciente", Toast.LENGTH_LONG).show();
           //throw new IllegalArgumentException("La actividad debe recibir un ID de paciente");
       }

       /*Abrimos la base de datos 'DBPacientehistoriamedica' en modo escritura*/
       /*
       BD_SQLite db_smartdent = new BD_SQLite (this);

       SQLiteDatabase db = db_smartdent.getWritableDatabase();

       //Compruebo que la BD se abre correctamente
       if (db != null)
       {
           for(int i=1; i<=10; i++)
           {
               //Generar valores
               int codigo = i;
               String  enfermedad = "alergia" + i;

               //Datos de prueba
               db.execSQL("INSERT INTO HISTORIA_MEDICA (codigo,enfermedades) VALUES ("+ codigo +",'"+ enfermedad +"')");
           }

           //Cerrar BD
           db.close();
       }
    */
       alergia = (Switch) findViewById(R.id.alergia);
       alergiaAnestesia = (Switch) findViewById(R.id.alergia_anestesia);
       alergiaAntibioticos = (Switch) findViewById(R.id.alergia_antibioticos);
       alergiaOtros = (Switch) findViewById(R.id.alergia_otros);
       alergiaOtrosComent = (TextView) findViewById(R.id.others_alergic_comments_field);
       alergiaSection = (LinearLayout) findViewById(R.id.alergic_records_group_fields);

       checkSwitchSection(alergia,alergiaSection);


       alergia.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
           public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
               checkSwitchSection(alergia,alergiaSection);
           }
       });

       if(!alergiaOtros.isChecked()){
           alergiaOtrosComent.setVisibility(View.GONE);
       }else{
           alergiaOtrosComent.setVisibility(View.VISIBLE);
       }

       alergiaOtros.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
           public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
               if(!isChecked){
                   alergiaOtrosComent.setVisibility(View.GONE);
               }else{
                   //alergiaSection.sethei
                   alergiaOtrosComent.setVisibility(View.VISIBLE);

               }
           }
       });

       anemia = (Switch) findViewById(R.id.anemia);
       artritis = (Switch) findViewById(R.id.artritis);
       asma = (Switch) findViewById(R.id.asma);
       cancer = (Switch) findViewById(R.id.cancer);
       cefalea = (Switch) findViewById(R.id.cefalea);
       diabetes = (Switch) findViewById(R.id.diabetes);
       embarazo = (Switch) findViewById(R.id.embarazo);
       encefalitis = (Switch) findViewById(R.id.encefalitis);
       enfermedad_renal = (Switch) findViewById(R.id.enfermedad_renal);
       epilepsia = (Switch) findViewById(R.id.epilepsia);
       fiebre_reumatica = (Switch) findViewById(R.id.fiebre_reumatica);
       hepatitis = (Switch) findViewById(R.id.hepatitis);
       problemas_endocrinos = (Switch) findViewById(R.id.problemas_endocrinos);

       //Problemas Cardiacos
       ProblemasCardiacos = (Switch) findViewById(R.id.problemas_cardiacos);
       mProblemasCardiacosSection = (LinearLayout) findViewById(R.id.problemas_cardiacos_records_group_fields);
       PresionAlta = (Switch) findViewById(R.id.presion_alta);
       PresionBaja = (Switch) findViewById(R.id.presion_baja);
       ProblemasCirculacion = (Switch) findViewById(R.id.problemas_circualcion);
       Infarto = (Switch) findViewById(R.id.infarto);
       SoploCardiaco = (Switch) findViewById(R.id.soplo_cardiaco);
       ArteriasCoronarias = (Switch) findViewById(R.id.arterias_coronarias);
       InsuficienciaCardiaca = (Switch) findViewById(R.id.insuficiencia_cardiaca);
       alergiaOtrosComent = (TextView) findViewById(R.id.others_alergic_comments_field);

       checkSwitchSection(ProblemasCardiacos,mProblemasCardiacosSection);
       ProblemasCardiacos.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
           public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
               checkSwitchSection(ProblemasCardiacos,mProblemasCardiacosSection);
           }
       });

       trauma_craneo_facial = (Switch) findViewById(R.id.trauma_craneo_facial);
       tabaquismo = (Switch) findViewById(R.id.tabaquismo);
       tuberculosis = (Switch) findViewById(R.id.tuberculosis);
       infectocontagiosas = (Switch) findViewById(R.id.infectocontagiosas);
       otros = (Switch) findViewById(R.id.otros);

       boton_paciente_historia_medica_agregar = (Button) findViewById(R.id.boton_paciente_historia_medica_agregar);
       boton_paciente_historia_medica_agregar.setOnClickListener(this);

    }

    private void getPatientMedicalRecord(String patientId) {

        APIPlug cliente = ServiceGenerator.createService(APIPlug.class);
        Call<PacienteHistoriaMedicaModel> call = cliente.getPatientMedicalRecord(patientId);
        call.enqueue(new Callback<PacienteHistoriaMedicaModel>() {
            @Override
            public void onResponse(Call<PacienteHistoriaMedicaModel> call, retrofit2.Response<PacienteHistoriaMedicaModel> response) {
                Boolean alergia_r = response.body().getAlergia();
                Boolean alergiaAnestesia_r = response.body().getAlergiaAnestesia();
                Boolean alergiaAntibioticos_r = response.body().getAlergiaAntibioticos();
                Boolean anemia_r = response.body().getAnemia();
                Boolean artritis_r = response.body().getArtritis();
                Boolean asma_r = response.body().getAsma();
                Boolean cancer_r = response.body().getCancer();
                Boolean cefalea_r = response.body().getCefalea();
                Boolean diabetes_r = response.body().getDiabetes();
                Boolean embarazo_r = response.body().getEmbarazo();
                Boolean encefalitis_r = response.body().getEncefalitis();
                Boolean enfermedad_renal_r = response.body().getEnfermedad_renal();
                Boolean epilepsia_r = response.body().getEpilepsia();
                Boolean fiebre_reumatica_r = response.body().getFiebre_reumatica();
                Boolean hepatitis_r = response.body().getHepatitis();
                Boolean problemas_endocrinos_r = response.body().getProblemas_endocrinos();
                Boolean trauma_craneo_facial_r = response.body().getTrauma_craneo_facial();
                Boolean tabaquismo_r = response.body().getTabaquismo();
                Boolean tuberculosis_r = response.body().getTuberculosis();
                Boolean infectocontagiosas_r = response.body().getInfectocontagiosas();
                Boolean alergiaOtros_r = response.body().getAlergiaOtros();
                Boolean PresionAlta_r = response.body().getPresionalta();
                Boolean PresionBaja_r = response.body().getPresionbaja();
                Boolean ProblemasCirculacion_r = response.body().getProblemasCirculacion();
                Boolean Infarto_r = response.body().getInfarto();
                Boolean SoploCardiaco_r = response.body().getSoploCardiaco();
                //Boolena get_alergiaOtrosComent_r = response.body().getAlergiaOtrosComent()
                //TextView alergiaOtrosComent_r = response.body().getAlergiaOtrosComent();

                //Problemas Cardiacos
                Boolean ProblemasCardiacos_r = response.body().getProblemasCardiacos();

                //Log.e("ALERGIA", String.valueOf(alergia_r));

                //Log.e("ANEMIA", String.valueOf(anemia_r));
                if(alergia_r){alergia.setChecked(true);}
                if(alergiaAnestesia_r){alergiaAnestesia.setChecked(true);}
                if(alergiaAntibioticos_r){alergiaAntibioticos.setChecked(true);}
                if(alergiaOtros_r){alergiaOtros.setChecked(true);}
                if(anemia_r){anemia.setChecked(true);}
                if(artritis_r){artritis.setChecked(true);}
                if(asma_r){asma.setChecked(true);}
                if(cancer_r){cancer.setChecked(true);}
                if(cefalea_r){cefalea.setChecked(true);}
                if(diabetes_r){diabetes.setChecked(true);}
                if(embarazo_r){embarazo.setChecked(true);}
                if(encefalitis_r){encefalitis.setChecked(true);}
                if(enfermedad_renal_r){enfermedad_renal.setChecked(true);}
                if(epilepsia_r){epilepsia.setChecked(true);}
                if(fiebre_reumatica_r){fiebre_reumatica.setChecked(true);}
                if(hepatitis_r){hepatitis.setChecked(true);}
                if(problemas_endocrinos_r){problemas_endocrinos.setChecked(true);}
                if(trauma_craneo_facial_r){trauma_craneo_facial.setChecked(true);}
                if(tabaquismo_r){tabaquismo.setChecked(true);}
                if(tuberculosis_r){tuberculosis.setChecked(true);}
                if(infectocontagiosas_r){infectocontagiosas.setChecked(true);}
                if(alergiaOtros_r){alergiaOtros.setChecked(true);}
                //    alergiaOtrosComent.setVisibility(View.GONE);
               // if(alergiaOtrosComent_r){alergiaOtrosComent.getText();}
                if(ProblemasCardiacos_r){ProblemasCardiacos.setChecked(true);}
                if(PresionAlta_r){PresionAlta.setChecked(true);}
                if(PresionBaja_r){PresionBaja.setChecked(true);}
                if(ProblemasCirculacion_r){ProblemasCirculacion.setChecked(true);}
                if(Infarto_r){Infarto.setChecked(true);}
                if(SoploCardiaco_r){SoploCardiaco.setChecked(true);}

            }

            @Override
            public void onFailure(Call<PacienteHistoriaMedicaModel> call, Throwable t) {

            }
        });

    }

    @Override
    public String setViewTitle() {
        return "Historia Medica";
    }

    @Override
    protected int getActivityLayout() {
        return R.layout.activity_paciente_historia_medica;
    }

    @Override
    public void onClick(View v) {
        pacienteAgregarHistoriaMedica();
    }



    private void pacienteAgregarHistoriaMedica() {

        MedicalRecord medicalRecord = new MedicalRecord();
        medicalRecord.alergia.alergia = alergia.isChecked();

        Boolean get_alergia = alergia.isChecked();
        Boolean get_alergiaAnestesia = false;
        Boolean get_alergiaAntibioticos = false;
        Boolean get_alergiaOtros = false;
        String getAlergiaComentarios = null;

        if(get_alergia){
            get_alergiaAnestesia = alergiaAnestesia.isChecked();
            get_alergiaAntibioticos = alergiaAntibioticos.isChecked();
            get_alergiaOtros = alergiaOtros.isChecked();

            if(get_alergiaOtros) {
                getAlergiaComentarios = (String) alergiaOtrosComent.getText();
            }

        }

        Boolean get_anemia = anemia.isChecked();
        Boolean get_artritis = artritis.isChecked();
        Boolean get_asma = asma.isChecked();
        Boolean get_cancer = cancer.isChecked();
        Boolean get_cefalea = cefalea.isChecked();
        Boolean get_diabetes = diabetes.isChecked();
        Boolean get_embarazo = embarazo.isChecked();
        Boolean get_encefalitis = encefalitis.isChecked();
        Boolean get_enfermedad_renal = enfermedad_renal.isChecked();
        Boolean get_epilepsia = epilepsia.isChecked();
        Boolean get_fiebre_reumatica = fiebre_reumatica.isChecked();
        Boolean get_hepatitis = hepatitis.isChecked();
        Boolean get_problemas_endocrinos = problemas_endocrinos.isChecked();
        Boolean get_trauma_craneo_facial = trauma_craneo_facial.isChecked();
        Boolean get_tabaquismo = tabaquismo.isChecked();
        Boolean get_tuberculosis = tuberculosis.isChecked();
        Boolean get_infectocontagiosas = infectocontagiosas.isChecked();
        Boolean get_otros = otros.isChecked();
        Boolean get_ProblemasCardiacos = ProblemasCardiacos.isChecked();
        //Boolean get_alergiaOtros = alergiaOtros.isChecked();
       // String get_alergiaOtrosComent = String.valueOf(alergiaOtrosComent.getText());
        Boolean get_Presionalta = PresionAlta.isChecked();
        Boolean get_Presionbaja = PresionBaja.isChecked();
        Boolean get_SoploCardiaco = SoploCardiaco.isChecked();
        Boolean get_ProblemasCirculacion = ProblemasCirculacion.isChecked();
        Boolean get_Infarto = Infarto.isChecked();

        agregarPacienteHistoriaMedica(get_alergia, get_alergiaAnestesia,get_alergiaAntibioticos, get_alergiaOtros,get_anemia, get_artritis, get_asma, get_cancer, get_cefalea, get_diabetes, get_embarazo, get_encefalitis,
                get_enfermedad_renal, get_epilepsia, get_fiebre_reumatica, get_hepatitis, get_problemas_endocrinos, get_trauma_craneo_facial,/*get_alergiaOtrosComent,*/get_tabaquismo,get_tuberculosis, get_infectocontagiosas, get_otros, get_ProblemasCardiacos,
                get_Presionalta,get_Presionbaja,get_SoploCardiaco,get_ProblemasCirculacion,get_Infarto);

    }

    private void agregarPacienteHistoriaMedica(Boolean get_alergia,Boolean get_alergiaAnestesia,Boolean get_alergiaAntibioticos,Boolean get_alergiaOtros,Boolean get_anemia,Boolean get_artritis,Boolean get_asma,Boolean get_cancer,
                                               Boolean get_cefalea,Boolean get_diabetes,Boolean get_embarazo,Boolean get_encefalitis, Boolean get_enfermedad_renal,Boolean get_epilepsia,Boolean get_fiebre_reumatica,Boolean get_hepatitis,
                                               Boolean get_problemas_endocrinos,Boolean get_trauma_craneo_facial,/*String get_alergiaOtrosComent,*/Boolean get_Presionalta,Boolean get_Presionbaja,Boolean get_SoploCardiaco,Boolean get_ProblemasCirculacion,
                                               Boolean get_Infarto,Boolean get_tabaquismo,Boolean get_tuberculosis,Boolean get_infectocontagiosas,Boolean get_otros,Boolean get_ProblemasCardiacos) {

        final ProgressDialog loading = ProgressDialog.show(this, "Grabando", "Por favor aguarde mientras finaliza el proceso", false, false);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://52.88.126.124:3000/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        APIPlug apiPlug = retrofit.create(APIPlug.class);
        //Agregar paciente historia medica
        Call<Response> call = apiPlug.agregarPacienteHistoriaMedica(patientId,get_alergia, get_alergiaAnestesia,get_alergiaAntibioticos, get_alergiaOtros, get_otros, get_anemia, get_artritis, get_asma, get_cancer, get_cefalea, get_diabetes,
                get_embarazo, get_encefalitis, get_enfermedad_renal, get_epilepsia, get_fiebre_reumatica, get_hepatitis, get_problemas_endocrinos, get_trauma_craneo_facial,get_tabaquismo,get_tuberculosis, get_infectocontagiosas,/*get_alergiaOtrosComent,*/get_Presionalta,
                get_Presionbaja,get_SoploCardiaco,get_Infarto,get_ProblemasCirculacion,get_ProblemasCardiacos);

        call.enqueue(new Callback<Response>() {
            @Override
            public void onResponse(Call<sigel.smart_dent.beans.Response> call, retrofit2.Response<Response> response) {

                //Log.e("onResponse", response.body().getInsertedCount().toString());
                loading.dismiss();
                Toast.makeText(getBaseContext(), "Historia Médica creada correctamente!", Toast.LENGTH_SHORT).show();
                finish();
                startActivity(getIntent());
            }

            @Override
            public void onFailure(Call<sigel.smart_dent.beans.Response> call, Throwable t) {

                //Log.e("onFailure", t.toString());
                loading.dismiss();
                Toast.makeText(getBaseContext(), "Por favor intente de nuevo", Toast.LENGTH_SHORT).show();
                finish();
                startActivity(getIntent());
            }
        });

    }


}

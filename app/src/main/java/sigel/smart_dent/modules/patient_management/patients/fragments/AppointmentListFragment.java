package sigel.smart_dent.modules.patient_management.patients.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import sigel.smart_dent.R;
import sigel.smart_dent.modules.patient_management.patients.adapters.AppointmentListAdapter;

/**
 * Created by Juan on 19/06/2016.
 */
public class AppointmentListFragment extends Fragment {

    public interface AppointmentInterface {
        void onAppointmentItemSelected(int index);
        int getAppointmentId(int index);
        String getAppointmentPatientName(int index);
        String getAppointmentPlace(int index);
        String getAppointmentTime(int index);
        String getAppointmentProcedure(int index);
        int getAppointmentsListCount();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        //patienListRecyclerView
        View view = inflater.inflate(R.layout.fragment_recycleview,container,false);
        AppointmentInterface listener = (AppointmentInterface) getActivity();
        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.recyclerView);
        AppointmentListAdapter recyclerAdapter = new AppointmentListAdapter(listener);
        recyclerView.setAdapter(recyclerAdapter);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        return view;

    }
}

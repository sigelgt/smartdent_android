package sigel.smart_dent.modules.patient_management.endodontics.fragments;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TableLayout;
import android.widget.TextView;

import java.util.ArrayList;

import sigel.smart_dent.R;
import sigel.smart_dent.modules.patient_management.odontogram.models.Theeth;

/**
 * Created by Juan on 24/06/2016.
 */

public class EndodonthicsViewPager extends Fragment {
    public Theeth mTheeth;
    private final ArrayList<Fragment> mEndodonthicsForms = new ArrayList<Fragment>();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_endodonthics_view_pager,container,false);


        mTheeth = new Theeth();
        //mEndodonthicsForms = new ArrayList<Fragment>();
        for(int i = 1; i < 5; i++){
            this.setCuadranteTheet(Theeth.getCuadranteNames(i,"bucal"), Theeth.getCuadranteResourceIds(i,"bucal"));
        }

        //ViewPager viewPager = (ViewPager) view.findViewById(R.id.viewOdontogramPager);
        ViewPager viewPager = (ViewPager) view.findViewById(R.id.viewEndodonthicsPager);
        FragmentPagerAdapter pagerAdapter = getViewPagerFragmentAdapter();
        viewPager.setAdapter(pagerAdapter);
        TabLayout tabLayout = (TabLayout) view.findViewById(R.id.endodonthicsTabLayout);
        tabLayout.setupWithViewPager(viewPager);

        TableLayout.LayoutParams lp;
        //lp = (TableLayout.LayoutParams) tabLayout.getLayoutParams();
        //lp.height = 160;
        //tabLayout.setLayoutParams(lp);



        //tabLayout.setMinimumHeight(512);


        for(int c = 0; c < mEndodonthicsForms.size(); c++){
            FrameLayout customTabView = (FrameLayout) inflater.inflate(R.layout.fragment_tab_endodonthics, null,false);
            String tn = ((EndodonthicsFormFragment) mEndodonthicsForms.get(c)).getThootTitle();
            int ti = ((EndodonthicsFormFragment) mEndodonthicsForms.get(c)).getTheetImageId();
            ((TextView) customTabView.findViewById(R.id.text1)).setText(tn);
            ((ImageView) customTabView.findViewById(R.id.icon)).setImageDrawable(getDrawableTooth(ti));
            //tabLayout.getTabAt(c).setText(tn);
            tabLayout.getTabAt(c).setCustomView(customTabView);
            //tabLayout.getTabAt(c).setIcon(getDrawableTooth(ti));

        }

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                //Log.d("FRAGMENTNINJA", "PAGE_SCROLLED : " + ((EndodonthicsFormFragment) mEndodonthicsForms.get(position)).getThootTitle());
            }

            @Override
            public void onPageSelected(int position) {
                //Log.d("FRAGMENTNINJA", "SELECTED : " + ((EndodonthicsFormFragment) mEndodonthicsForms.get(position)).getThootTitle());
            }

            @Override
            public void onPageScrollStateChanged(int state) {
                //Log.d("FRAGMENTNINJA", "PAGE_SCROLLSTATE_CHANGES : " + ((EndodonthicsFormFragment) mEndodonthicsForms.get(position)).getThootTitle());
            }
        });


        //tabLayout.getTabAt(0).setIcon(getDrawableTooth(R.drawable.bucal_1_1));

        return view;
    }

    private void setCuadranteTheet(String[] cudarante, int[] resources){
        int flag = 0;
        for(String thoot : cudarante){
            EndodonthicsFormFragment thootFromframent = new EndodonthicsFormFragment();
            thootFromframent.setInitParams(thoot, resources[flag]);
            mEndodonthicsForms.add(thootFromframent);
            flag++;
        }

    }

    private FragmentPagerAdapter getViewPagerFragmentAdapter (){

        return new FragmentPagerAdapter(getChildFragmentManager()) {
            @Override
            public Fragment getItem(int position) {
                return (EndodonthicsFormFragment) mEndodonthicsForms.get(position);
            }

            @Override
            public int getCount() {
                return mEndodonthicsForms.size();
            }

            @Override
            public CharSequence getPageTitle(int position) {
                return getResources().getString(R.string.title_activity_endodonthis) + ": " + ((EndodonthicsFormFragment) mEndodonthicsForms.get(position)).getThootTitle();
                //return null;
            }

        };
    }

    private Drawable getDrawableTooth(int resourceId){
        return ContextCompat.getDrawable(getContext(), resourceId);

    }

}

package sigel.smart_dent.modules.patient_management.plaque.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import sigel.smart_dent.R;
import sigel.smart_dent.modules.patient_management.odontogram.models.Theeth;
import sigel.smart_dent.modules.patient_management.plaque.adapters.PlaqueAdapter;
import sigel.smart_dent.modules.patient_management.plaque.models.ThootPlaque;

/**
 * Created by Juan on 1/07/2016.
 */
public class PlaqueCuadrantFragment extends Fragment {

    int fragmentCuadrantNumber = 0;

    public interface plaqueCuadrantInterface {
        void onCuadrantListItemSelect (int index, View v);
        //FragmentManager getActivityFragmentManager ();
    }

    private String cuadranteTitle;
    private String[] thootList;

    public PlaqueCuadrantFragment(){}

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        plaqueCuadrantInterface listener = (plaqueCuadrantInterface) getActivity();
        super.onCreate(savedInstanceState);
        View view = inflater.inflate(R.layout.fragment_generic_recyclerview,container,false);
        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.genericRecyclerView);

        PlaqueAdapter plaqueAdapter = new PlaqueAdapter(getContext());
        plaqueAdapter.setCuadrant(getCuadrantNumber());

        int id = getArguments().getInt("id", 0);

        Log.e("GET C# to SET C# ", String.valueOf(getCuadrantNumber()));
        Log.e("GET ARGUMENTS ", String.valueOf(id));

        ArrayList<ThootPlaque> thootList = new ArrayList<ThootPlaque>();
        if(getCuadrantNumber()==1){
            for(int c = 0; c < Theeth.cudarante1b.length; c++){
                Log.e("C1", Theeth.cudarante1b[c]);

                ThootPlaque tp = new ThootPlaque();
                tp.setThoot(Theeth.cudarante1b[c]);
                thootList.add(tp);
            }

            for(int c = 0; c < Theeth.cudarante5b.length; c++){
                Log.e("C5", Theeth.cudarante5b[c]);

                ThootPlaque tp = new ThootPlaque();
                tp.setThoot(Theeth.cudarante5b[c]);
                thootList.add(tp);
            }
        }else if(getCuadrantNumber() == 2){

            for(int c = 0; c < Theeth.cudarante2b.length; c++){
                Log.e("C2", Theeth.cudarante2b[c]);

                ThootPlaque tp = new ThootPlaque();
                tp.setThoot(Theeth.cudarante2b[c]);
                thootList.add(tp);
            }

            for(int c = 0; c < Theeth.cudarante6b.length; c++){
                Log.e("C6", Theeth.cudarante6b[c]);

                ThootPlaque tp = new ThootPlaque();
                tp.setThoot(Theeth.cudarante6b[c]);
                thootList.add(tp);
            }

        }else if(getCuadrantNumber()==3){
            for(int c = 0; c < Theeth.cudarante3b.length; c++){
                Log.e("C3", Theeth.cudarante3b[c]);

                ThootPlaque tp = new ThootPlaque();
                tp.setThoot(Theeth.cudarante3b[c]);
                thootList.add(tp);
            }

            for(int c = 0; c < Theeth.cudarante7b.length; c++){
                Log.e("C7", Theeth.cudarante7b[c]);

                ThootPlaque tp = new ThootPlaque();
                tp.setThoot(Theeth.cudarante7b[c]);
                thootList.add(tp);
            }
        }else{
            for(int c = 0; c < Theeth.cudarante4b.length; c++){
                Log.e("C4", Theeth.cudarante4b[c]);

                ThootPlaque tp = new ThootPlaque();
                tp.setThoot(Theeth.cudarante4b[c]);
                thootList.add(tp);
            }

            for(int c = 0; c < Theeth.cudarante8b.length; c++){
                Log.e("C8", Theeth.cudarante8b[c]);

                ThootPlaque tp = new ThootPlaque();
                tp.setThoot(Theeth.cudarante8b[c]);
                thootList.add(tp);
            }
        }

        plaqueAdapter.setItems(thootList);
        recyclerView.setAdapter(plaqueAdapter);
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(getActivity(),8);

        recyclerView.setLayoutManager(layoutManager);
        return view;
    }



    public void setCuadrantTitle(String title){
        cuadranteTitle = title;
    }
    public String getCuadrantTitle(){
        return cuadranteTitle;
    }

    public void setCuadrantNumber(int number){
        fragmentCuadrantNumber = number;
        Log.e("CNumber", String.valueOf(number));
    }

    public int getCuadrantNumber (){
        return this.fragmentCuadrantNumber;
    }

}

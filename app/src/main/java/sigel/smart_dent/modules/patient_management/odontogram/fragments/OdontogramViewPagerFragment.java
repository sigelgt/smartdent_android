package sigel.smart_dent.modules.patient_management.odontogram.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import sigel.smart_dent.R;

/**
 * Created by Juan on 16/06/2016.
 */
public class OdontogramViewPagerFragment extends Fragment {

    public static final String KEY_PATIENT_ID = "KEY_PATIENT_ID";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_odontogram_view_pager,container,false);
        //  int index = getArguments().getInt(OdontogramViewPagerFragment.KEY_PATIENT_ID);

        getActivity().setTitle("Odontograma");

        final Cuadrante1Fragment cuadrante1Fragment = new Cuadrante1Fragment();
        //Bundle ingredientsBundle = new Bundle();
        //ingredientsBundle.putInt(KEY_RECIPE_INDEX,index);
        //ingredientsFragment.setArguments(ingredientsBundle);

        final Cuadrante2Fragment cuadrante2Fragment = new Cuadrante2Fragment();
        final Cuadrante3Fragment cuadrante3Fragment = new Cuadrante3Fragment();
        final Cuadrante4Fragment cuadrante4Fragment = new Cuadrante4Fragment();

        ViewPager viewPager = (ViewPager) view.findViewById(R.id.viewOdontogramPager);
        viewPager.setAdapter(
        new FragmentPagerAdapter(getChildFragmentManager()) {
            @Override
            public Fragment getItem(int position) {
                switch (position){
                    case 0:     return cuadrante1Fragment;
                    case 1:     return cuadrante2Fragment;
                    case 2:     return cuadrante3Fragment;
                    case 3:     return cuadrante4Fragment;
                    default:    return cuadrante1Fragment;
                }

            }

            @Override
            public int getCount() {
                return 4;
            }

            @Override
            public CharSequence getPageTitle(int position) {
                switch (position){
                    case 0:     return getResources().getString(R.string.cuadrant1); //"Cuadrante 1";
                    case 1:     return getResources().getString(R.string.cuadrant2);
                    case 2:     return getResources().getString(R.string.cuadrant3);
                    case 3:     return getResources().getString(R.string.cuadrant4);
                    default:    return getResources().getString(R.string.cuadrant1);
                }
            }
        });


        TabLayout tabLayout = (TabLayout) view.findViewById(R.id.tabLayout);
        tabLayout.setupWithViewPager(viewPager);
        return view;
    }

    @Override
    public void onStop() {
        super.onStop();
        getActivity().setTitle(getResources().getString(R.string.odontogram_seccion_title));
    }

}

package sigel.smart_dent.modules.patient_management.atm;

import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.Toast;

import retrofit2.Call;
import retrofit2.Callback;
import sigel.smart_dent.R;
import sigel.smart_dent.beans.Response;
import sigel.smart_dent.modules.PatientsManagementFrameActivity;
import sigel.smart_dent.modules.patient_management.atm.models.PatientATMModel;
import sigel.smart_dent.rest.APIPlug;
import sigel.smart_dent.rest.ServiceGenerator;

/**
 * Created by pablin on 25/05/2016.
 */
public class PacienteATM extends PatientsManagementFrameActivity implements View.OnClickListener {

    private Switch  htf, bruxismo, tratamiento_ortodoncia, ruido_articular, artritis,
                    chasquido_izquierdo_ninguno, chasquido_izquierdo_apertura, chasquido_izquierdo_cierre,
                    chasquido_derecho_ninguno, chasquido_derecho_apertura, chasquido_derecho_cierre,
                    crepitacion_izquierdo_ninguno, crepitacion_izquierdo_articular, crepitacion_izquierdo_muscular,
                    crepitacion_derecho_ninguno, crepitacion_derecho_articular, crepitacion_derecho_muscular,
                    dolor_izquierdo_ninguno, dolor_izquierdo_articular, dolor_izquierdo_muscular,
                    dolor_derecho_ninguno, dolor_derecho_articular, dolor_derecho_muscular,
                    apertura_recto, apertura_ala_izquierda, apertura_ala_derecha,
                    cerrado_recto, cerrado_ala_izquierda, cerrado_ala_derecha;

    private Button boton_paciente_examen_atm_agregar;

    public static String patientId = null;
    public static final String EXTRA_PACIENTE = "PATIENT_ID";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(getIntent().hasExtra(EXTRA_PACIENTE)){
            patientId = getIntent().getExtras().getString(EXTRA_PACIENTE);
            Log.e("PATIENT_ID", patientId);

            getPatientATM(patientId);

        }else{
            Toast.makeText(getBaseContext(), "Por favor seleccione un paciente", Toast.LENGTH_LONG).show();
        }

        htf = (Switch) findViewById(R.id.htf);
        bruxismo = (Switch) findViewById(R.id.bruxismo);
        tratamiento_ortodoncia = (Switch) findViewById(R.id.tratamiento_ortodoncia);
        ruido_articular = (Switch) findViewById(R.id.ruido_articular);
        artritis = (Switch) findViewById(R.id.artritis);


        chasquido_izquierdo_ninguno = (Switch) findViewById(R.id.chasquido_izquierdo_ninguno);
        chasquido_izquierdo_apertura = (Switch) findViewById(R.id.chasquido_izquierdo_apertura);
        chasquido_izquierdo_cierre = (Switch) findViewById(R.id.chasquido_izquierdo_cierre);

        chasquido_derecho_ninguno = (Switch) findViewById(R.id.chasquido_derecho_ninguno);
        chasquido_derecho_apertura = (Switch) findViewById(R.id.chasquido_derecho_apertura);
        chasquido_derecho_cierre = (Switch) findViewById(R.id.chasquido_derecho_cierre);

        crepitacion_izquierdo_ninguno = (Switch) findViewById(R.id.crepitacion_izquierdo_ninguno);
        crepitacion_izquierdo_articular = (Switch) findViewById(R.id.crepitacion_izquierdo_articular);
        crepitacion_izquierdo_muscular = (Switch) findViewById(R.id.crepitacion_izquierdo_muscular);

        crepitacion_derecho_ninguno = (Switch) findViewById(R.id.crepitacion_derecho_ninguno);
        crepitacion_derecho_articular = (Switch) findViewById(R.id.crepitacion_derecho_articular);
        crepitacion_derecho_muscular = (Switch) findViewById(R.id.crepitacion_derecho_muscular);

        dolor_izquierdo_ninguno = (Switch) findViewById(R.id.dolor_izquierdo_ninguno);
        dolor_izquierdo_articular = (Switch) findViewById(R.id.dolor_izquierdo_articular);
        dolor_izquierdo_muscular = (Switch) findViewById(R.id.dolor_izquierdo_muscular);

        dolor_derecho_ninguno = (Switch) findViewById(R.id.dolor_derecho_ninguno);
        dolor_derecho_articular = (Switch) findViewById(R.id.dolor_derecho_articular);
        dolor_derecho_muscular = (Switch) findViewById(R.id.dolor_derecho_muscular);

        apertura_recto = (Switch) findViewById(R.id.apertura_recto);
        apertura_ala_izquierda = (Switch) findViewById(R.id.apertura_ala_izquierda);
        apertura_ala_derecha = (Switch) findViewById(R.id.apertura_ala_derecha);

        cerrado_recto = (Switch) findViewById(R.id.cierre_recto);
        cerrado_ala_izquierda = (Switch) findViewById(R.id.cierre_ala_izquierda);
        cerrado_ala_derecha = (Switch) findViewById(R.id.cierre_ala_derecha);

        boton_paciente_examen_atm_agregar = (Button) findViewById(R.id.boton_paciente_atm_agregar);

        boton_paciente_examen_atm_agregar.setOnClickListener(this);

        chasquido_izquierdo_ninguno.setChecked(true);
        chasquido_derecho_ninguno.setChecked(true);
        crepitacion_izquierdo_ninguno.setChecked(true);
        crepitacion_derecho_ninguno.setChecked(true);
        dolor_izquierdo_ninguno.setChecked(true);
        dolor_derecho_ninguno.setChecked(true);
        apertura_recto.setChecked(true);
        cerrado_recto.setChecked(true);

        chasquido_izquierdo_apertura.setClickable(false);
        chasquido_izquierdo_cierre.setClickable(false);

        chasquido_derecho_apertura.setClickable(false);
        chasquido_derecho_cierre.setClickable(false);

        crepitacion_izquierdo_articular.setClickable(false);
        crepitacion_izquierdo_muscular.setClickable(false);

        crepitacion_derecho_articular.setClickable(false);
        crepitacion_derecho_muscular.setClickable(false);

        dolor_izquierdo_articular.setClickable(false);
        dolor_izquierdo_muscular.setClickable(false);

        dolor_derecho_articular.setClickable(false);
        dolor_derecho_muscular.setClickable(false);

        apertura_ala_izquierda.setClickable(false);
        apertura_ala_derecha.setClickable(false);

        cerrado_ala_izquierda.setClickable(false);
        cerrado_ala_derecha.setClickable(false);


        chasquido_izquierdo_ninguno.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if(!isChecked){
                    chasquido_izquierdo_apertura.setClickable(true);
                    chasquido_izquierdo_cierre.setClickable(true);
                    chasquido_izquierdo_apertura.setChecked(true);
                    chasquido_izquierdo_cierre.setChecked(true);
                }else{
                    chasquido_izquierdo_apertura.setClickable(false);
                    chasquido_izquierdo_cierre.setClickable(false);
                    chasquido_izquierdo_apertura.setChecked(false);
                    chasquido_izquierdo_cierre.setChecked(false);
                }
            }
        });

        chasquido_derecho_ninguno.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if(!isChecked){
                    chasquido_derecho_apertura.setClickable(true);
                    chasquido_derecho_cierre.setClickable(true);
                    chasquido_derecho_apertura.setChecked(true);
                    chasquido_derecho_cierre.setChecked(true);
                }else{
                    chasquido_derecho_apertura.setClickable(false);
                    chasquido_derecho_cierre.setClickable(false);
                    chasquido_derecho_apertura.setChecked(false);
                    chasquido_derecho_cierre.setChecked(false);
                }
            }
        });

        //CREPITACION
        crepitacion_izquierdo_ninguno.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if(!isChecked){
                    crepitacion_izquierdo_articular.setClickable(true);
                    crepitacion_izquierdo_muscular.setClickable(true);
                    crepitacion_izquierdo_articular.setChecked(true);
                    crepitacion_izquierdo_muscular.setChecked(true);
                }else{
                    crepitacion_izquierdo_articular.setClickable(false);
                    crepitacion_izquierdo_muscular.setClickable(false);
                    crepitacion_izquierdo_articular.setChecked(false);
                    crepitacion_izquierdo_muscular.setChecked(false);
                }
            }
        });


        crepitacion_derecho_ninguno.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if(!isChecked){
                    crepitacion_derecho_articular.setClickable(true);
                    crepitacion_derecho_muscular.setClickable(true);
                    crepitacion_derecho_articular.setChecked(true);
                    crepitacion_derecho_muscular.setChecked(true);
                }else{
                    crepitacion_derecho_articular.setClickable(false);
                    crepitacion_derecho_muscular.setClickable(false);
                    crepitacion_derecho_articular.setChecked(false);
                    crepitacion_derecho_muscular.setChecked(false);
                }
            }
        });



        //DOLOR
        dolor_izquierdo_ninguno.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if(!isChecked){
                    dolor_izquierdo_articular.setClickable(true);
                    dolor_izquierdo_muscular.setClickable(true);
                    dolor_izquierdo_articular.setChecked(true);
                    dolor_izquierdo_muscular.setChecked(true);
                }else{
                    dolor_izquierdo_articular.setClickable(false);
                    dolor_izquierdo_muscular.setClickable(false);
                    dolor_izquierdo_articular.setChecked(false);
                    dolor_izquierdo_muscular.setChecked(false);
                }
            }
        });


        dolor_derecho_ninguno.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if(!isChecked){
                    dolor_derecho_articular.setClickable(true);
                    dolor_derecho_muscular.setClickable(true);
                    dolor_derecho_articular.setChecked(true);
                    dolor_derecho_muscular.setChecked(true);
                }else{
                    dolor_derecho_articular.setClickable(false);
                    dolor_derecho_muscular.setClickable(false);
                    dolor_derecho_articular.setChecked(false);
                    dolor_derecho_muscular.setChecked(false);
                }
            }
        });


        //MOVIMIENTO
        //APERTURA
        apertura_recto.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if(!isChecked){
                    apertura_ala_izquierda.setClickable(true);
                    apertura_ala_derecha.setClickable(true);
                    apertura_ala_izquierda.setChecked(true);
                    apertura_ala_derecha.setChecked(true);
                }else{
                    apertura_ala_izquierda.setClickable(false);
                    apertura_ala_derecha.setClickable(false);
                    apertura_ala_izquierda.setChecked(false);
                    apertura_ala_derecha.setChecked(false);
                }
            }
        });


        cerrado_recto.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if(!isChecked){
                    cerrado_ala_izquierda.setClickable(true);
                    cerrado_ala_derecha.setClickable(true);
                    cerrado_ala_izquierda.setChecked(true);
                    cerrado_ala_derecha.setChecked(true);
                }else{
                    cerrado_ala_izquierda.setClickable(false);
                    cerrado_ala_derecha.setClickable(false);
                    cerrado_ala_izquierda.setChecked(false);
                    cerrado_ala_derecha.setChecked(false);
                }
            }
        });

        /*
        atm_chasquido_izquierdo = (Spinner) findViewById(R.id.atm_chasquido_izquierdo);
        atm_chasquido_derecho = (Spinner) findViewById(R.id.atm_chasquido_derecho);

        atm_crepitacion_izquierdo = (Spinner) findViewById(R.id.atm_crepitacion_izquierdo);
        atm_crepitacion_derecho = (Spinner) findViewById(R.id.atm_crepitacion_derecho);

        atm_dolor_izquierdo = (Spinner) findViewById(R.id.atm_dolor_izquierdo);
        atm_dolor_derecho = (Spinner) findViewById(R.id.atm_dolor_derecho);

        atm_apertura = (Spinner) findViewById(R.id.atm_apertura);
        atm_cerrado = (Spinner) findViewById(R.id.atm_cerrado);


        //SPINNERS
        ArrayAdapter<CharSequence> adapter_izquierdo_derecho = ArrayAdapter.createFromResource(this, R.array.atm_chasquido_array, android.R.layout.simple_spinner_item);
        adapter_izquierdo_derecho.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        atm_chasquido_izquierdo.setAdapter(adapter_izquierdo_derecho);
        atm_chasquido_derecho.setAdapter(adapter_izquierdo_derecho);

        ArrayAdapter<CharSequence> adapter_crepitacion_dolor = ArrayAdapter.createFromResource(this, R.array.atm_crepitacion_dolor_array, android.R.layout.simple_spinner_item);
        adapter_crepitacion_dolor.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        atm_crepitacion_izquierdo.setAdapter(adapter_crepitacion_dolor);
        atm_crepitacion_derecho.setAdapter(adapter_crepitacion_dolor);

        atm_dolor_izquierdo.setAdapter(adapter_crepitacion_dolor);
        atm_dolor_derecho.setAdapter(adapter_crepitacion_dolor);

        ArrayAdapter<CharSequence> adapter_apertura_cerrado = ArrayAdapter.createFromResource(this, R.array.atm_apertura_cerrado_array, android.R.layout.simple_spinner_item);
        adapter_apertura_cerrado.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        atm_apertura.setAdapter(adapter_apertura_cerrado);
        atm_cerrado.setAdapter(adapter_apertura_cerrado);
        */

    }



    @Override
    public String setViewTitle() {
        return "Exament ATM";
    }

    @Override
    protected int getActivityLayout() {
        return R.layout.activity_paciente_atm;
    }

    @Override
    public void onClick(View v) {
        pacienteAgregarExamenATM();
    }

    private void pacienteAgregarExamenATM() {
        //GENERAL
        Boolean get_htf = htf.isChecked();
        Boolean get_bruxismo = bruxismo.isChecked();
        Boolean get_tratamiento_ortodoncia = tratamiento_ortodoncia.isChecked();
        Boolean get_ruido_articular = ruido_articular.isChecked();
        Boolean get_artritis = artritis.isChecked();
        //CHASQUIDO
        Boolean get_chasquido_izquierdo_ninguno = chasquido_izquierdo_ninguno.isChecked();
        Boolean get_chasquido_izquierdo_apertura = chasquido_izquierdo_apertura.isChecked();
        Boolean get_chasquido_izquierdo_cierre = chasquido_izquierdo_cierre.isChecked();
        Boolean get_chasquido_derecho_ninguno = chasquido_derecho_ninguno.isChecked();
        Boolean get_chasquido_derecho_apertura = chasquido_derecho_apertura.isChecked();
        Boolean get_chasquido_derecho_cierre = chasquido_derecho_cierre.isChecked();
        //CREPITACION
        Boolean get_crepitacion_derecho_ninguno = crepitacion_derecho_ninguno.isChecked();
        Boolean get_crepitacion_derecho_articular = crepitacion_derecho_articular.isChecked();
        Boolean get_crepitacion_derecho_muscular = crepitacion_derecho_muscular.isChecked();
        Boolean get_crepitacion_izquierdo_ninguno = crepitacion_izquierdo_ninguno.isChecked();
        Boolean get_crepitacion_izquierdo_articular = crepitacion_izquierdo_articular.isChecked();
        Boolean get_crepitacion_izquierdo_muscular = crepitacion_izquierdo_muscular.isChecked();
        //DOLOR
        Boolean get_dolor_derecho_ninguno = dolor_derecho_ninguno.isChecked();
        Boolean get_dolor_derecho_articular = dolor_derecho_articular.isChecked();
        Boolean get_dolor_derecho_muscular = dolor_derecho_muscular.isChecked();
        Boolean get_dolor_izquierdo_ninguno = dolor_izquierdo_ninguno.isChecked();
        Boolean get_dolor_izquierdo_articular = dolor_izquierdo_articular.isChecked();
        Boolean get_dolor_izquierdo_muscular = dolor_izquierdo_muscular.isChecked();
        //MOVIMIENTO
        Boolean get_apertura_recto = apertura_recto.isChecked();
        Boolean get_apertura_ala_izquierda = apertura_ala_izquierda.isChecked();
        Boolean get_apertura_ala_derecha = apertura_ala_derecha.isChecked();
        Boolean get_cerrado_recto = cerrado_recto.isChecked();
        Boolean get_cerrado_ala_izquierda = cerrado_ala_izquierda.isChecked();
        Boolean get_cerrado_ala_derecha = cerrado_ala_derecha.isChecked();

        APIPlug cliente = ServiceGenerator.createService(APIPlug.class);

        Call<Response> call = cliente.addPatientATM(patientId, get_htf, get_bruxismo, get_tratamiento_ortodoncia, get_ruido_articular, get_artritis,
                get_chasquido_izquierdo_ninguno, get_chasquido_izquierdo_apertura, get_chasquido_izquierdo_cierre, get_chasquido_derecho_ninguno, get_chasquido_derecho_apertura, get_chasquido_derecho_cierre,
                get_crepitacion_izquierdo_ninguno, get_crepitacion_izquierdo_articular, get_crepitacion_izquierdo_muscular, get_crepitacion_derecho_ninguno, get_crepitacion_derecho_articular, get_crepitacion_derecho_muscular,
                get_dolor_izquierdo_ninguno, get_dolor_izquierdo_articular, get_dolor_izquierdo_muscular, get_dolor_derecho_ninguno, get_dolor_derecho_articular, get_dolor_derecho_muscular,
                get_apertura_recto, get_apertura_ala_izquierda, get_apertura_ala_derecha, get_cerrado_recto, get_cerrado_ala_izquierda, get_cerrado_ala_derecha);

        call.enqueue(new Callback<Response>() {
            @Override
            public void onResponse(Call<Response> call, retrofit2.Response<Response> response) {
                Toast.makeText(getBaseContext(), "Examen ATM creada correctamente!", Toast.LENGTH_SHORT).show();
                finish();
                startActivity(getIntent());
            }

            @Override
            public void onFailure(Call<Response> call, Throwable t) {
                Toast.makeText(getBaseContext(), "Examen ATM no se ha podido crear!", Toast.LENGTH_SHORT).show();
                finish();
                startActivity(getIntent());
            }
        });

    }


    private void getPatientATM(String patientId) {


        APIPlug cliente = ServiceGenerator.createService(APIPlug.class);

        Call<PatientATMModel> call = cliente.getPatientATM(patientId);

        call.enqueue(new Callback<PatientATMModel>() {

            Boolean htf_r = false, bruxismo_r = false, tratamiento_ortodoncia_r = false, ruido_articular_r = false, artritis_r = false,
                    chasquido_izquierdo_ninguno_r = false, chasquido_izquierdo_apertura_r = false, chasquido_izquierdo_cierre_r = false, chasquido_derecho_ninguno_r = false, chasquido_derecho_apertura_r = false, chasquido_derecho_cierre_r = false,
                    crepitacion_izquierdo_ninguno_r = false, crepitacion_izquierdo_articular_r = false, crepitacion_izquierdo_muscular_r = false, crepitacion_derecho_ninguno_r = false, crepitacion_derecho_articular_r = false, crepitacion_derecho_muscular_r = false,
                    dolor_izquierdo_ninguno_r = false, dolor_izquierdo_articular_r = false, dolor_izquierdo_muscular_r = false, dolor_derecho_ninguno_r = false, dolor_derecho_articular_r = false, dolor_derecho_muscular_r = false,
                    apertura_recto_r = false, apertura_ala_izquierda_r = false, apertura_ala_derecha_r = false, cerrado_recto_r = false, cerrado_ala_izquierda_r = false, cerrado_ala_derecha_r = false;


            @Override
            public void onResponse(Call<PatientATMModel> call, retrofit2.Response<PatientATMModel> response) {
                //Log.e("TRAIGO DATA", "PENDIENTE CONFIRMAR");
                htf_r = response.body().getHtf();
                bruxismo_r = response.body().getBruxismo();
                tratamiento_ortodoncia_r = response.body().getTratamiento_ortodoncia();
                ruido_articular_r = response.body().getRuido_articular();
                artritis_r = response.body().getArtritis();

                chasquido_izquierdo_ninguno_r = response.body().getChasquido_izquierdo_ninguno();
                chasquido_izquierdo_apertura_r = response.body().getChasquido_izquierdo_apertura();
                chasquido_izquierdo_cierre_r = response.body().getChasquido_izquierdo_cierre();
                chasquido_derecho_ninguno_r = response.body().getChasquido_derecho_ninguno();
                chasquido_derecho_apertura_r = response.body().getChasquido_derecho_apertura();
                chasquido_derecho_cierre_r = response.body().getChasquido_derecho_cierre();

                Log.e("CHASQUIO DERECHO", String.valueOf(chasquido_derecho_ninguno_r));
                crepitacion_izquierdo_ninguno_r = response.body().getCrepitacion_izquierdo_ninguno();
                crepitacion_izquierdo_articular_r = response.body().getCrepitacion_izquierdo_articular();
                crepitacion_izquierdo_muscular_r = response.body().getCrepitacion_izquierdo_muscular();
                crepitacion_derecho_ninguno_r = response.body().getCrepitacion_derecho_ninguno();
                crepitacion_derecho_articular_r = response.body().getCrepitacion_derecho_articular();
                crepitacion_derecho_muscular_r = response.body().getCrepitacion_derecho_muscular();
                
                dolor_izquierdo_ninguno_r = response.body().getDolor_izquierdo_ninguno();
                dolor_izquierdo_articular_r = response.body().getDolor_izquierdo_articular();
                dolor_izquierdo_muscular_r = response.body().getDolor_izquierdo_muscular();
                dolor_derecho_ninguno_r = response.body().getDolor_derecho_ninguno();
                dolor_derecho_articular_r = response.body().getDolor_derecho_articular();
                dolor_derecho_muscular_r = response.body().getDolor_derecho_muscular();

                apertura_recto_r = response.body().getApertura_recto();
                apertura_ala_izquierda_r = response.body().getApertura_ala_izquierda();
                apertura_ala_derecha_r = response.body().getApertura_ala_derecha();
                cerrado_recto_r = response.body().getCerrado_recto();
                cerrado_ala_izquierda_r = response.body().getCerrado_ala_izquierda();
                cerrado_ala_derecha_r = response.body().getCerrado_ala_derecha();

                if(htf_r){htf.setChecked(true);}
                if(bruxismo_r){bruxismo.setChecked(true);}
                if(tratamiento_ortodoncia_r){tratamiento_ortodoncia.setChecked(true);}
                if(ruido_articular_r){ruido_articular.setChecked(true);}
                if(artritis_r){artritis.setChecked(true);}
                
                if(chasquido_izquierdo_ninguno_r == false){chasquido_izquierdo_ninguno.setChecked(false);}
                if(chasquido_izquierdo_apertura_r){chasquido_izquierdo_apertura.setChecked(true);}
                if(chasquido_izquierdo_cierre_r){chasquido_izquierdo_cierre.setChecked(true);}
                if(chasquido_derecho_ninguno_r == false){chasquido_derecho_ninguno.setChecked(false);}
                if(chasquido_derecho_apertura_r){chasquido_derecho_apertura.setChecked(true);}
                if(chasquido_derecho_cierre_r){chasquido_derecho_cierre.setChecked(true);}

                if(crepitacion_izquierdo_ninguno_r == false){crepitacion_izquierdo_ninguno.setChecked(false);}
                if(crepitacion_izquierdo_articular_r){crepitacion_izquierdo_articular.setChecked(true);}
                if(crepitacion_izquierdo_muscular_r){crepitacion_izquierdo_muscular.setChecked(true);}
                if(crepitacion_derecho_ninguno_r == false){crepitacion_derecho_ninguno.setChecked(false);}
                if(crepitacion_derecho_articular_r){crepitacion_derecho_articular.setChecked(true);}
                if(crepitacion_derecho_muscular_r){crepitacion_derecho_muscular.setChecked(true);}
                
                if(dolor_izquierdo_ninguno_r == false){dolor_izquierdo_ninguno.setChecked(false);}
                if(dolor_izquierdo_articular_r){dolor_izquierdo_articular.setChecked(true);}
                if(dolor_izquierdo_muscular_r){dolor_izquierdo_muscular.setChecked(true);}
                if(dolor_derecho_ninguno_r == false){dolor_derecho_ninguno.setChecked(false);}
                if(dolor_derecho_articular_r){dolor_derecho_articular.setChecked(true);}
                if(dolor_derecho_muscular_r){dolor_derecho_muscular.setChecked(true);}
                
                if(apertura_recto_r == false){apertura_recto.setChecked(false);}
                if(apertura_ala_izquierda_r){apertura_ala_izquierda.setChecked(true);}
                if(apertura_ala_derecha_r){apertura_ala_derecha.setChecked(true);}

                if(cerrado_recto_r == false){cerrado_recto.setChecked(false);}
                if(cerrado_ala_izquierda_r){cerrado_ala_izquierda.setChecked(true);}
                if(cerrado_ala_derecha_r){cerrado_ala_derecha.setChecked(true);}
                
            }

            @Override
            public void onFailure(Call<PatientATMModel> call, Throwable t) {
                //Log.e("NO TRAIGO DATA", "PENDIENTE AVERIGUAR");
            }
        });


    }




}

package sigel.smart_dent.modules.patient_management.patients;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import java.util.ArrayList;
import java.util.List;

import sigel.smart_dent.R;
import sigel.smart_dent.adapters.PacienteAdapter;
import sigel.smart_dent.models.Paciente;
import sigel.smart_dent.modules.user_management.activities.Dashboard;
import sigel.smart_dent.modules.user_management.activities.Login;
import sigel.smart_dent.rest.APIPlug;
import sigel.smart_dent.rest.ServiceGenerator;
import sigel.smart_dent.sqlite.OperationsDB;
import sigel.smart_dent.syn.Syn;

/**
 * Created by pablin on 23/05/2016.
 */
public class PacienteListar extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private RecyclerView recyclerView;
    private PacienteAdapter pacienteAdapter;

    List<Paciente> pacientesList;
    OperationsDB operationsDB;

    Syn syn;
    String idUsuario;

    APIPlug cliente = ServiceGenerator.createService(APIPlug.class);
    ArrayList<Paciente> pacienteArrayList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_paciente_listar);

        syn =  new Syn();

        idUsuario = syn.userId(getApplicationContext());

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        recyclerView = (RecyclerView) findViewById(R.id.pacienteListarRecyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getApplication()));

        pacienteAdapter = new sigel.smart_dent.adapters.PacienteAdapter(getApplication());
        recyclerView.setAdapter(pacienteAdapter);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        setTitle("Listado de pacientes");
        //getPacientes();
    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_paciente_listar, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){

        int id = item.getItemId();

        if(id == R.id.paciente_menu_listar){
            //startActivity(new Intent(this, PacienteListar.class));
        }else if(id == R.id.paciente_menu_agregar){
            startActivity(new Intent(this, PacienteAgregar.class));
        }else{
            //Toast.makeText(getBaseContext(), "OTRA OPCION MENU LISTAR OTRA COSA", Toast.LENGTH_SHORT).show();
        }
        return true;
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.paciente_listado) {
            Intent intent = new Intent(this, Dashboard.class);
            intent.putExtra("currentPage",1);
            startActivity(intent);
        } else if (id == R.id.usuario_dashboard) {
            startActivity(new Intent(this, Dashboard.class));
        } else if (id == R.id.salir) {
            startActivity(new Intent(this, Login.class));
        } else {
            //Toast.makeText(getBaseContext(), "OTRA OPCION", Toast.LENGTH_SHORT).show();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

}

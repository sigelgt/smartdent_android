package sigel.smart_dent.modules.patient_management.odontogram.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import sigel.smart_dent.R;

/**
 * Created by Juan on 16/06/2016.
 */
public abstract class OdontogramCuadrantsAdapter extends RecyclerView.Adapter {

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(getLayoutId(), parent, false);
        return new ListViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ((ListViewHolder) holder).bindView(position);
    }

    @Override
    public int getItemCount() {
        return 8;
    }

    private class ListViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private ImageView mBucalImateView;
        private ImageView mOclusalImateView;
        private String mIndexName;
        private int mIndex;

        public ListViewHolder(View itemView) {
            super(itemView);
            this.mBucalImateView = (ImageView) itemView.findViewById(R.id.bucalImage);
            this.mOclusalImateView = (ImageView) itemView.findViewById(R.id.oclusalImage);
            itemView.setOnClickListener(this);
        }

        public void bindView (int position){
            this.mIndex = position;
            this.mBucalImateView.setImageResource(getBucalResourceId(position));
            this.mOclusalImateView.setImageResource(getOclusalResourceId(position));
            this.mIndexName = getIndexName(position);
        }

        @Override
        public void onClick(View v) {
            onItemSelected(this.mIndex);
        }

    }

    public abstract void onItemSelected(int index);
    public abstract int getBucalResourceId(int position);
    public abstract int getOclusalResourceId(int position);
    public abstract String getIndexName(int position);
    public abstract int getLayoutId();
}

package sigel.smart_dent.sqlite;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;

import sigel.smart_dent.models.Paciente;
import sigel.smart_dent.sqlite.columnsTables.*;
import sigel.smart_dent.sqlite.BD_SQLite.*;

/**
 * Created by juanlopez on 7/28/16.
 */
public class OperationsDB {
    private static BD_SQLite baseDatos;
    private static OperationsDB instancia = new OperationsDB();

    private OperationsDB() {
    }
    public static OperationsDB obtenerInstancia(Context contexto) {
        if (baseDatos == null) {
            baseDatos = new BD_SQLite(contexto);
        }
        return instancia;
    }
    public int getPatientsCount() {

        SQLiteDatabase db = baseDatos.getReadableDatabase();

        String sql = String.format("SELECT COUNT(*) FROM %s", Tables.Patients);

        Cursor cursor = db.rawQuery(sql,null);

        cursor.moveToFirst();
        int total = cursor.getInt(0);
        cursor.close();
        return total;
    }
    public ArrayList<Paciente> obtenerPatients() {
        ArrayList<Paciente> pacienteList = new ArrayList<>();

        SQLiteDatabase db = baseDatos.getReadableDatabase();

        String sql = String.format("SELECT * FROM %s", Tables.Patients);

        Cursor cursor = db.rawQuery(sql,null);

        Paciente paciente = null;
        if (cursor.moveToFirst()) {
            do {
                paciente = new Paciente();
                paciente.set_id(cursor.getString(0));
                paciente.set_id_doctor(cursor.getString(1));
                paciente.setImagen(cursor.getString(16));
                paciente.setNombres(cursor.getString(2));
                paciente.setApellidos(cursor.getString(3));
                paciente.setFecha_nacimiento(cursor.getString(7));
                paciente.setGenero(cursor.getString(8));
                paciente.setNacionalidad(cursor.getString(10));
                paciente.setEstado_civil(cursor.getString(6));
                paciente.setDireccion(cursor.getString(4));
                paciente.setEmail(cursor.getString(5));
                paciente.setOcupacion(cursor.getString(9));
                paciente.setNumero_telefono(cursor.getString(11));
                paciente.setReferido_por(cursor.getString(12));
                paciente.setContacto(cursor.getString(13));
                paciente.setContacto_numero_telefono(cursor.getString(14));
                paciente.setConsideraciones_medicas(cursor.getString(15));
                pacienteList.add(paciente);
            } while (cursor.moveToNext());
        }

        Log.d("obtenerPatients",pacienteList.toString());
        return pacienteList;
    }
    public ArrayList<Paciente> obtenerPatientsOffLine() {
        ArrayList<Paciente> pacienteList = new ArrayList<>();

        SQLiteDatabase db = baseDatos.getReadableDatabase();

        String whereClause = String.format("%s=?", Patients.ESTADO);
        String[] whereArgs = {"1"};

        String sql = "SELECT * FROM "+Tables.Patients+" WHERE "+Patients.ESTADO+"=1";

        Cursor cursor = db.rawQuery(sql,null);

        Paciente paciente = null;
        if (cursor.moveToFirst()) {
            do {
                paciente = new Paciente();
                paciente.set_id(cursor.getString(0));
                paciente.set_id_doctor(cursor.getString(1));
                paciente.setImagen(cursor.getString(16));
                paciente.setNombres(cursor.getString(2));
                paciente.setApellidos(cursor.getString(3));
                paciente.setFecha_nacimiento(cursor.getString(7));
                paciente.setGenero(cursor.getString(8));
                paciente.setNacionalidad(cursor.getString(10));
                paciente.setEstado_civil(cursor.getString(6));
                paciente.setDireccion(cursor.getString(4));
                paciente.setEmail(cursor.getString(5));
                paciente.setOcupacion(cursor.getString(9));
                paciente.setNumero_telefono(cursor.getString(11));
                paciente.setReferido_por(cursor.getString(12));
                paciente.setContacto(cursor.getString(13));
                paciente.setContacto_numero_telefono(cursor.getString(14));
                paciente.setConsideraciones_medicas(cursor.getString(15));
                pacienteList.add(paciente);
            } while (cursor.moveToNext());
        }
        for (Paciente paciente1 : pacienteList){
            paciente1.setEstado("0");
            updatePatients(paciente1);
        }

        Log.d("obtenerPatients",pacienteList.toString());
        return pacienteList;
    }
    private Paciente cursorToComment(Cursor cursor) {
        Paciente paciente = new Paciente();

        paciente.set_id(cursor.getString(0));

        return paciente;
    }
    public String insertPatients(Paciente paciente,String idPaciente) {

        SQLiteDatabase db = baseDatos.getWritableDatabase();

        String idPatients = idPaciente;

        ContentValues valores = new ContentValues();
        valores.put(Patients.ID, idPatients);
        valores.put(Patients.ID_DOCTOR, paciente.get_id_doctor());
        valores.put(Patients.IMAGEN, paciente.getImagen());
        valores.put(Patients.NOMBRES, paciente.getNombres());
        valores.put(Patients.APELLIDOS, paciente.getApellidos());
        valores.put(Patients.FECHA_NACIMIENTO, paciente.getFecha_nacimiento());
        valores.put(Patients.GENERO, paciente.getGenero());
        valores.put(Patients.NACIONALIDAD, paciente.getNacionalidad());
        valores.put(Patients.ESTADO_CIVIL, paciente.getEstado_civil());
        valores.put(Patients.DIRECCION, paciente.getDireccion());
        valores.put(Patients.EMAIL, paciente.getEmail());
        valores.put(Patients.OCUPACION, paciente.getOcupacion());
        valores.put(Patients.TELEFONO, paciente.getNumero_telefono());
        valores.put(Patients.REFERIDO, paciente.getReferido_por());
        valores.put(Patients.CONTACTO, paciente.getContacto());
        valores.put(Patients.TELEFONO_CONTACTO, paciente.getContacto_numero_telefono());
        valores.put(Patients.CONSIDERACION, paciente.getConsideraciones_medicas());
        valores.put(Patients.ESTADO, paciente.getEstado());
        valores.put(Patients.EDAD, paciente.getEdad());
        valores.put(Patients.NUMERO_IDENTIFICACION, paciente.getNumero_identificacion());
        valores.put(Patients.TIPO_IDENTIFICACION, paciente.getTipo_identificacion());
        valores.put(Patients.MOTIVO_CONSULTA, paciente.getMotivo_consulta());

        // Insertar Patients
        return db.insertOrThrow(Tables.Patients, null, valores) > 0 ? idPatients : null;
    }
    public boolean updatePatients(Paciente paciente) {
        SQLiteDatabase db = baseDatos.getWritableDatabase();

        ContentValues valores = new ContentValues();
        valores.put(Patients.ID, paciente.get_id());
        valores.put(Patients.ID_DOCTOR, paciente.get_id_doctor());
        valores.put(Patients.IMAGEN, paciente.getImagen());
        valores.put(Patients.NOMBRES, paciente.getNombres());
        valores.put(Patients.APELLIDOS, paciente.getApellidos());
        valores.put(Patients.FECHA_NACIMIENTO, paciente.getFecha_nacimiento());
        valores.put(Patients.GENERO, paciente.getGenero());
        valores.put(Patients.NACIONALIDAD, paciente.getNacionalidad());
        valores.put(Patients.ESTADO_CIVIL, paciente.getEstado_civil());
        valores.put(Patients.DIRECCION, paciente.getDireccion());
        valores.put(Patients.EMAIL, paciente.getEmail());
        valores.put(Patients.OCUPACION, paciente.getOcupacion());
        valores.put(Patients.TELEFONO, paciente.getNumero_telefono());
        valores.put(Patients.REFERIDO, paciente.getReferido_por());
        valores.put(Patients.CONTACTO, paciente.getContacto());
        valores.put(Patients.TELEFONO_CONTACTO, paciente.getContacto_numero_telefono());
        valores.put(Patients.CONSIDERACION, paciente.getConsideraciones_medicas());
        valores.put(Patients.ESTADO, paciente.getEstado());
        valores.put(Patients.EDAD, paciente.getEdad());
        valores.put(Patients.NUMERO_IDENTIFICACION, paciente.getNumero_identificacion());
        valores.put(Patients.TIPO_IDENTIFICACION, paciente.getTipo_identificacion());
        valores.put(Patients.MOTIVO_CONSULTA, paciente.getMotivo_consulta());

        String whereClause = String.format("%s=?", Patients.ID);
        String[] whereArgs = {paciente.get_id()};

        int resultado = db.update(Tables.Patients, valores, whereClause, whereArgs);

        return resultado > 0;
    }
    public boolean deletePatients(String idPatients) {
        SQLiteDatabase db = baseDatos.getWritableDatabase();

        String whereClause = String.format("%s=?", Patients.ID);
        String[] whereArgs = {idPatients};

        int resultado = db.delete(Tables.Patients, whereClause, whereArgs);

        return resultado > 0;
    }
    public SQLiteDatabase getDb() {
        return baseDatos.getWritableDatabase();
    }
}
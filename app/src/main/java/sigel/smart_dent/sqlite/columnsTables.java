package sigel.smart_dent.sqlite;

import java.util.UUID;

/**
 * Created by juanlopez on 7/28/16.
 */
public class columnsTables {
    private columnsTables(){}
    interface columnsPatients{
        String ID = "id";
        String ID_DOCTOR = "id_doctor";
        String IMAGEN = "imagen";
        String NOMBRES = "nombres";
        String APELLIDOS = "apellidos";
        String FECHA_NACIMIENTO = "fecha_nacimiento";
        String GENERO = "genero";
        String NACIONALIDAD = "nacionalidad";
        String ESTADO_CIVIL = "estado_civil";
        String DIRECCION = "direccion";
        String EMAIL = "email";
        String OCUPACION = "ocupacion";
        String TELEFONO = "numero_telefono";
        String REFERIDO = "referido_por";
        String CONTACTO = "contacto";
        String TELEFONO_CONTACTO = "contacto_numero_telefono";
        String CONSIDERACION = "consideraciones_medicas";
        String ESTADO = "estado";
        String EDAD = "edad";
        String NUMERO_IDENTIFICACION = "numero_identificacion";
        String TIPO_IDENTIFICACION = "tipo_identificacion";
        String MOTIVO_CONSULTA = "motivo_consulta";
    }
    public static class Patients implements columnsPatients{
        public static String generarIdPaciente() {
            return "PACI-" + UUID.randomUUID().toString();
        }
    }

}

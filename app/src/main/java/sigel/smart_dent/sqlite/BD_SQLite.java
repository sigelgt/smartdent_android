package sigel.smart_dent.sqlite;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import sigel.smart_dent.sqlite.columnsTables.*;

/**
 * Created by juanlopez on 7/28/16.
 */

public class BD_SQLite extends SQLiteOpenHelper {

    private static final String NOMBRE_BASE_DATOS = "smartDent.db";
    private static final int VERSION_ACTUAL = 1;
    private final Context contexto;

    interface Tables {
        String Patients = "Patients";
    }

    public BD_SQLite(Context contexto) {
        super(contexto, NOMBRE_BASE_DATOS, null, VERSION_ACTUAL);
        this.contexto = contexto;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL(String.format("CREATE TABLE %s (%s TEXT NOT NULL,%s TEXT NOT NULL, %s TEXT NOT NULL, %s TEXT NOT NULL,"+
                " %s TEXT, %s TEXT, %s TEXT, %s TEXT, %s TEXT, %s TEXT, %s TEXT, %s TEXT, %s TEXT, %s TEXT,"+
                " %s TEXT, %s TEXT, %s BLOB,%s TEXT, %s TEXT, %s TEXT, %s TEXT, %s TEXT);",Tables.Patients, Patients.ID,Patients.ID_DOCTOR,
                Patients.NOMBRES,Patients.APELLIDOS,Patients.DIRECCION,Patients.EMAIL,Patients.ESTADO_CIVIL,
                Patients.FECHA_NACIMIENTO,Patients.GENERO, Patients.OCUPACION,Patients.NACIONALIDAD,
                Patients.TELEFONO,Patients.REFERIDO,Patients.CONTACTO, Patients.TELEFONO_CONTACTO,
                Patients.CONSIDERACION,Patients.IMAGEN,Patients.ESTADO,Patients.EDAD,
                Patients.NUMERO_IDENTIFICACION,Patients.TIPO_IDENTIFICACION,Patients.MOTIVO_CONSULTA));
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int versionAnterior, int versionNueva) {

        db.execSQL("DROP TABLE IF EXISTS " + Tables.Patients);

        onCreate(db);
    }
}
package sigel.smart_dent.rest;

import java.util.List;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;
import sigel.smart_dent.beans.Response;
import sigel.smart_dent.beans.ResponseFileUpload;
import sigel.smart_dent.models.Paciente;
import sigel.smart_dent.modules.patient_management.atm.models.PatientATMModel;
import sigel.smart_dent.modules.patient_management.medical_record.models.PacienteHistoriaMedicaModel;
import sigel.smart_dent.modules.patient_management.odontology.models.PacienteHistoriaOdontologicaModel;
import sigel.smart_dent.modules.user_management.Usuario;

/**
 * Created by pablin on 23/05/2016.
 */
public interface APIPlug {
    @GET("pacientes/{id_doctor}")
    Call<List<Paciente>> getPacientes(@Path("id_doctor") String id_doctor);

    @GET("usuarios/{user}/{pass}")
    Call<Usuario> loginUser(
            @Path("user") String user,
            @Path("pass") String pass
    );

    @FormUrlEncoded
    @POST("usuarios")
    Call<Usuario> registerUser(
            @Field("city") String city,
            @Field("address") String address,
            @Field("postalCode") String postalCode,
            @Field("dateBirth") String dateBirth,
            @Field("name") String name,
            @Field("documentIdentification") String documentIdentification,
            @Field("numberDocument") String numberDocument,
            @Field("email") String email,
            @Field("profession") String profession,
            @Field("nationality") String nationality,
            @Field("province") String province,
            @Field("telephone") String telephone,
            @Field("password") String password,
            @Field("lastName") String lastName,
            @Field("user") String user,
            @Field("id_planes") String id_planes
    );

    @FormUrlEncoded
    @POST("pacientes")
    Call<Response> agregarPaciente(
            @Field("_id_doctor") String id_doctor,
            @Field("imagen") String imagen,
            @Field("motivo_consulta") String motivo_consulta,
            @Field("nombres") String nombres,
            @Field("apellidos") String apellidos,
            @Field("fecha_nacimiento") String fecha_nacimiento,
            @Field("edad") String edad,
            @Field("tipo_identificacion") String tipo_identificacion,
            @Field("numero_identificacion") String numero_identificacion,
            @Field("genero") String genero,
            @Field("nacionalidad") String nacionalidad,
            @Field("estado_civil") String estado_civil,
            @Field("direccion") String direccion,
            @Field("email") String email,
            @Field("ocupacion") String ocupacion,
            @Field("numero_telefono") String numero_telefono,
            @Field("referido_por") String referido_por,
            @Field("contacto") String contacto,
            @Field("contacto_numero_telefono") String contacto_numero_telefono,
            @Field("consideraciones_medicas") String consideraciones_medicas
    );


    @Multipart
    @POST("file_receiver.php")
    Call<ResponseFileUpload> uploadFile(@Part MultipartBody.Part file);

    @FormUrlEncoded
    @POST("pacientes_historia_medica")
    Call<Response> agregarPacienteHistoriaMedica(
            @Field("_id_paciente") String _id_paciente,
            @Field("alergia") Boolean alergia,
            @Field("alergiaAnestesia") Boolean alergiaAnestesia,
            @Field("alergiaAntibioticos") Boolean alergiaAntibioticos,
            @Field("alergiaOtros") Boolean alergiaOtros,
            @Field("otros") Boolean Otros,
            @Field("anemia") Boolean anemia,
            @Field("artritis") Boolean artritis,
            @Field("asma") Boolean asma,
            @Field("cancer") Boolean cancer,
            @Field("cefalea") Boolean cefalea,
            @Field("diabetes") Boolean diabetes,
            @Field("embarazo") Boolean embarazo,
            @Field("encefalitis") Boolean encefalitis,
            @Field("enfermedad_renal") Boolean enfermedad_renal,
            @Field("epilepsia") Boolean epilepsia,
            @Field("fiebre_reumatica") Boolean fiebre_reumatica,
            @Field("hepatitis") Boolean hepatitis,
            @Field("problemas_endocrinos") Boolean problemas_endocrinos,
            @Field("trauma_craneo_facial") Boolean trauma_craneo_facial,
            @Field("tabaquismo") Boolean tabaquismo,
            @Field("tuberculosis") Boolean tuberculosis,
            @Field("infectocontagiosas") Boolean infectocontagiosas,
           // @Field("alergiaOtrosComent") String alergiaOtrosComent,
            @Field("presionalta") Boolean presionalta,
            @Field("presionbaja") Boolean presionbaja,
            @Field("SoploCardiaco") Boolean SoploCardiaco,
            @Field("Infarto") Boolean mInfarto,
            @Field("ProblemasCirculacion") Boolean ProblemasCirculacion,
            @Field("ProblemasCardiacos") Boolean ProblemasCardiacos
    );

    @GET("pacientes_historia_medica/{id_paciente}")
    Call<PacienteHistoriaMedicaModel> getPatientMedicalRecord(
            @Path("id_paciente") String id_paciente
    );

    @FormUrlEncoded
    @POST("pacientes_historia_odontologica")
    Call<Response> agregarPacienteHistoriaOdontologica(
            @Field("_id_paciente") String patientId,
            @Field("sensibility_hot_beverages") Boolean sensibility_hot_beverages,
            @Field("sensibility_sweet_food") Boolean sensibility_sweet_food,
            @Field("sensibility_chewing") Boolean sensibility_chewing,
            @Field("sensibility_air") Boolean sensibility_air,
            @Field("sensibility_cold_beverages") Boolean sensibility_cold_beverages,
            @Field("labio_superior") Boolean labio_superior,
            @Field("labio_inferior") Boolean labio_inferior,
            @Field("frenillos") Boolean frenillos,
            @Field("paladar") Boolean paladar,
            @Field("piso_de_boca") Boolean piso_de_boca,
            @Field("mucosa") Boolean mucosa,
            @Field("trauma_oclusal") Boolean trauma_oclusal,
            @Field("lengua") Boolean lengua,
            @Field("color_encias") Boolean color_encias,
            @Field("edema_gingival") Boolean edema_gingival,
            @Field("inflamacion_gingival") Boolean inflamacion_gingival,
            @Field("hemorragia_encias") Boolean hemorragia_encias,
            @Field("parulis") Boolean parulis,
            @Field("otros") Boolean otros
    );

    @GET("pacientes_historia_odontologica/{id_paciente}")
    Call<PacienteHistoriaOdontologicaModel> getPatientMedicalOdontologicoRecord(
            @Path("id_paciente") String id_paciente
    );

    @FormUrlEncoded
    @POST("pacientes_examen_atm")
    Call<Response> addPatientATM(
            @Field("_id_paciente") String _id_paciente,
            //GENERALES
            @Field("htf") Boolean htf,
            @Field("bruxismo") Boolean bruxismo,
            @Field("tratamiento_ortodoncia") Boolean tratamiento_ortodoncia,
            @Field("ruido_articular") Boolean ruido_articular,
            @Field("artritis") Boolean artritis,
            //CHASQUIDO
            @Field("chasquido_izquierdo_ninguno") Boolean chasquido_izquierdo_ninguno,
            @Field("chasquido_izquierdo_apertura") Boolean chasquido_izquierdo_apertura,
            @Field("chasquido_izquierdo_cierre") Boolean chasquido_izquierdo_cierre,
            @Field("chasquido_derecho_ninguno") Boolean chasquido_derecho_ninguno,
            @Field("chasquido_derecho_apertura") Boolean chasquido_derecho_apertura,
            @Field("chasquido_derecho_cierre") Boolean chasquido_derecho_cierre,
            //CREPITACION
            @Field("crepitacion_izquierdo_ninguno") Boolean crepitacion_izquierdo_ninguno,
            @Field("crepitacion_izquierdo_articular") Boolean crepitacion_izquierdo_articular,
            @Field("crepitacion_izquierdo_muscular") Boolean crepitacion_izquierdo_muscular,
            @Field("crepitacion_derecho_ninguno") Boolean crepitacion_derecho_ninguno,
            @Field("crepitacion_derecho_articular") Boolean crepitacion_derecho_articular,
            @Field("crepitacion_derecho_muscular") Boolean crepitacion_derecho_muscular,
            //DOLOR
            @Field("dolor_izquierdo_ninguno") Boolean dolor_izquierdo_ninguno,
            @Field("dolor_izquierdo_articular") Boolean dolor_izquierdo_articular,
            @Field("dolor_izquierdo_muscular") Boolean dolor_izquierdo_muscular,
            @Field("dolor_derecho_ninguno") Boolean dolor_derecho_ninguno,
            @Field("dolor_derecho_articular") Boolean dolor_derecho_articular,
            @Field("dolor_derecho_muscular") Boolean dolor_derecho_muscular,
            //MOVIMIENTO
            @Field("apertura_recto") Boolean apertura_recto,
            @Field("apertura_ala_izquierda") Boolean apertura_ala_izquierda,
            @Field("apertura_ala_derecha") Boolean apertura_ala_derecha,
            @Field("cerrado_recto") Boolean cerrado_recto,
            @Field("cerrado_ala_izquierda") Boolean cerrado_ala_izquierda,
            @Field("cerrado_ala_derecha") Boolean cerrado_ala_derecha
    );

    @GET("pacientes_examen_atm/{id_paciente}")
    Call<PatientATMModel> getPatientATM(
            @Path("id_paciente") String id_paciente
    );
}

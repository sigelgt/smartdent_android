package sigel.smart_dent.syn;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import retrofit2.Call;
import retrofit2.Callback;
import sigel.smart_dent.beans.Response;
import sigel.smart_dent.models.Paciente;
import sigel.smart_dent.rest.APIPlug;
import sigel.smart_dent.rest.ServiceGenerator;
import sigel.smart_dent.sqlite.OperationsDB;

/**
 * Created by juanlopez on 7/26/16.
 */
public class Syn {
    private static final String PREFS_FILE = "sigel.smart_dent.preferences";

    public String userId(Context context){
        SharedPreferences prefs = context.getSharedPreferences(PREFS_FILE, context.MODE_PRIVATE);
        String restoredID = prefs.getString("_id", null);
        return restoredID;
    }

    public boolean validateSession(Context context){
        boolean validar = false;

        SharedPreferences prefs = context.getSharedPreferences(PREFS_FILE, context.MODE_PRIVATE);
        Boolean restoredSesion = prefs.getBoolean("sesion", false);

        if (restoredSesion){
            validar = true;
        }
        return validar;
    }
    public boolean closeSession(Context context){
        SharedPreferences.Editor editorSharedPref = context.getSharedPreferences(PREFS_FILE, context.MODE_PRIVATE).edit();
        Boolean sesion = false;
        editorSharedPref.putBoolean("sesion", sesion);
        editorSharedPref.commit();

        return true;
    }
    public void synPat(Paciente paciente,Context context) {
        final OperationsDB operationsDB = OperationsDB.obtenerInstancia(context);
        final Paciente paciente1 = paciente;

        APIPlug cliente = ServiceGenerator.createService(APIPlug.class);

        Call<Response> call = cliente.agregarPaciente(paciente.get_id_doctor(),paciente.getImagen(),
                paciente.getMotivo_consulta(), paciente.getNombres(),paciente.getApellidos(),
                paciente.getFecha_nacimiento(),paciente.getEdad(), paciente.getTipo_identificacion(),
                paciente.getNumero_identificacion(),paciente.getGenero(),paciente.getNacionalidad(),
                paciente.getEstado_civil(),paciente.getDireccion(),paciente.getEmail(),paciente.getOcupacion(),
                paciente.getNumero_telefono(),paciente.getReferido_por(),paciente.getContacto(),
                paciente.getContacto_numero_telefono(),paciente.getConsideraciones_medicas());

        call.enqueue(new Callback<Response>() {
            @Override
            public void onResponse(Call<sigel.smart_dent.beans.Response> call, retrofit2.Response<Response> response) {
                Log.e("onResponse",response.body().toString());
                paciente1.set_id(response.body().getInsertedIds().get(0));
                operationsDB.updatePatients(paciente1);
            }

            @Override
            public void onFailure(Call<sigel.smart_dent.beans.Response> call, Throwable t) {
                Log.e("onFailure", t.toString());
            }
        });
    }

}

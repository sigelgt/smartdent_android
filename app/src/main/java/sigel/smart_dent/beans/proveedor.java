package sigel.smart_dent.beans;

/**
 * Created by eduardogomez on 12/04/16.
 */
public class proveedor {
    public String nombre;
    public String foto;
    public String descripcion;

    public String getnombre() {
        return nombre;
    }

    public void setnombre(String nombre) {
        this.nombre = nombre;
    }

    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
}

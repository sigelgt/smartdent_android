package sigel.smart_dent.beans;

import java.io.Serializable;

/**
 * Created by eduardogomez on 03/03/16.
 */
public class usuario implements Serializable{
    private String message;
    private String email;
    private String password;
    private String restult;

    public String getRestult() {
        return restult;
    }

    public void setRestult(String restult) {
        this.restult = restult;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }



}

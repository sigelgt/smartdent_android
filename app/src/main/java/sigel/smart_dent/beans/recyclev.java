package sigel.smart_dent.beans;

/**
 * Created by eduardogomez on 12/05/16.
 */
public class recyclev {

    private int foto;
    private String nombre;

    public recyclev(int foto, String nombre) {
        this.foto = foto;
        this.nombre = nombre;
    }

    public int getFoto() {
        return foto;
    }

    public void setFoto(int foto) {
        this.foto = foto;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
}

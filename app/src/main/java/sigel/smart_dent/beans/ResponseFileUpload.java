package sigel.smart_dent.beans;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by pablin on 30/08/2016.
 */
public class ResponseFileUpload {

    @SerializedName("result")
    @Expose
    private String result;


    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }
}

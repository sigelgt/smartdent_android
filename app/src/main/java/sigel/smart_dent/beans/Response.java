package sigel.smart_dent.beans;

import java.util.List;

/**
 * Created by eduardogomez on 10/03/16.
 */
public class Response{
    public String insertedCount;
    public boolean message;
    public List<String> insertedIds;

    public String getInsertedCount() {
        return insertedCount;
    }

    public void setInsertedCount(String insertedCount) {
        this.insertedCount = insertedCount;
    }

    public boolean getMessage() {
        return message;
    }

    public List<String> getInsertedIds() {
        return insertedIds;
    }

    public void setInsertedIds(List<String> insertedIds) {
        this.insertedIds = insertedIds;
    }
}
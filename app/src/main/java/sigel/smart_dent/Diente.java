package sigel.smart_dent;

/**
 * Created by pablin on 9/06/2016.
 */
public class Diente {

    private String name;
    private int imageSource;

    public Diente (int imageSource, String name) {
        this.name = name;
        this.imageSource = imageSource;
    }

    public String getName() {
        return name;
    }

    public int getImageSource() {
        return imageSource;
    }
}

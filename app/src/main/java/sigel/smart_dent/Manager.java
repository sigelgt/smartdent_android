package sigel.smart_dent;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import java.sql.DatabaseMetaData;

/**
 * Created by eduardogomez on 02/03/16.
 */
public class Manager {
    public static final String TABLE_NAME = "contactos";

    public static final String CN_ID = "_id";
    public static final String CN_NAME = "nombre";
    public static final String CN_PASSWORD = "contraseña";

    //create table contactos(
    //                      _id integer primary key autoincrement,
    //                      nombr texto not null,
    //                      telefono text);

    public static final String CREATE_TABLE = "create table " +TABLE_NAME+ " ("
            +CN_ID+" integer primary key autoincrement,"
            +CN_NAME+ " text not null,"
            +CN_PASSWORD+ " text);";

//    private DBHelper helper;
//    private SQLiteDatabase db;
//
//    public Manager(Context context){
//
//        helper = new DbHelper(context);
//        db = helper.getWritableDatabase();
//    }

}

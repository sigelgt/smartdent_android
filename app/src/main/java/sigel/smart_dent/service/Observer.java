package sigel.smart_dent.service;

/**
 * Created by juanlopez on 8/3/16.
 */
public interface Observer {
    public void update();
}

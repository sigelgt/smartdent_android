package sigel.smart_dent.service;

/**
 * Created by juanlopez on 7/28/16.
 */
public interface ObservantSubject {
    public void notificar();
}

package sigel.smart_dent.service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.util.Log;

import sigel.smart_dent.models.Paciente;
import sigel.smart_dent.modules.user_management.activities.Dashboard;
import sigel.smart_dent.sqlite.OperationsDB;
import sigel.smart_dent.syn.Syn;

/**
 * Created by juanlopez on 8/3/16.
 */
public class NetworkConnectivityCheck {

    private BroadcastReceiver networkChangeReceiver;
    public boolean internetAvailable = false;
    public NetworkConnectivityCheck(){
        this.networkChangeReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                int networkState = NetworkUtil.getConnectionStatus(context);
                if(networkState == NetworkUtil.NOT_CONNECTED){
                    internetAvailable = false;
                    Log.e("NetworkConnectivity","OFFLINE");
                    Dashboard.isOnline = "OFFLINE";
                } else if(networkState == NetworkUtil.MOBILE){
                    internetAvailable = true;
                    Dashboard.isOnline = "ONLINE";
                    Log.e("NetworkConnectivity","ONLINE");
                    isNetworkConnectivity(context);
                    Dashboard.listPatientsService();
                } else if(networkState == NetworkUtil.WIFI){
                    internetAvailable = true;
                    Dashboard.isOnline = "ONLINE";
                    Log.e("NetworkConnectivity","ONLINE");
                    isNetworkConnectivity(context);
                    Dashboard.listPatientsService();
                }
            }
        };
    }
    public void register(Context context){
        context.registerReceiver(networkChangeReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
    }
    public void unregister(Context context){
        context.unregisterReceiver(networkChangeReceiver);
    }
    public void isNetworkConnectivity(Context context){
        OperationsDB operationsDB = OperationsDB.obtenerInstancia(context);
        Syn syn = new Syn();
        for (Paciente paciente : operationsDB.obtenerPatientsOffLine()){
            syn.synPat(paciente,context);
        }
    }
}